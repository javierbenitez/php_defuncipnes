<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"&&$_SESSION['rango']!="especial"){
    header("Location:index.php");
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>
<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container" >
    <?php
    if(isset($_GET['info']) && trim($_GET['info']) != ""){
        echo '<div><p class="alert alert-warning">'.$_GET['info'].'</p></div>';
    }
    ?>
    <form class="form-horizontal m-5-arriba" id="formulario" name="formulario" action="controller/UsuarioController.php" autocomplete="off" method="POST">
        <section class="row">
            <div class="col-md-12 text-center">
                <h5>Alta del usuario</h5>
            </div>
        </section>
        <br><br>

        <section class="row justify-content-center">
            <div class="col-sm-12 p-5 Borde_difuminado">
                <div class="row">
                    <div class="col-md-12" >
                        <h4>Usuario</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" >
                        <input required name ="usuario"  autocomplete="off" type="text" class="form-control" id="usuario" value="<?php if(isset($_SESSION['created_user']))echo $_SESSION['created_usuariop'];?>" placeholder="Usuario">
                    </div>
                    <div class="col-md-4" >
                        <input required name ="correo" type="email" class="form-control" id="correo" value="<?php if(isset($_SESSION['created_correo']))echo $_SESSION['created_correo'];?>" placeholder="Correo">
                    </div>
                    <div class="col-md-4" >
                        <input name ="precio" type="number" step="any" class="form-control" id="precio" value="<?php if(isset($_SESSION['created_precio']))echo $_SESSION['created_precio'];?>" placeholder="0.0">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" >
                        <input required name ="contrasena"  autocomplete="off" type="password" class="form-control" id="contrasena" aria-describedby="" placeholder="Contraseña">
                    </div>
                    <div class="col-md-6" >
                        <input required name ="rep_contrasena" type="password" class="form-control" id="rep_contrasena" aria-describedby="" placeholder="Repite Contraseña">
                    </div>
                    <div class="col-md-3 p-5-izquierda">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="rango">
                            Tipo de usario
                        </label>
                    </div>
                    <div class="col-md-3">
                        <input type="radio" name="rango" value="admin"> Administrador<br>
                        <input type="radio" name="rango" value="grupo"> Grupo<br>
                        <input type="radio" name="rango" checked value="normal"> Normal
                    </div>
                    <div class="col-md-6 padd12 alert-warning">
                        <div class="">
                            <label for="contra_admin">Introduce tu contraseña para validar:</label>
                            <input name ="contra_admin" type="password" class="form-control" id="contra_admin" aria-describedby="" placeholder="Contraseña rango admin">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="row justify-content-center m-5-arriba">
            <div class="col-sm-12 p-5 Borde_difuminado" >
                <div class="row">
                    <div class="col-md-12" >
                        <h4>Datos Empresa</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" >
                        <input required name ="nombre" type="text" class="form-control" id="nombre"  value="<?php if(isset($_SESSION['created_nombre']))echo $_SESSION['created_nombre'];?>" placeholder="Nombre">
                    </div>
                    <div class="col-md-4" >
                        <input required name ="numero" type="text" class="form-control" id="numero"  value="<?php if(isset($_SESSION['created_numero']))echo $_SESSION['created_numero'];?>" placeholder="Numero De Telefono">
                    </div>
                    <div class="col-md-4" >
                        <input required name ="nif" type="text" class="form-control" id="nif"  value="<?php if(isset($_SESSION['created_nif']))echo $_SESSION['created_nif'];?>" placeholder="NIF">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <input required name ="direccion" type="text" class="form-control" id="direccion"  value="<?php if(isset($_SESSION['created_direccion']))echo $_SESSION['created_direccion'];?>" placeholder="Direccion">
                    </div>
                    <div class="col-md-4">
                        <input required name ="localidad" type="text" class="form-control" id="localidad"  value="<?php if(isset($_SESSION['created_localidad']))echo $_SESSION['created_localidad'];?>" placeholder="Localidad">
                    </div>
                    <div class="col-md-4">
                        <input required name ="cp" type="number" class="form-control" id="cp"  value="<?php if(isset($_SESSION['created_cp']))echo $_SESSION['created_cp'];?>" placeholder="Codigo Postal">
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-6" >
                        <input name ="cuenta" type="text" class="form-control" id="cuenta"  value="<?php if(isset($_SESSION['created_cuenta']))echo $_SESSION['created_cuenta'];?>" placeholder="Numero de Cuenta">
                    </div>
                    <div class="col-md-6" >
                        <input name ="gerente" type="text" class="form-control" id="gerente"  value="<?php if(isset($_SESSION['created_gerente']))echo $_SESSION['created_gerente'];?>" placeholder="Gerente">
                    </div>
                </div>
            </div>
        </section>
        <br>
        <input name="boton" type="submit" class="btn btn-primary align-self-center" value="Enviar">
    </form>
</div>
<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script>

</script>
</body>
</html>
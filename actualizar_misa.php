<?php
session_start();

if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}

function deleteSessions()
{
    unset($_SESSION['codeCeremony']);
    unset($_SESSION['misa_f_nombre']);
    unset($_SESSION['misa_f_apellidos']);
    unset($_SESSION['misa_f_apodo']);

    unset($_SESSION['misa_f_direccion']);
    unset($_SESSION['misa_f_localidad']);
    unset($_SESSION['factura_DNI']);
    unset($_SESSION['misa_f_edad']);

    unset($_SESSION['misa_f_fecha_fallecimiento']);
    unset($_SESSION['misa_f_fecha_alta']);

//    unset($_SESSION['misa_f_imagen']);

    unset($_SESSION['misa_f_premiun']);
    unset($_SESSION['misa_f_precio']);

    unset($_SESSION['cp1']);
    unset($_SESSION['cp2']);
    unset($_SESSION['cp3']);

    unset($_SESSION['misa_f_marido']);

    unset($_SESSION['misa_f_hijos']);
    unset($_SESSION['misa_f_hijos_politicos']);

    unset($_SESSION['misa_f_hermanos']);
    unset($_SESSION['misa_f_hermanos_politicos']);

    unset($_SESSION['misa_f_familiares']);
    unset($_SESSION['misa_f_pareja']);
    unset($_SESSION['misa_f_padres']);

    unset($_SESSION['misa_f_d_recibe']);
    unset($_SESSION['misa_f_d_despide']);

    unset($_SESSION['misa_f_lugar']);
    unset($_SESSION['misa_f_lugar_iglesia']);

    unset($_SESSION['misa_f_fecha_ceremonia']);
    unset($_SESSION['misa_f_hora']);

    unset($_SESSION['f_lugar2']);
    unset($_SESSION['f_fecha_cerem2']);
    unset($_SESSION['f_hora2']);

    unset($_SESSION['misa_f_Texto']);

    unset($_SESSION['fact_nombre']);
    unset($_SESSION['fact_apellidos']);
    unset($_SESSION['fact_telefono']);

}

if (!empty( $_GET["cod"] ) ) {

    if ($_SESSION["fact_Codigo"] === $_GET["cod"]){

        require("controller/Conexion.php");
        include 'librerias/Encriptador.php';

        $usuario=$_SESSION['usuario'];
        $correo=$_SESSION['correo'];
        $rango=$_SESSION['rango'];
        $nif=$_SESSION['nif'];
        $nombre=$_SESSION['nombre'];

        $nombre=$_SESSION['misa_f_nombre'];
        $apellidos=$_SESSION['misa_f_apellidos'];
        $apodo=$_SESSION['misa_f_apodo'];

        $edad=$_SESSION['misa_f_edad'];

        $fecha_fallecimiento=$_SESSION['misa_f_fecha_fallecimiento'];
        $fecha_alta=$_SESSION['misa_f_fecha_alta'];

//        $Imagen_Ruta=$_SESSION['misa_f_imagen'];

        $premium=$_SESSION['misa_f_premiun'];
        $anual=$_SESSION['misa_f_anual'];
        $precio=$_SESSION['misa_f_precio'];

        $cp1 = $_SESSION['cp1'];
        $cp2 = $_SESSION['cp2'];
        $cp3 = $_SESSION['cp3'];

        $marido=$_SESSION['misa_f_marido'];

        $hijos=$_SESSION['misa_f_hijos'];
        $hijos_politicos=$_SESSION['misa_f_hijos_politicos'];

        $hermanos=$_SESSION['misa_f_hermanos'];
        $hermanos_politicos=$_SESSION['misa_f_hermanos_politicos'];

        $familiares=$_SESSION['misa_f_familiares'];
        $pareja=$_SESSION['misa_f_pareja'];
        $padres=$_SESSION['misa_f_padres'];

        $lugar=$_SESSION['misa_f_lugar'];

        $fecha_ceremonia=$_SESSION['misa_f_fecha_ceremonia'];
        $hora=$_SESSION['misa_f_hora'];

        $arrCeremonia2 = [];
        if(isset($_SESSION['f_lugar2']) && $_SESSION['f_lugar2'] != null){
            $arrCeremonia2[0] = $_SESSION['f_lugar2'];
            $arrCeremonia2[1] = $_SESSION['f_fecha_cerem2'];
            $arrCeremonia2[2] = $_SESSION['f_hora2'];
        }

        $fNombre = $_SESSION['fact_nombre'];
        $fApellidos = $_SESSION['fact_apellidos'];
        $fTelefono = $_SESSION['fact_telefono'];
        $fCodigo = $_SESSION['fact_Codigo'];

        $Libreria=new Logeo();

        if ($Libreria->updateCeremony($nombre, $apellidos, $apodo, $edad, $fecha_fallecimiento, $premium, $marido,
            $hijos, $hijos_politicos, $hermanos, $hermanos_politicos, $familiares, $pareja, $padres, $lugar, $fecha_ceremonia, $hora,
            $anual, $cp1, $cp2, $cp3, $arrCeremonia2, $fNombre, $fApellidos, $fTelefono)){

            deleteSessions();
            $message = "Misa actualizada correctamente";
            header("Location: inicio.php?m=$message");
        }else{
            $message = "Error al intentar actualizar la misa";
            header("Location: inicio.php?m=$message");
        }
    }else{
        deleteSessions();
        header("Location: inicio.php");
    }
}else{
    echo "codigo no introducido";
}




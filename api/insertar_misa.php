<?php
// /api/insertar_misa.php
session_start();

$result['estado'] = 1;

if($_SERVER['REQUEST_METHOD'] == 'POST') {

    $result['estado'] = 10; // Resquest POST method

    include_once "../modelo/Misa.php";
    include_once "../librerias/DataMisa.php";
    include_once "../librerias/CustomDate.php";

    $misa = new Misa();
    $dataMisa = new DataMisa();

    $pay = 0;
    $fechaFallecimiento = CustomDate::formatDate(trim($_POST['fecha_fallecimiento']));
    $fechaMisa = CustomDate::formatDate(trim($_POST['fecha_misa']));
    if(isset($_POST['lugar2']) && $_POST['lugar2'] != "" &&
        isset($_POST['fecha_misa2']) && $_POST['fecha_misa2'] != ""){
        $fechaMisa2 = CustomDate::formatDate(trim($_POST['fecha_misa2']));
        $misa->setPlaceCeremony2(trim($_POST['lugar2']));
        $misa->setDateCeremony2($fechaMisa2);
        $misa->setHourCeremony2(trim($_POST['hora_misa2']));
    }

    $misa->setAniversary(trim($_POST['aniversario']));
    $misa->setName(trim($_POST['nombre']));
    $misa->setLastName(trim($_POST['apellidos']));
    $misa->setNickName(trim($_POST['apodo']));
    $misa->setDateDeath($fechaFallecimiento);
    $misa->setAge(trim($_POST['edad']));
    $misa->setPremium(trim($_POST['premium']));
    $misa->setConyuge(trim($_POST['conyuge']));
    $misa->setHusband(trim($_POST['esposo']));
    $misa->setChildren(trim($_POST['hijos']));
    $misa->setPoliticalChildren(trim($_POST['hijos_politicos']));
    $misa->setBrothers(trim($_POST['hermanos']));
    $misa->setPoliticalBrothers(trim($_POST['hermanos_politicos']));
    $misa->setFamily(trim($_POST['familiares']));
    $misa->setPartner(trim($_POST['pareja']));
    $misa->setPlaceCeremony(trim($_POST['lugar_misa']));
    $misa->setDateCeremony($fechaMisa);
    $misa->setHourCeremony(trim($_POST['hora_misa']));
    $misa->setCp1(trim($_POST['cp_1']));
    $misa->setCp2(trim($_POST['cp_2']));
    $misa->setCp3(trim($_POST['cp_3']));
    $misa->setPhone(trim($_POST['telefono']) != null ? trim($_POST['telefono']) : "");
    $misa->setPay(0);

    $ok = $dataMisa->insertMisa($misa);

    if($ok){
        $result['estado'] = 2;
    }else{
        $result['estado'] = 3;
    }
}
else if ($_SERVER['REQUEST_METHOD'] == 'GET'){
    $result['estado'] = 9; // Resquest GET method

    include_once "../modelo/Misa.php";
    include_once "../librerias/DataMisa.php";
    include_once "../modelo/Notification.php";
    include_once "../librerias/DataDevice.php";

    $dataMisa = new DataMisa();
    $misa = new Misa();

    if(isset($_GET['p1']) && isset($_GET['p2']) && isset($_GET['p3']) && isset($_GET['p4']) && isset($_GET['p5'])){
        $date = new DateTime();
        $date = $date->format('dmY');
        if(trim($_GET['p1']) === '_.loperthjnmaty*mnvsiuo?!wer' && $date === trim($_GET['p5'])){
            $misa->setName(trim($_GET['p2']));
            $misa->setLastName(trim($_GET['p3']));
            $misa->setAge(trim($_GET['p4']));
            $respMisa = $dataMisa->getMisa($misa);
            $misa->setCp1($respMisa[0]);
            if($respMisa[1] != ""){
                $misa->setCp2($respMisa[1]);
            }
            if($respMisa[2] != ""){
                $misa->setCp3($respMisa[2]);
            }

            if($respMisa != null && !empty($respMisa)){
                $ok = $dataMisa->updateMisa($misa);

                if($ok > -1){
                    $result['estado'] = 2;
                    $_POST['payment'] = "_.loperthjnmaty*mnvsiuo?!wer";
                    $dataDevice = new DataDevice();
                    $notification = new Notification();

                    $arrayDevicesAndroid = $dataDevice->getDevices($misa->getCp1(), $misa->getCp2(), $misa->getCp3(), "Android");
                    $arrayDevicesiOS = $dataDevice->getDevices($misa->getCp1(), $misa->getCp2(), $misa->getCp3(), "iOS");

                    $tokenDevicesAndroid = [];
                    foreach ($arrayDevicesAndroid as $deviceAndroid){
                        $tokenDevicesAndroid[] = $deviceAndroid['token_device'];
                    }

                    $title = "Nueva misa ...";
                    if ($misa->getPremium() == 1) {
                        // Misa premium
                        if(!empty($tokenDevicesAndroid)){
                            $notification->sendAndroidPush($tokenDevicesAndroid, $title, "");
                        }
                        foreach ($arrayDevicesiOS as $deviceiOS){
                            $notification->sendIOSPush("",$message);
                        }
                    } else {
                        // Misa no premium
                        if(!empty($tokenDevicesAndroid)){
                            $body = "Nueva misa en el ".$misa->getCp1();
                            $mensaje = "Nueva misa";
                            if(count($tokenDevicesAndroid)>1000){
                                $numRequest = ceil(count($tokenDevicesAndroid) / 1000);
                                for($a = 0; $a < $numRequest; $a++){
                                    $devices = array_slice($tokenDevicesAndroid, ($a*1000), 1000);
                                    $notification->sendAndroidPush($devices, $mensaje, $body);
                                }
                            }else{
                                $notification->sendAndroidPush($tokenDevicesAndroid, $mensaje, $body);
                            }
                        }
                        foreach ($arrayDevicesiOS as $deviceiOS){
                            $notification->sendIOSPush($deviceiOS, $message);
                        }

                    }
                }else{
                    $result['estado'] = 3;
                }
            }else{
                $result['estado'] = 3;
            }
        }
    }
}

echo json_encode($result);

<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']=="especial"){
    header("Location:index.php");
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];
require "controller/Conexion.php";
$Libreria=new Logeo();

?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>

<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container ">
    <?php
    if (isset($_GET["e"])){
        switch ($_GET["e"]){
            case 1:
                ?>
                <div class="alert alert-danger" role="alert">
                    Contraseña actual incorrecta
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php
                break;
            case 2:
                ?>
                <div class="alert alert-danger" role="alert">
                    Error cambiando la contraseña. (Consejo de segurirdad: Utiliza números, letras y algún carácter raro como '.' o ',' o '?' o '*', etc.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php
                break;
            default:
                ?>
                <div class="alert alert-success" role="alert">
                    Contraseña cambiada correctamente
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php
                break;
        }
        ?>
        <?php

    }
    ?>
    <section class="justify-content-center">
        <h2>Cambiar contraseña</h2>
        <form  class="row" id="formulario" name="formulario" action="confirmar_cambio_pass.php" enctype="multipart/form-data" method="POST">
            <div class="col-md-4" >
                <input required name ="pass_1" type="password" class="form-control" id="pass_1" aria-describedby="" placeholder="Contraseña actual">
            </div>
            <div class="col-md-4" >
                <input required name ="pass_2" type="password" class="form-control" id="pass_2" aria-describedby="" placeholder="Nueva contraseña">
            </div>
            <div class="col-md-4" >
                <input required name ="pass_3" type="password" class="form-control" id="pass_3" aria-describedby="" placeholder="Repite contraseña">
            </div>
        </form>
    </section>
    <br>
    <a class="btn btn-primary btn-lg" href='javascript:void(0);' onclick="cambiar();" role="button">
        Cambiar
    </a>
</div>
 <?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script>
    function cambiar() {
        var pass_2 = document.getElementById("pass_2").value;
        var pass_3 = document.getElementById("pass_3").value;

        if(pass_2.length > 2 && pass_3.length > 2){
            if (pass_3 === pass_2) {
                document.getElementById("formulario").submit();
            } else {
                alert("Las nuevas contraseñas no coinciden");
            }
        }else{
            alert("Debes escibir al menos 3 caracteres.");
        }

    }
</script>
</body>
</html>
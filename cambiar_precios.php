<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']=="especial"){
    header("Location:index.php");
}

$result = null;
$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];
require "controller/Conexion.php";
$Libreria=new Logeo();

if(isset($_SESSION['results_prices']) && $_SESSION['results_prices'] != null){
    $result = $_SESSION['results_prices'];
}


?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>

<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<?php
if (isset($_GET["e"])){
    switch ($_GET["e"]){
        case 'Ok':
            ?>
            <div class="panel text-center justify-content-center m-5-arriba">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    Precio cambiado correctamente
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <?php
            break;
        case 'Ko':
            ?>
            <div class="panel text-center justify-content-center m-5-arriba">
                <div class="alert alert-danger  alert-dismissible fade show" role="alert">
                    Error al cambiar el precio
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <?php
            break;
    }
    ?>
    <?php

}
?>
<div class="container ">
    <section class="justify-content-center">
        <article>
            <h2>Cambiar precios</h2>
            <form  id="formulario" name="formulario" action="controller/change_prices.php" enctype="multipart/form-data" method="POST">
                <div class="row">
                    <div class="col-md-3">
                        <select name="element" id="element" class="form-control">
                            <option value="Misas">Misas</option>
                            <option value="Defunciones">Defunciones</option>
                            <option value="Misas móviles">Misas móviles</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select name="type" id="type" class="form-control">
                            <option value="normal">Normal</option>
                            <option value="premium">Premium</option>
                        </select>
                    </div>
                    <div class="col-md-3" >
                        <input required name ="price" type="number" step="0.01" class="form-control" id="price" aria-describedby="" placeholder="15.0">
                    </div>
                    <input type="submit" value="Cambiar" class="btn btn-success">
                </div>
            </form>
        </article>
        <article>
            <h3>Precios actuales</h3>
            <table class="table">
                <tr>
                    <th scope="col">Elemento</th>
                    <th scope="col">Tipo</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Actualizado</th>
                    <th scope="col">Usuario</th>
                </tr>
            <?php
            if($result != null) {
                for ($i = 0; $i < count($result); $i++) {
                    echo '<tr>
                            <td>' . $result[$i]['elemento'] . '</td>
                            <td>' . $result[$i]['tipo'] . '</td>
                            <td>' . $result[$i]['precio'] . ' €</td>
                            <td>' . $result[$i]['updated'] . '</td>
                            <td>' . $result[$i]['usuario'] . '</td>
                        </tr>';
                }
            }
            ?>
            </table>
        </article>
    </section>
</div>
 <?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script>
    function cambiar() {
        var pass_2 = document.getElementById("pass_2").value;
        var pass_3 = document.getElementById("pass_3").value;

        if(pass_2.length > 2 && pass_3 > 2){
            if (pass_3 == pass_2) {
                document.getElementById("formulario").submit();
            } else {
                alert("Las nuevas contraseñas no coinciden");
            }
        }else{
            alert("Debes escibir al menos 3 caracteres.");
        }

    }
</script>
</body>
</html>
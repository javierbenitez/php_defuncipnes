<?php
session_start();
session_destroy();

unset($_SESSION['usuario']);
unset($_SESSION['usuario_id']);
unset($_SESSION['correo']);
unset($_SESSION['rango']);
unset($_SESSION['nif']);
unset($_SESSION['nombre']);
unset($_SESSION['cp']);
unset($_SESSION['empresa_id']);
unset($_SESSION['fecha']);

header("Location:index.php");

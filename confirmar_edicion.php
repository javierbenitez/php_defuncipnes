<?php
session_start();
if (!isset($_SESSION['usuario']) && !isset($_SESSION['rango'])) {
    header("Location: index.php");
}

$ruta = "./imagenes/difuntos/";
@chmod($ruta, 0755);

$Precio_Normal=49;
$Precio_Premiun=100;
$lugar2 = null;
$insurance = null;
$foto1 = substr($_SESSION['publicacion']['foto'], -4, 4);
$foto2 = substr($_SESSION['publicacion']['foto2'], -4, 4);
$hasImg = strstr($foto1, '.');
$hasImg2 = strstr($foto2, '.');

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];
$nif=$_SESSION['nif'];
$cp=$_SESSION['cp'];

$nombre=$_POST['nombre'];
$apellidos = $_POST["apellidos"];
$apodo=$_POST["apodo"];
$direccion=$_POST["direccion"];
$localidad=$_POST["localidad"];
$edad=$_POST["edad"];

if(isset($_POST['is_insurance'])){
    if($_POST['is_insurance'] == "on") {
        $insurance = $_POST["insurance"];
    }
}

$fecha_fallecimiento=$_POST["fecha_fallecimiento"];
$fecha_alta=$_POST["fecha_alta"];

$contactPhone1 = $_POST["contact_phone1"];
$contactPhone2 = $_POST["contact_phone2"];

$path = "imagenes/difuntos/".date('Ymd')."/";

if (!file_exists($path)) {
    mkdir($path, 0777, true);
}

if(isset($_FILES['imagen']) && $_FILES['imagen']['size'] > 0){
    $imagen = $_FILES["imagen"];
}else{
    $imagen = $_SESSION['publicacion']['foto'];
}
if(isset($_POST['imagen'])){
    $imagen = $path.$_POST["imagen"];
}

if(isset($_POST['imagen2'])){
    $imagen2 = $path.$_POST["imagen2"];
}

if (isset($_POST["premiun"])){
    $premiun=1;
    $precio=$Precio_Premiun;
}else{
    $premiun=0;
    $precio=$Precio_Normal;
}

$cp1 = $_POST['codigo_postal1'];
$cp2 = $_POST['codigo_postal2'];
$cp3 = $_POST['codigo_postal3'];

$conyuge=$_POST["conyuge"];

$hijos=$_POST["hijos"];
$hijos_politicos=$_POST["hijos_politicos"];

$hermanos=$_POST["hermanos"];
$hermanos_politicos=$_POST["hermanos_politicos"];

$familiares=$_POST["familiares"];
$pareja=$_POST["pareja"];
$padres=$_POST["padres"];

$d_recibe = $_POST["d_recibe"];
$d_despide = $_POST["d_despide"];

$lugar=$_POST["lugar"];
$lugar_iglesia=$_POST["lugar_iglesia"];
$fecha_entierro=$_POST["fecha_entierro"];
$hora=$_POST["hora"];

if(isset($_POST['otra_ceremonia'])){
    if($_POST['otra_ceremonia'] == "on"){
        $lugar2=$_POST["lugar2"];
        $lugar_iglesia2=$_POST["lugar_iglesia2"];
        $fecha_entierro2=$_POST["fecha_entierro2"];
        $hora2=$_POST["hora2"];
    }
}

$Texto=$_POST["Texto"];

$Imagen_Ruta = "";
$Imagen_Ruta2 = "";

$extensiones=array("image/jpg","image/jpeg","image/png");
$limite_mg=5;

@chmod($path, 0755);

if(isset($_FILES['imagen']) && $_FILES['imagen']['size'] > 0){
    $Imagen_Ruta = $path.$_FILES["imagen"]["name"];
    move_uploaded_file($_FILES["imagen"]["tmp_name"], $Imagen_Ruta);
}else{
    $Imagen_Ruta = $_SESSION['publicacion']['foto'];
}
if(isset($_POST['imagen']) && $_POST['imagen'] != ''){
    $Imagen_Ruta = $_POST["imagen"];
}

if(isset($_FILES['imagen2']) && $_FILES['imagen2']['size'] > 0){
    $Imagen_Ruta2 = $path.$_FILES["imagen2"]["name"];
    move_uploaded_file($_FILES["imagen2"]["tmp_name"], $Imagen_Ruta2);
}else{
    $Imagen_Ruta2 = $_SESSION['publicacion']['foto2'];
}

if(isset($_POST['imagen2']) && trim($_POST['imagen2']) != ''){
    $Imagen_Ruta2 = $_POST["imagen2"];
}

if (isset($_POST["modificar"])){
    $editar=true;
    $Imagen_Ruta=$_FILES["imagen"];
    $Imagen_Ruta2 = $_FILES["imagen2"];
}else{
    $editar=false;
}
if (isset($_POST["borrar"])){
    header("Location: inicio.php?m=PnooK");
}
if (isset($_POST["enviar"])){
    require("controller/Conexion.php");

    if(isset($_POST['imagen']) && trim($_POST['imagen']) != ''){
        $_SESSION['f_imagen'] = $_POST['imagen'];
    }
    if(isset($_POST['imagen2']) && trim($_POST['imagen2']) != ''){
        $_SESSION['f_imagen2'] = $_POST['imagen2'];
    }

    $_SESSION['f_nombre'] = $nombre;
    $_SESSION['f_apellidos'] = $apellidos;
    $_SESSION['f_direccion'] = $direccion;
    $_SESSION['f_apodo']=$apodo;
    $_SESSION['f_localidad']=$localidad;
    $_SESSION['f_edad']=$edad;

    if($insurance != null) {
        $_SESSION['f_insurance'] = $insurance;
    }

    $_SESSION['f_fecha_fallecimiento']=$fecha_fallecimiento;
    $_SESSION['f_fecha_alta']=$fecha_alta;

    $_SESSION['contact_phone1'] = $contactPhone1;
    $_SESSION['contact_phone2'] = $contactPhone2;

    if ($_POST["premiun"]){
        $_SESSION['f_premiun']=1;
        $_SESSION['f_precio']=$Precio_Premiun;
    }else{
        $_SESSION['f_premiun']=0;
        $_SESSION['f_precio']=$Precio_Normal;
    }

    $_SESSION['cp1'] = $cp1;
    $_SESSION['cp2'] = $cp2;
    $_SESSION['cp3'] = $cp3;

    $_SESSION['f_conyuge']=$conyuge;

    $_SESSION['f_hijos']=$hijos;
    $_SESSION['f_hijos_politicos']=$hijos_politicos;

    $_SESSION['f_hermanos']=$hermanos;
    $_SESSION['f_hermanos_politicos']=$hermanos_politicos;

    $_SESSION['f_familiares']=$familiares;
    $_SESSION['f_pareja']=$pareja;
    $_SESSION['f_padres']=$padres;

    $_SESSION['f_d_recibe'] = $d_recibe;
    $_SESSION['f_d_despide'] = $d_despide;

    $_SESSION['f_lugar']=$lugar;
    $_SESSION['f_lugar_iglesia']=$lugar_iglesia;
    $_SESSION['f_fecha_entierro']=$fecha_entierro;
    $_SESSION['f_hora']=$hora;

    if($lugar2 != null){
        $_SESSION['f_lugar2']=$lugar2;
        $_SESSION['f_lugar_iglesia2']=$lugar_iglesia2;
        $_SESSION['f_fecha_entierro2']=$fecha_entierro2;
        $_SESSION['f_hora2']=$hora2;
    }

    $_SESSION['f_Texto']=$Texto;

    $_SESSION['factura_Nombre'] = $_POST["f_nombre"];
    $_SESSION['factura_Apellidos'] = $_POST["f_apellidos"];
    $_SESSION['factura_Telefono'] = $_POST['f_telefono'];

    $fecha_actual = strtotime(date('Y-m-d'));
    $fecha_entrada = strtotime($_SESSION['fecha']);
    $fecha_diferencia=$fecha_actual-$fecha_entrada;
    $diasFalt = (( ( $fecha_diferencia / 60 ) / 60 ) / 24);

    $libreria=new Logeo();
    $company = $libreria->getCompanyLogged();

    if($company[0]['premium'] == 1){
        header("Location: paypublication.php");
    }else{
        if($_SESSION['f_premiun'] == 1){
            header("Location: paypublication.php");
        }else{
            $_SESSION['f_precio'] = 0;
            $_SESSION['factura_Codigo'] = $_SESSION['codePublication'];
            header("Location: actualizar_publicacion.php?cod=".$_SESSION['codePublication'] );
        }
    }
}

?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">-->
    <script src="ckeditor/ckeditor.js"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/personalizado.css">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
</head>

<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<form  class="form-horizontal m-5-arriba" id="formulario" name="formulario"  enctype="multipart/form-data" method="POST">
    <div class="container" >
        <section class="row justify-content-center">
            <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba"   >
                <div class="row">
                    <div class="col-md-12" >
                        <h4>Datos compra</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-4" >
                        <h5>Nombre</h5>
                        <input value="<?php echo $_SESSION['publicacion']['name_contact'];  ?>" required name="f_nombre" type="text" class="form-control" id="f_nombre" aria-describedby="" placeholder="Nombre">
                    </div>
                    <div class="col-sm-6 col-md-4" >
                        <h5>Apellidos</h5>
                        <input value="<?php echo $_SESSION['publicacion']['last_name_contact'];  ?>" required name="f_apellidos" type="text" class="form-control" id="f_apellidos" aria-describedby="" placeholder="Apellidos">
                    </div>
                    <div class="col-sm-6 col-md-4" >
                        <h5>Teléfono</h5>
                        <input value="<?php echo $_SESSION['publicacion']['phone_contact'];  ?>" name="f_telefono" type="text" class="form-control" id="f_telefono" aria-describedby=""  placeholder="Teléfono">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba"   >
                <div class="row">
                    <div class="col-md-12" >
                        <h4>Datos publicacion</h4>
                    </div>
                </div>
                <section class="row justify-content-center m-5-arriba">
                    <div class="col-md-4 text-center box_img">
                        <h3>Imagen del tanatorio</h3>
                        <input readonly type='hidden' name='imagen' value='<?php echo $Imagen_Ruta?>'>
                        <img style="width: 100%" id="img_1" src="<?php echo $Imagen_Ruta?>">
                        <button type="button" class="x_close" data-value="1">x</button>
                    </div>
                    <div class="col-md-4 text-center box_img">
                        <h3>Imagen del fallecido</h3>
                        <input readonly type='hidden' name='imagen2' value='<?php echo $Imagen_Ruta2?>'>
                        <img style="width: 100%"  id="img_2" src="<?php echo $Imagen_Ruta2?>">
                        <button type="button" class="x_close" data-value="2">x</button>
                    </div>
                </section>
                <div class="row m-5-arriba">
                    <div class="col-md-4" >
                        <h5>Nombre</h5>
                        <input readonly value="<?php echo $nombre;?>" required name ="nombre" type="text" class="form-control" id="nombre" aria-describedby="" placeholder="Nombre" >
                    </div>
                    <div class="col-md-4" >
                        <h5>Apellidos</h5>
                        <input readonly value="<?php echo $apellidos;?>" required name ="apellidos" type="text" class="form-control" id="apellidos" aria-describedby="" placeholder="Apellidos">
                    </div>
                    <div class="col-md-4" >
                        <h5>Apodo</h5>
                        <input readonly value="<?php echo $apodo;?>" name ="apodo" type="text" class="form-control" id="apodo" aria-describedby="" placeholder="Apodo">
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-8" >
                        <h5>Direccion</h5>
                        <input readonly value="<?php echo $direccion;?>" required name ="direccion" type="text" class="form-control" id="direccion" aria-describedby="" placeholder="Direccion">
                    </div>
                    <div class="col-md-4" >
                        <h5>Localidad</h5>
                        <input readonly value="<?php echo $localidad;?>" required name ="localidad" type="text" class="form-control" id="localidad" aria-describedby="" placeholder="Localidad">
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-4" >
                        <h5>Fecha fallecimiento</h5>

                        <input readonly  value="<?php echo $fecha_fallecimiento;?>" required name="fecha_fallecimiento" type="text" class="form-control" id="fecha_fallecimiento" aria-describedby="" placeholder="Contraseña" >
                    </div>
                    <div class="col-md-4" >
                        <h5>Fecha alta</h5>
                        <input readonly required readonly="readonly" name ="fecha_alta" type="date" value="<?php echo $fecha_alta;?>" class="form-control m-0" id="fecha_alta" aria-describedby="" placeholder="fecha_alta">
                    </div>
                    <div class="col-md-4">
                        <h5>Edad</h5>
                        <input readonly value="<?php echo $edad;?>" required name="edad" type="text" class="form-control" id="edad" aria-describedby="" placeholder="Edad" >
                    </div>
                </div>
                <?php if($insurance != null){ ?>
                    <input readonly type="text" class="hidden" value="on" id="is_insurance" name="is_insurance">
                    <div class="row m-2-arriba" >
                        <div class="col-sm-12 col-md-4" >
                            <h4>Aseguradora</h4>
                        </div>
                        <div class="col-md-8" >
                            <input readonly value="<?php echo $insurance;?>" name ="insurance" type="text" class="form-control" id="insurance">
                        </div>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-md-6" >
                        <input type="text" title="Este campo es opcional" readonly
                               value="<?php echo $contactPhone1;?>"
                               name="contact_phone1" id="contact_phone1" class="form-control" placeholder="Teléfono contacto">
                    </div>
                    <div class="col-md-6" >
                        <input type="text" title="Este campo es opcional" readonly
                               value="<?php echo $contactPhone2;?>"
                               name="contact_phone2" id="contact_phone2" class="form-control" placeholder="Otro teléfono contacto">
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-3 align-self-center">
                        <h5>Servicio</h5>
                        <input readonly type='hidden' name='premiun' value='<?php echo $premiun?>'>
                        <span><?php if ($premiun){echo "Premiun";}else{echo "Normal";}?></span>
                    </div>
                    <div class="col-md-3">
                        <h5>Código postal 1</h5>
                        <input readonly type='text' name='codigo_postal1' readonly class="form-control" value="<?php echo $cp1?>">
                    </div>
                    <div class="col-md-3">
                        <h5>Código postal 2</h5>
                        <input readonly type='text' name='codigo_postal2' readonly class="form-control" value="<?php echo $cp2?>">
                    </div>
                    <div class="col-md-3">
                        <h5>Código postal 3</h5>
                        <input readonly type='text' name='codigo_postal3' readonly class="form-control" value="<?php echo $cp3?>">
                    </div>
                </div>

                <div class="row m-5-arriba">
                    <div class="col-md-6" >
                        <h5>Cónyuge</h5>
                        <input readonly value="<?php echo $conyuge;?>" name ="conyuge" type="text" class="form-control" id="conyuge" aria-describedby="" placeholder="Conyuge">
                    </div>
                    <div class="col-md-6" >
                        <h5>Padres</h5>
                        <input readonly value="<?php echo $padres;?>" name="padres" type="text" class="form-control" id="padres" aria-describedby="" placeholder="Padres">
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-6" >
                        <h5>Hijos</h5>
                        <textarea readonly   class="form-control" style="resize: none" rows="3" id="hijos" name="hijos" placeholder="Hijos"><?php echo $hijos;?></textarea>
                    </div>
                    <div class="col-md-6" >
                        <h5>Hijos Politicos</h5>
                        <textarea readonly class="form-control" style="resize: none" rows="3" id="hijos_politicos" name="hijos_politicos" placeholder="Hijos Politicos"><?php echo $hijos_politicos;?></textarea>
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-6" >
                        <h5>Hermanos</h5>
                        <textarea readonly class="form-control" style="resize: none" rows="3" id="hermanos" name="hermanos" placeholder="Hermanos"><?php echo $hermanos;?></textarea>
                    </div>
                    <div class="col-md-6" >
                        <h5>Hermanos politicos</h5>
                        <textarea readonly class="form-control" style="resize: none" rows="3" id="hermanos_politicos" name="hermanos_politicos" placeholder="Hermanos Politicos"><?php echo $hermanos_politicos;?></textarea>
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-6" >
                        <h5>Familiares</h5>
                        <textarea readonly class="form-control" style="resize: none" rows="3" id="familiares"  name="familiares" placeholder="Familiares"><?php echo $familiares;?></textarea>
                    </div>
                    <div class="col-md-6" >
                        <h5>Pareja</h5>
                        <input readonly value="<?php echo $pareja;?>" name="pareja" type="text" class="form-control" id="pareja" aria-describedby="" placeholder="Pareja">
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-6" >
                        <h5>Duelo recibe</h5>
                        <input readonly value="<?php echo $d_recibe;?>" name="d_recibe" type="text" class="form-control" id="d_recibe" >
                    </div>
                    <div class="col-md-6" >
                        <h5>Duelo despide</h5>
                        <input readonly value="<?php echo $d_despide;?>" name="d_despide" type="text" class="form-control" id="d_despide" >
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba"   >
                <div class="row">
                    <div class="col-md-12" >
                        <h4>Datos ceremonia</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" >
                        <h5>Localidad</h5>
                        <input readonly value="<?php echo $lugar;?>" required name ="lugar" type="text" class="form-control" id="lugar" aria-describedby="" placeholder="Lugar tanatorio">
                    </div>
                    <div class="col-md-6">
                        <h5>Iglesia</h5>
                        <input readonly value="<?php echo $lugar_iglesia;?>" required name ="lugar_iglesia" type="text" class="form-control" id="lugar_iglesia" aria-describedby="" placeholder="Lugar Iglesia">
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-6" >
                        <h5>Fecha Entierro</h5>
                        <input readonly value="<?php echo $fecha_entierro;?>" required name ="fecha_entierro" type="text" class="form-control" id="fecha_entierro" aria-describedby="" placeholder="fecha_entierro">
                    </div>
                    <div class="col-md-6" >
                        <h5>Hora Entierro</h5>
                        <input readonly value="<?php echo $hora;?>" required name ="hora" type="time" class="form-control" id="hora" aria-describedby="" placeholder="Hora">
                    </div>
                </div>
            </div>
            <?php if($lugar2 != null){ ?>
                <input readonly type="text" class="hidden" value="on" id="otra_ceremonia" name="otra_ceremonia">
                <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba" >
                    <div class="row">
                        <div class="col-md-12" >
                            <h4>Datos ceremonia 2</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" >
                            <h5>Localidad</h5>
                            <input readonly value="<?php echo $lugar2;?>" required name ="lugar2" type="text" class="form-control" id="lugar2" aria-describedby="" placeholder="Lugar tanatorio">
                        </div>
                        <div class="col-md-6">
                            <h5>Iglesia</h5>
                            <input readonly value="<?php echo $lugar_iglesia2;?>" required name ="lugar_iglesia2" type="text" class="form-control" id="lugar_iglesia2" aria-describedby="" placeholder="Lugar Iglesia">
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-md-6" >
                            <h5>Fecha Entierro</h5>
                            <input readonly value="<?php echo $fecha_entierro2;?>" required name ="fecha_entierro2" type="text" class="form-control" id="fecha_entierro2" aria-describedby="" placeholder="fecha_entierro">
                        </div>
                        <div class="col-md-6" >
                            <h5>Hora Entierro</h5>
                            <input readonly value="<?php echo $hora2;?>" required name ="hora2" type="time" class="form-control" id="hora2" aria-describedby="" placeholder="Hora">
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba">
                <div class="row">
                    <div class="col-md-12" >
                        <h4>Resumen</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" >
                        <textarea readonly required placeholder="Texto" name="Texto" id="Texto" >
                            <?php echo $Texto;?>
                        </textarea>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba">
                <div class="row">
                    <div class="col-md-12" >
                        <input required name ="" type="checkbox" class="form-check-input" id="" aria-describedby="" placeholder="">Acepto <a href="politica_cookies.php" target="_blank">los terminos y politicas de privacidad</a>
                    </div>
                </div>
            </div>

        </section>

            <a href="javascript:void(-1);" class="btn btn-primary align-self-center">Atrás</a>
            <button name="borrar" value="borrar" type="submit"  class="btn btn-primary align-self-center">Borrar</button>
            <button name="enviar" value="enviar" type="submit"  class="btn btn-primary align-self-center">Enviar</button>
    </div>
</form>

<?php include "includes/footer.php"; ?>


<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


<script>

    CKEDITOR.replace( 'Texto',{
        filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
        filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserWindowWidth: '1000',
        filebrowserWindowHeight: '700',
        height: "60vh"
    });


    CKFinder.widget( 'ckfinder-widget', {
        readOnly: true,
        width: '100%',
    });
</script>
<script>
    $(document).ready(function() {
        $('.x_close').on('click', function (e) {
            e.preventDefault();
            var item = $(this).data('value');
            var nInput = 2;
            if(item == 1){
                nInput = "";
            }
            var inputImg = $('input[name=imagen'+nInput+']');
            $('img#img_'+item).attr('src', "");
            inputImg.value = null;
            inputImg.val(null);
        })
    });
</script>
</body>
</html>

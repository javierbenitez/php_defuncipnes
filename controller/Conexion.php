<?php

//$basededatos= "db776154755";
//$servidor = "db776154755.hosting-data.io";
//$user = "dbo776154755";
//$pass = "ServiciosAurora2020.";

use modelo\Invoice;

/**
 * Class Logeo
 */
class Logeo{

    private static $instance;

    // The singleton method

    /**
     * @return __CLASS__|Logeo
     */
    public static function singleton()
    {
        if (!isset(self::$instance)) {
            self::$instance = new Logeo();
        }
        return self::$instance;
    }

    public function __construct() { }

    public function conectarBD(){
        $basededatos= "db776154755";
        $servidor = "127.0.0.1";
        $user = "root";
        $pass = "";

        $conexion = mysqli_connect($servidor,$user,$pass, $basededatos);

        if ($conexion){
            //echo "La conexión de la base de datos se ha realizado correctamente";
        }else{

            //echo "Error conectando a la base de datos";
        }

        return $conexion;
    }

    public function desconectarBD($conexion){
        $cerrado = mysqli_close($conexion);

        if ($cerrado){
            //echo "Desconexión a la BD correcta";
        }else{
            //echo "Error desconectando la BD";
        }
        return $cerrado;

    }

    /**
     * @param $usuario
     * @param $contraseña
     * @param $correo
     * @param $rol
     * @param $fecha_creacion
     * @param $created
     * @return bool
     */
    private function crearUsuario($conn, $usuario, $contraseña, $correo, $rol, $fecha_creacion, $created){

        $sql="INSERT INTO 
                  usuarios
                  (usuario, contrasena, correo, rango, fecha_creacion, created) 
              VALUES 
                  ('$usuario','".password_hash($contraseña, PASSWORD_DEFAULT)."','$correo','$rol','$fecha_creacion', '$created')";

        $funciono=mysqli_query($conn,$sql);

        if ($funciono){
            return true;
        }else{
            return false;
        }
    }

    private function actualizarFactura(){
        $codigo = $_SESSION['factura_Codigo'];
        $nombre = $_SESSION['factura_Nombre'];
        $apellidos = $_SESSION['factura_Apellidos'];

        $sql="UPDATE 
                    facturas 
              SET 
                  valida = 1 
              WHERE 
                  codigo = '$codigo' 
                  AND nombre = '$nombre' 
                  AND apellidos = '$apellidos' ";

        $ok = $this->querySet($sql);
        if ($ok){
            return true;
        }else{
            return false;
        }
    }

    private function actualizarFacturaMisas(){
        $codigo = $_SESSION['factura_Codigo'];
        $nombre = $_SESSION['factura_Nombre'];
        $apellidos = $_SESSION['factura_Apellidos'];

        $sql="UPDATE 
                    facturas_misas 
              SET 
                  valida = 1 
              WHERE 
                  codigo = '$codigo' 
                  AND nombre = '$nombre' 
                  AND apellidos = '$apellidos' ";

        $ok= $this->querySet($sql);
        if ($ok){
            return true;
        }else{
            return false;
        }
    }


    /**
     * Create company if user is created well
     * @param $usuario
     * @param $contraseña
     * @param $correo
     * @param $rol
     * @param $nif
     * @param $nombre
     * @param $codigo_postal
     * @param $direccion
     * @param $cuenta
     * @param $gerente
     * @param $numero
     * @param $fecha_creacion
     * @param int $premium
     * @return bool
     * @throws Exception
     * @throws Exception
     */
    function Dar_Alta($usuario, $contraseña, $correo, $rol, $nif, $nombre, $codigo_postal, $direccion, $cuenta, $gerente, $numero,
                      $fecha_creacion, $premium = 0){

        $date = new DateTime();
        $created = $date->format('Y-m-d');
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        if ($this->crearUsuario($conexion, $usuario,$contraseña,$correo,$rol,$fecha_creacion, $created)){

            $lastId = $conexion->insert_id;

            $sql="INSERT INTO 
                      empresas
                      (NIF, usuario_id, Nombre, CP, direccion, cuenta, gerente, numero, created, premium) 
                  VALUES 
                      ('$nif', '$lastId','$nombre',$codigo_postal,'$direccion','$cuenta','$gerente','$numero', '$created', '$premium')";
            $funciono=mysqli_query($conexion,$sql);

            $this->desconectarBD($conexion);
            if ($funciono){
                return true;
            }else{
                $conexion = $this->conectarBD();
                mysqli_set_charset($conexion,"utf8");
                $sql="DELETE FROM usuarios WHERE usuario='$usuario'";
                $funciono=mysqli_query($conexion,$sql);
                $this->desconectarBD($conexion);
                return false;
            }
        }else{
            return false;
        }
    }

    function crearFactura($nif, $codigo, $precio){
        $factNombre = $_SESSION['factura_Nombre'];
        $factApellidos = $_SESSION['factura_Apellidos'];
        $factDireccion = $_SESSION['factura_Direccion'];
        $telefono = $_SESSION['telefono'];
        $today = new DateTime();
        $today = $today->format("Y-m-d");

        $sql="INSERT INTO 
                facturas  
                  (codigo,
                  nif, 
                  precio, 
                  fecha,
                  telefono, 
                  nombre, 
                  apellidos, 
                  direccion,
                  valida) 
              VALUES 
                  ('$codigo',
                  '$nif',
                  '$precio', 
                  '$today',
                  '$telefono',
                  '$factNombre',
                  '$factApellidos',
                  '$factDireccion',
                  0)";

        $ok = $this->querySet($sql);

        if ($ok){
            return true;
        }else{
            return false;
        }
    }

    function crearFacturaMisas($nif, $codigo, $precio){
        $factNombre = $_SESSION['factura_Nombre'];
        $factApellidos = $_SESSION['factura_Apellidos'];
        $factDireccion = $_SESSION['factura_Direccion'];
        $today = new DateTime();
        $today = $today->format("Y-m-d");
        $dni=substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

        $sql="INSERT INTO 
                      facturas_misas 
                  (codigo,
                  nif, 
                  dni, 
                  precio, 
                  fecha, 
                  nombre, 
                  apellidos, 
                  direccion,
                  valida) 
              VALUES 
                  ('$codigo',
                  '$nif',
                  '$dni', 
                  '$precio', 
                  '$today',
                  '$factNombre',
                  '$factApellidos',
                  '$factDireccion',
                  0)";

        $ok = $this->querySet($sql);

        if ($ok){
            return true;
        }else{
            return false;
        }
    }

    function createFactMisaMobile($nif, $codigo, $precio = 1){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $date = new DateTime();
        $created = $date->format('Y-m-d');
        $sql="INSERT INTO 
                      fact_misas_mbile 
                      (codigo, 
                      nif, 
                      precio, 
                      created, 
                      updated) 
                  VALUES 
                      ('$codigo',  
                      '$nif',
                      '$precio', 
                      '$created', 
                      '$created'
                      )";

        $funciono=mysqli_query($conexion,$sql);
        $this->desconectarBD($conexion);
        if ($funciono){
            return true;
        }else{
            return false;
        }
    }

    function Comprobar($usuario,$password){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");

        $sql = "SELECT 
                    password, 
                    rango,
                    email, 
                    created, 
                    id,
                    roles AS roles
                FROM 
                    user 
                WHERE username = '$usuario' ";

        $result = $this->queryParamsSingle($sql);

        if(strstr($result[5], 'ROLE_USER') == true || strstr($result[5], 'ROLE_TANATORIO') == true ||
            strstr($result[5], 'ROLE_ADMIN') == true || strstr($result[5], 'ROLE_SUPERADMIN') == true) {
            $resultado = mysqli_fetch_row(mysqli_query($conexion, $sql));
            $this->desconectarBD($conexion);

            if (password_verify($password, $resultado['0'])) {
                $conexion = $this->conectarBD();
                mysqli_set_charset($conexion, "utf8");

                $sql = "SELECT
                        c.nif,
                        c.name,
                        u.cp,
                        c.id
                    FROM
                        company c
                    LEFT JOIN user AS u
                    	ON c.user_id = u.id
                    WHERE c.user_id = $resultado[4] ";

                $resultado2 = mysqli_fetch_row(mysqli_query($conexion, $sql));
                $this->desconectarBD($conexion);
                session_start();

                $_SESSION['usuario'] = $usuario;
                $_SESSION['usuario_id'] = $resultado[4];
                $_SESSION['correo'] = $resultado['2'];
                $_SESSION['rango'] = $resultado['1'];
                $_SESSION['nif'] = $resultado2['0'];
                $_SESSION['nombre'] = $resultado2['1'];
                $_SESSION['cp'] = $resultado2['2'];
                $_SESSION['empresa_id'] = $resultado2['3'];
                $_SESSION['fecha'] = $resultado['3'];
                $this->desconectarBD($conexion);
                return true;
            } else {
                $this->desconectarBD($conexion);
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Get all Devices by postal code or not
     * @param string $cpPrincipal
     * @param null $cp2
     * @param null $cp3
     * @param $SO
     * @return array
     */
    function getDevices($cpPrincipal = "", $cp2 = null, $cp3 = null, $SO) {

        if ($cpPrincipal == "") {
            // Obtengo todos los token devices
            $sql="SELECT id.token_device, id.so_device FROM info_device AS id WHERE id.so_device = '$SO' ";
        } else {
            // Obtengo solo los token devices para ese codigo_postal
            $sql="SELECT 
                        id.token_device, 
                        id.so_device 
                  FROM 
                        info_device AS id 
                  WHERE 
                        (id.cp_1 = '$cpPrincipal' 
                        OR id.cp_2 = '$cpPrincipal' 
                        OR id.cp_3 = '$cpPrincipal' ";

            if($cp2 != null){
                $sql .= " OR id.cp_1 = '$cp2' 
                        OR id.cp_2 = '$cp2' 
                        OR id.cp_3 = '$cp2'";
            }
            if($cp3 != null){
                $sql .= " OR id.cp_1 = '$cp3' 
                        OR id.cp_2 = '$cp3' 
                        OR id.cp_3 = '$cp3'";
            }

            $sql .= ")  
                    AND id.so_device = '$SO' ";
        }

        $response = $this->queryParamsArray($sql);

        return $response;
    }

    /**
     * Get android devices
     * @param $mensaje
     * @param string $cpPrincipal
     * @param null $cp2
     * @param null $cp3
     * @param string $messageBody
     */
    function sendNotificationPush($mensaje, $cpPrincipal = "", $cp2 = null, $cp3 = null, $messageBody = "") {
        $arrayDevicesAndroid = $this->getDevices($cpPrincipal, $cp2, $cp3, "Android");
        $arrayDevicesiOS = $this->getDevices($cpPrincipal, $cp2, $cp3, "iOS");

        foreach ($arrayDevicesiOS as $deviceiOS){
            $this->executeScriptPhpToSendIosNotification($deviceiOS['token_device'], $mensaje);
        }

        $tokenDevicesAndroid = [];
        foreach ($arrayDevicesAndroid as $deviceAndroid){
            $tokenDevicesAndroid[] = $deviceAndroid['token_device'];
        }

        if(!empty($tokenDevicesAndroid)){
            if(count($tokenDevicesAndroid)>1000){
                $numRequest = ceil(count($tokenDevicesAndroid) / 1000);
                for($a = 0; $a < $numRequest; $a++){
                    $devices = array_slice($tokenDevicesAndroid, ($a*1000), 1000);
                    $this->sendAndroidPush($devices, $mensaje, $messageBody.$cpPrincipal);
                }
            }else{
                $this->sendAndroidPush($tokenDevicesAndroid, $mensaje, $messageBody.$cpPrincipal);
            }
        }
    }

    function updatePublication($nombre, $apellidos, $apodo, $direccion, $localidad, $fecha_fallecimiento,
                                  $premium, $imagen, $imagen2, $conyuge, $hijos, $hijos_politicos, $hermanos, $hermanos_politicos, $familiares,
                                  $pareja, $padres, $fecha_entierro, $lugar, $hora, $texto, $f_nombre, $f_apellidos, $f_telefono,
                                  $d_recibe, $d_despide, $lugar_iglesia, $edad, $cpPrincipal, $cp2, $cp3, $arrCeremonia2 = [], $insurance,
                                    $phone1 = null, $phone2 = null){

        include_once "librerias/DataInvoice.php";
        include_once "librerias/DataUser.php";

        $code = $_SESSION['codePublication'];
        $fechaEntierro2 = "NULL";
        $horaEntierro2 = "NULL";
        $iglesiaCerem2 = "NULL";
        $localidadCerem2 = "NULL";
        $idInsurance = "NULL";

        $dataUser = new DataUser();
        $user = new User();
        $user->setUsername($_SESSION['usuario']);
        $userLogged = $dataUser->getUserByCPAndUsername($user)[0];
        $idUser = $userLogged['id'];
        $priceUser = $userLogged['price'];

        $fecha_fallecimiento = $this->formatDate($fecha_fallecimiento);
        $fecha_entierro = $this->formatDate($fecha_entierro);

        if($insurance != null){
            include_once "librerias/DataInsurance.php";
            $repoInsurance = new DataInsurance();
            $idInsurance = $repoInsurance->getInsurance($insurance);
        }

        if($premium != 0){
            $precio = $this->getElementPrice("defunciones", "premium");
        }else{
            $precio = $priceUser;
        }

        if(!empty($arrCeremonia2)){
            $localidadCerem2 = '"'.$arrCeremonia2[0].'"';
            $fechaEntierro2 = '"'.$this->formatDate($arrCeremonia2[2]).'"';
            $horaEntierro2 = '"'.$arrCeremonia2[3].'"';
            $iglesiaCerem2 = '"'.$arrCeremonia2[1].'"';
        }

        $sql = "UPDATE death SET 
                      nombre = '$nombre',
                      apellidos = '$apellidos',
                      apodo = '$apodo',
                      direccion = '$direccion', 
                      localidad = '$localidad',
                      fecha_fallecimiento = '$fecha_fallecimiento',
                      foto = '$imagen', 
                      foto2 = '$imagen2',
                      premium = '$premium', 
                      conyuge = '$conyuge',
                      hijos = '$hijos', 
                      hijos_politicos = '$hijos_politicos',
                      hermanos = '$hermanos', 
                      hermanos_politicos = '$hermanos_politicos',
                      familiares = '$familiares', 
                      pareja = '$pareja',
                      padres = '$padres', 
                      fecha_ceremonia = '$fecha_entierro',
                      localidad_ceremonia = '$lugar', 
                      hora_ceremonia = '$hora',
                      texto = '$texto', 
                      duelo_recibe = '$d_recibe',
                      duelo_despide = '$d_despide', 
                      iglesia_ceremonia = '$lugar_iglesia',
                      edad = '$edad', 
                      cp_1 = '$cpPrincipal',
                      cp_2 = '$cp2', 
                      cp_3 = '$cp3',
                      localidad_ceremonia2 = $localidadCerem2, 
                      iglesia_ceremonia2 = $iglesiaCerem2,
                      fecha_ceremonia2 = $fechaEntierro2, 
                      hora_ceremonia2 = $horaEntierro2,
                      insurance_id = $idInsurance, 
                      name = '$f_nombre', 
                      last_name = '$f_apellidos', 
                      phone = '$f_telefono', 
                      price = '$precio',
                      updated = NOW(),
                      contact_phone1 = '$phone1', 
                      contact_phone2 = '$phone2' 
                WHERE 
                      code = '$code'
                      AND user_id = '$idUser' ";

        $ok = $this->querySet($sql);

        if ($ok > -1){
            if ($premium == 1) {
                $mensaje = "Nueva defunción ...";
                $body = "Un fallecido en el código postal ";
                $this->sendNotificationPush($mensaje, "");
            }
            return true;
        }else{
            return false;
        }
    }

    function updateCeremony($nombre, $apellidos, $apodo, $edad, $fecha_fallecimiento, $premium, $conyuge, $hijos,
                            $hijos_p, $hermanos, $hermanos_p, $familiares, $pareja, $padres, $lugar, $fecha_ceremonia, $hora,
                            $aniversario, $cpPrincipal, $cp2, $cp3, $arrCeremonia2 = [], $fNombre, $fApellidos, $fTelefono){

        include_once "librerias/DataInvoice.php";
        include_once "librerias/DataUser.php";

        $code = $_SESSION['codeCeremony'];
        $localidadCerem2 = "NULL";
        $fechaCerem2 = "NULL";
        $horaCerem2 = "NULL";

        $dataUser = new DataUser();
        $user = new User();
        $user->setUsername($_SESSION['usuario']);
        $userLogged = $dataUser->getUserByCPAndUsername($user)[0];
        $idUser = $userLogged['id'];
        $priceUser = $userLogged['price'];

        $fecha_fallecimiento = $this->formatDate($fecha_fallecimiento);
        $fecha_ceremonia = $this->formatDate($fecha_ceremonia);

        if($premium != 0){
            $precio = $this->getElementPrice("misas", "premium");
        }else{
            $precio = $priceUser;
        }

        if(!empty($arrCeremonia2)){
            $localidadCerem2 = '"'.$arrCeremonia2[0].'"';
            $fechaCerem2 = '"'.$this->formatDate($arrCeremonia2[1]).'"';
            $horaCerem2 = '"'.$arrCeremonia2[2].'"';
        }

        $sql = "UPDATE ceremony SET 
                      nombre = '$nombre',
                      apellidos = '$apellidos',
                      apodo = '$apodo',
                      edad = '$edad', 
                      fecha_fallecimiento = '$fecha_fallecimiento',
                      premium = '$premium', 
                      conyuge = '$conyuge',
                      hijos = '$hijos', 
                      hijos_politicos = '$hijos_p',
                      hermanos = '$hermanos', 
                      hermanos_politicos = '$hermanos_p',
                      familiares = '$familiares', 
                      pareja = '$pareja',
                      padres = '$padres', 
                      localidad_ceremonia = '$lugar', 
                      fecha_ceremonia = '$fecha_ceremonia',
                      hora_ceremonia = '$hora',
                      aniversario = '$aniversario',
                      cp_1 = '$cpPrincipal',
                      cp_2 = '$cp2', 
                      cp_3 = '$cp3',
                      localidad_ceremonia2 = $localidadCerem2, 
                      fecha_ceremonia2 = $fechaCerem2, 
                      hora_ceremonia2 = $horaCerem2, 
                      name = '$fNombre', 
                      last_name = '$fApellidos', 
                      phone = '$fTelefono', 
                      price = '$precio',
                      updated = NOW() 
                WHERE 
                      code = '$code'
                      AND user_id = '$idUser' ";

        print_r($sql);

        $ok = $this->querySet($sql);

        if ($ok > -1){
            if ($premium == 1) {
                $mensaje = "Nueva misa ...";
                $body = "Una nueva misa en el código postal ";
                $this->sendNotificationPush($mensaje, "");
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * Insert new publicacion
     * @param $nombre
     * @param $apellidos
     * @param $apodo
     * @param $direccion
     * @param $localidad
     * @param $fecha_fallecimiento
     * @param $premium
     * @param $imagen
     * @param $imagen2
     * @param $conyuge
     * @param $hijos
     * @param $hijos_politicos
     * @param $hermanos
     * @param $hermanos_politicos
     * @param $familiares
     * @param $pareja
     * @param $padres
     * @param $fecha_entierro
     * @param $lugar
     * @param $hora
     * @param $texto
     * @param $f_nombre
     * @param $f_apellidos
     * @param $f_telefono
     * @param $f_codigo
     * @param $d_recibe
     * @param $d_despide
     * @param $lugar_iglesia
     * @param $edad
     * @param $cpPrincipal
     * @param $cp2
     * @param $cp3
     * @param array $arrCeremonia2
     * @param $insurance
     * @param null $phone1
     * @param null $phone2
     * @return bool 37
     */
    function Insertar_Publicacion($nombre, $apellidos, $apodo, $direccion, $localidad, $fecha_fallecimiento, $premium, $imagen,
                                  $imagen2, $conyuge, $hijos, $hijos_politicos, $hermanos, $hermanos_politicos, $familiares, $pareja,
                                  $padres, $fecha_entierro, $lugar, $hora, $texto, $f_nombre, $f_apellidos, $f_telefono,
                                  $f_codigo, $d_recibe, $d_despide, $lugar_iglesia, $edad, $cpPrincipal, $cp2, $cp3,
                                  $arrCeremonia2 = [], $insurance, $phone1 = null, $phone2 = null){

        include_once "librerias/DataInvoice.php";
        include_once "librerias/DataUser.php";

        $fechaEntierro2 = "NULL";
        $horaEntierro2 = "NULL";
        $iglesiaCerem2 = "NULL";
        $localidadCerem2 = "NULL";
        $idInsurance = "NULL";

        $dataUser = new DataUser();
        $user = new User();
        $user->setUsername($_SESSION['usuario']);
        $userLogged = $dataUser->getUserByCPAndUsername($user)[0];
        $idUser = $userLogged[0];
        $priceUser = $userLogged['price'];

        $fecha_fallecimiento = $this->formatDate($fecha_fallecimiento);
        $fecha_entierro = $this->formatDate($fecha_entierro);

        if($insurance != null){
            include_once "librerias/DataInsurance.php";
            $repoInsurance = new DataInsurance();
            $idInsurance = $repoInsurance->getInsurance($insurance);
        }

        if($premium != 0){
            $precio = $this->getElementPrice("defunciones", "premium");
        }else{
            $precio = $priceUser;
        }

        if(!empty($arrCeremonia2)){
            $localidadCerem2 = '"'.$arrCeremonia2[0].'"';
            $fechaEntierro2 = '"'.$this->formatDate($arrCeremonia2[2]).'"';
            $horaEntierro2 = '"'.$arrCeremonia2[3].'"';
            $iglesiaCerem2 = '"'.$arrCeremonia2[1].'"';
        }

        $sql = "INSERT INTO
                  death
                      (nombre,
                      apellidos, apodo,
                      direccion, localidad,
                      fecha_fallecimiento,
                      foto, foto2,
                      premium, conyuge,
                      hijos, hijos_politicos,
                      hermanos, hermanos_politicos,
                      familiares, pareja,
                      padres, fecha_ceremonia,
                      localidad_ceremonia, hora_ceremonia,
                      texto, duelo_recibe,
                      duelo_despide, iglesia_ceremonia,
                      edad, cp_1,
                      cp_2, cp_3,
                      localidad_ceremonia2, iglesia_ceremonia2,
                      fecha_ceremonia2, hora_ceremonia2,
                      insurance_id, created,
                      user_id, name, 
                      last_name, phone, 
                      code, price,
                      contact_phone1, contact_phone2)
                VALUES
                      ('$nombre',
                      '$apellidos','$apodo',
                      '$direccion','$localidad',
                      '$fecha_fallecimiento',
                      '$imagen', '$imagen2',
                      '$premium', '$conyuge',
                      '$hijos','$hijos_politicos',
                      '$hermanos','$hermanos_politicos',
                      '$familiares','$pareja',
                      '$padres', '$fecha_entierro',
                      '$lugar', '$hora',
                      '$texto','$d_recibe',
                      '$d_despide','$lugar_iglesia',
                      '$edad', '$cpPrincipal',
                      '$cp2', '$cp3',
                      $localidadCerem2, $iglesiaCerem2,
                      $fechaEntierro2, $horaEntierro2,
                      $idInsurance, NOW(),
                      $idUser,  '$f_nombre', 
                      '$f_apellidos', '$f_telefono', 
                      '$f_codigo', '$precio', 
                      '$phone1', '$phone2')";

        $ok = $this->querySet($sql);

        if ($ok > 0){
            if ($premium == 1) {
                $mensaje = "Nueva defunción ...";
                $body = "Un fallecido en el código postal ";
                $this->sendNotificationPush($mensaje, "");
            } else {
                $mensaje = "Nueva defunción en ".$cpPrincipal;
                $body = "Un fallecido en el código postal ";
                $this->sendNotificationPush($mensaje, $cpPrincipal, $cp2, $cp3, $body);
            }
            return true;
        }else{
            return false;
        }
    }

    function Insertar_Publicacion_misa($nombre, $apellidos, $apodo, $edad, $fecha_fallecimiento, $premium, $conyuge, $hijos,
                                       $hijos_p, $hermanos, $hermanos_p, $familiares, $pareja, $padres, $lugar, $fech_ceremonia, $hora,
                                       $aniversario, $cpPrincipal, $cp2, $cp3, $arrCeremonia2 = [], $fNombre, $fApellidos, $fTelefono, $fCodigo){

        include_once "librerias/DataInvoice.php";
        include_once "librerias/DataUser.php";

        $localidadCerem2 = "NULL";
        $fechaCerem2 = "NULL";
        $horaCerem2 = "NULL";

        $dataUser = new DataUser();
        $user = new User();
        $user->setUsername($_SESSION['usuario']);
        $userLogged = $dataUser->getUserByCPAndUsername($user)[0];
        $idUser = $userLogged['id'];
        $priceUser = $userLogged['price'];

        $fecha_fallecimiento = $this->formatDate($fecha_fallecimiento);
        $fech_ceremonia = $this->formatDate($fech_ceremonia);

        if($premium != 0){
            $precio = $this->getElementPrice("misas", "premium");
            if($precio == null) {
                $precio = $precio[1];
            }
        }else{
            $precio = $priceUser;
        }

        if(!empty($arrCeremonia2)){
            $localidadCerem2 = '"'.$arrCeremonia2[0].'"';
            $fechaCerem2 = '"'.$this->formatDate($arrCeremonia2[1]).'"';
            $horaCerem2 = '"'.$arrCeremonia2[2].'"';
        }

        $sql = "INSERT INTO 
                      ceremony
                      (nombre,
                      user_id, 
                      apellidos, 
                      apodo, 
                      edad, 
                      fecha_fallecimiento, 
                      premium, 
                      conyuge, 
                      hijos, 
                      hijos_politicos, 
                      hermanos, 
                      hermanos_politicos, 
                      familiares, 
                      pareja, 
                      padres, 
                      localidad_ceremonia, 
                      fecha_ceremonia, 
                      hora_ceremonia, 
                      aniversario,
                      cp_1,
                      cp_2,
                      cp_3,
                      localidad_ceremonia2,
                      fecha_ceremonia2,
                      hora_ceremonia2,
                      paid, created, name, 
                      last_name, phone, 
                      code, price )
              VALUES (
                      '$nombre',
                      '$idUser',
                      '$apellidos',
                      '$apodo', 
                      $edad,
                      '$fecha_fallecimiento',
                      '$premium',
                      '$conyuge',
                      '$hijos',
                      '$hijos_p',
                      '$hermanos',
                      '$hermanos_p',
                      '$familiares',
                      '$pareja',
                      '$padres',
                      '$lugar',
                      '$fech_ceremonia',
                      '$hora',
                      '$aniversario',
                      '$cpPrincipal',
                      '$cp2', 
                      '$cp3',
                      $localidadCerem2,
                      $fechaCerem2,
                      $horaCerem2,
                      1, NOW(), '$fNombre', 
                      '$fApellidos', '$fTelefono', 
                      '$fCodigo', '$precio')";

        $ok = $this->querySet($sql);

        if ($ok){
            if ($premium == 1) {
                $mensaje = "Nueva misa ...";
                $this->sendNotificationPush($mensaje, "");
            } else {
                $mensaje = "Nueva misa en ".$cpPrincipal;
                $body = "Una nueva misa en el código postal ";
                $this->sendNotificationPush($mensaje, $cpPrincipal, $cp2, $cp3, $body);
            }
            return true;
        }else{
            return false;
        }
    }


    function Conseguir_Publicaciones_propias($nif){
        $sql="SELECT publicacion.nombre, publicacion.dni, fallecimiento, foto, premiun, ceremonia, lugar, hora, texto FROM publicacion 
              INNER JOIN facturas on publicacion.dni=facturas.dni
              INNER JOIN empresas on facturas.nif=empresas.NIF
              where empresas.NIF='$nif' ORDER BY facturas.fecha DESC;";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);

        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;

        }

        $this->desconectarBD($conexion);

        return $array;
    }
    function Conseguir_Publicaciones_propias_misas($nif){
        $sql="SELECT publicacion_misa.nombre, publicacion_misa.dni, fecha_fallecimiento, aniversario, premiun, fecha_ceremonia, lugar, hora FROM publicacion_misa 
              INNER JOIN facturas_misas on publicacion_misa.dni=facturas_misas.dni
              INNER JOIN empresas on facturas_misas.nif=empresas.NIF
              where empresas.NIF='$nif' ORDER BY facturas_misas.fecha DESC;";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);

        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;

        }

        $this->desconectarBD($conexion);

        return $array;
    }
    function Conseguir_Facturas(){
        $sql="SELECT * FROM facturas WHERE fecha>'0000-00-00' ORDER BY facturas.fecha DESC";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);

        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;

        }

        $this->desconectarBD($conexion);

        return $array;
    }
    function Conseguir_Facturas_misas(){
        $sql="SELECT * FROM facturas_misas WHERE fecha>'0000-00-00' ORDER BY facturas_misas.fecha DESC";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);

        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;

        }

        $this->desconectarBD($conexion);

        return $array;
    }

    function Conseguir_Publicidad(){
        $sql="SELECT * FROM publicidad";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);

        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;
        }

        $this->desconectarBD($conexion);

        return $array;
    }

    function Conseguir_Usuarios(){

        $sql = "SELECT 
                    c.name,
                    c.name_contact,
                    u.localidad,
                    u.cp,
                    c.address,
                    c.account,
                    u.username,
                    c.phone1,
                    u.enabled,
                    u.created,
                    u.rango,
                    c.nif,
                    u.id
                FROM user u 
                LEFT JOIN company AS c 
                ON u.id = c.user_id
                WHERE u.roles = 'ROLE_TANATORIO' ";

        $result = $this->queryParamsArray($sql);

        return $result;
    }
    function Conseguir_Publicacion($dni){
        $sql="SELECT * FROM publicacion WHERE dni='$dni'";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);
        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;

        }

        $this->desconectarBD($conexion);
        return $array;
    }
    function Conseguir_Factura($dni,$nif){
        $sql="SELECT publicacion.nombre,publicacion.apellidos, publicacion.dni,empresas.Usuario,empresas.NIF,empresas.CP,facturas.fecha,publicacion.premiun,facturas.precio, facturas.codigo FROM publicacion
                INNER JOIN facturas on publicacion.dni=facturas.dni
                INNER JOIN empresas on facturas.nif=empresas.NIF
                where publicacion.dni='$dni' and empresas.NIF = '$nif';";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);
        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;
        }
        $this->desconectarBD($conexion);
        return $array;
    }

    function Descargar_Publicaciones_CP($cp){
        $sql = "SELECT 
                      p.id AS identificador, 
                      p.nombre, 
                      p.apellidos, 
                      p.apodo,
                      p.direccion,
                      DATE_FORMAT(p.fecha_fallecimiento, '%d-%m-%Y') AS fallecimiento, 
                      p.conyuge,
                      p.conyuge AS marido, p.conyuge AS esposa,
                      p.hijos, 
                      p.hijos_politicos, 
                      p.hermanos, 
                      p.hermanos_politicos, 
                      p.familiares, 
                      p.pareja, 
                      p.foto, 
                      p.foto2, 
                      DATE_FORMAT(p.fecha_ceremonia, '%d-%m-%Y') AS ceremonia,  
                      p.localidad_ceremonia AS lugar, 
                      p.hora_ceremonia AS hora, 
                      p.texto, 
                      p.duelo_recibe AS d_recibe, 
                      p.duelo_despide AS d_despide, 
                      p.iglesia_ceremonia AS lugar_iglesia, 
                      p.edad,
                      p.localidad_ceremonia2 AS localidad_cerem2,
                      p.iglesia_ceremonia2 AS iglesia_cerem2,
                      DATE_FORMAT(p.fecha_ceremonia2, '%d-%m-%Y') AS fecha_cerem2,
                      p.hora_ceremonia2 AS hora_cerem2,
                      p.padres,
                      p.contact_phone1,
                      p.contact_phone2
                  FROM 
                      death AS p 
                  WHERE ";

        if($cp != "" && $cp != null){
            $sql .= " (((p.cp_1 = '$cp'
                       OR p.cp_2 = '$cp'
                       OR p.cp_3 = '$cp') AND 
                       p.premium = 0 )
                       OR (p.premium = 1))";
        }else{
            $sql .= " p.premium = 1 ";
        }

        // Se muestran las publicaciones cuya ceremonia sea a partir de mañana u hoy si no ha pasado hora y media desde su celebracion
        $sql .= " AND (DATEDIFF(CURDATE(), p.fecha_ceremonia) < 0 OR (DATEDIFF(CURDATE(), p.fecha_ceremonia) < 1 
                    AND ADDTIME(p.hora_ceremonia, '1:31:00') > DATE_FORMAT(NOW(), '%H:%i:%s')))
                  ORDER BY 
                      p.fecha_fallecimiento DESC, 
                      p.fecha_ceremonia DESC ";

        $result = $this->queryParamsArray($sql);
        return json_encode($result);
    }

    /**
     * Get misas by postal code
     * @param $cp
     * @param null $idMisa
     * @param null $hour
     * @return array|string
     */
    function Descargar_Publicaciones_CP_misas($cp){
        $sql="SELECT 
                    pm.id AS identificador,
                    pm.nombre, 
                    pm.apellidos,
                    pm.apodo, 
                    DATE_FORMAT(pm.fecha_fallecimiento, '%d-%m-%Y') as fecha_fallecimiento, 
                    pm.conyuge,
                    pm.viuda,
                    pm.marido AS esposo,
                    pm.marido,
                    pm.hijos, 
                    pm.hijos_politicos,  
                    pm.hermanos, 
                    pm.hermanos_politicos,
                    pm.familiares,
                    pm.pareja, 
                    DATE_FORMAT(pm.fecha_ceremonia,'%d-%m-%Y') as fecha_ceremonia, 
                    pm.localidad_ceremonia AS lugar, 
                    pm.hora_ceremonia AS hora,
                    pm.edad,
                    pm.aniversario,
                    pm.localidad_ceremonia2 AS lugar2,
                    DATE_FORMAT(pm.fecha_ceremonia2,'%d-%m-%Y') as fecha_cerem2,
                    pm.hora_ceremonia2 AS hora2,
                    pm.padres
                FROM 
                    ceremony AS pm 
                WHERE 
                    pm.paid = 1 AND ";

        if($cp != "" && $cp != null){
            $sql .= " (((pm.cp_1 = '$cp'
                       OR pm.cp_2 = '$cp'
                       OR pm.cp_3 = '$cp') AND 
                       pm.premium = 0 )
                       OR (pm.premium = 1))";
        }else{
            $sql .= " pm.premium = 1 ";
        }

        // Se muestran las misas cuya ceremonia sea a partir de mañana u hoy si no ha pasado hora y media desde su celebracion
        $sql .= " AND (DATEDIFF(CURDATE(), pm.fecha_ceremonia) < 0  OR (DATEDIFF(CURDATE(), pm.fecha_ceremonia) < 1 
        AND ADDTIME(pm.hora_ceremonia, '1:31:00') > DATE_FORMAT(NOW(), '%H:%i:%s')))
                  ORDER BY 
                      pm.fecha_fallecimiento DESC, 
                      pm.fecha_ceremonia DESC ";

        $result = $this->queryParamsArray($sql);
        return json_encode($result);

    }
    function Descargar_Publicaciones_Global(){
        $sql = "SELECT 
                      p.id AS identificador, 
                      p.nombre, 
                      p.apellidos, 
                      p.apodo,
                      p.direccion,
                      DATE_FORMAT(p.fecha_fallecimiento, '%d-%m-%Y') AS fallecimiento, 
                      p.conyuge,
                      p.conyuge AS marido,
                      p.hijos, 
                      p.hijos_politicos, 
                      p.hermanos, 
                      p.hermanos_politicos, 
                      p.familiares, 
                      p.pareja, 
                      p.foto, 
                      p.foto2, 
                      DATE_FORMAT(p.fecha_ceremonia, '%d-%m-%Y') AS ceremonia,  
                      p.localidad_ceremonia AS lugar, 
                      p.hora_ceremonia AS hora, 
                      p.texto, 
                      p.duelo_recibe AS d_recibe, 
                      p.duelo_despide AS d_despide, 
                      p.iglesia_ceremonia AS lugar_iglesia, 
                      p.edad,
                      p.localidad_ceremonia2 AS localidad_cerem2,
                      p.iglesia_ceremonia2 AS iglesia_cerem2,
                      DATE_FORMAT(p.fecha_ceremonia2, '%d-%m-%Y') AS fecha_cerem2,
                      p.hora_ceremonia2 AS hora_cerem2,
                      p.padres, 
                      p.contact_phone1,
                      p.contact_phone2
                  FROM 
                      death AS p
                  WHERE p.premium = 1 
                      AND DATEDIFF(CURDATE(), p.fecha_fallecimiento) < 2
                  ORDER BY 
                      p.fecha_fallecimiento DESC, 
                      p.fecha_ceremonia DESC";
        $result = $this->queryParamsArray($sql);
        return json_encode($result);
    }

    function Descargar_Publicaciones_Global_misas(){
        $sql="SELECT 
                    pm.identificador,
                    pm.nombre, 
                    pm.apellidos,
                    pm.apodo, 
                    DATE_FORMAT(pm.fecha_fallecimiento,'%d-%m-%Y') as fecha_fallecimiento, 
                    pm.conyuge,
                    pm.conyuge AS esposo,
                    pm.hijos, 
                    pm.hijos_politicos, 
                    pm.hermanos, 
                    pm.hermanos_politicos,
                    pm.familiares,
                    pm.pareja, 
                    DATE_FORMAT(pm.fecha_ceremonia,'%d-%m-%Y') as fecha_ceremonia, 
                    pm.lugar, 
                    pm.hora,
                    pm.edad,
                    pm.aniversario,
                    pm.lugar2,
                    DATE_FORMAT(pm.fecha_cerem2,'%d-%m-%Y') as fecha_cerem2,
                    pm.hora2,
                    pm.padres
                FROM 
                    publicacion_misa AS pm 
                WHERE 
                    pm.paid = 1 
                    AND premiun = 1 
                    AND DATEDIFF(CURDATE(), pm.fecha_ceremonia) < 1
                ORDER BY 
                    pm.fecha_ceremonia DESC";

        $result = $this->queryParamsArray($sql);
        return json_encode($result);

    }
    function Descargar_Publicidad($cp){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="SELECT * FROM publicidad WHERE codigo_postal='$cp'";
        $resultado = mysqli_query($conexion,$sql);
        $array = array();
        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;
        }
        $this->desconectarBD($conexion);
        return json_encode($array);
    }
    function CP_Esta($cp){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="SELECT CP FROM empresas where cp='$cp'";
        $resultado = mysqli_query($conexion,$sql);
        $array = array();
        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;
        }
        $this->desconectarBD($conexion);
        return json_encode($array);
    }
    function Codigo_Version(){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="SELECT codigo FROM codigo_version";
        $resultado = mysqli_query($conexion,$sql);
        $array = array();
        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;
        }
        $this->desconectarBD($conexion);
        return json_encode($array);
    }
    function Insertar_Publicidad($cp,$nombre,$imagen, $cp2 = "", $cp3 = "", $url = "", $premium = 0){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="INSERT INTO publicidad(codigo_postal,nombre, imagen, cp_2, cp_3, url, premium) 
              VALUES ('$cp','$nombre','$imagen', '$cp2', '$cp3', '$url', '$premium')";

        $funciono=mysqli_query($conexion,$sql);
        $this->desconectarBD($conexion);
        if ($funciono){
            return true;
        }else{
            return false;
        }
    }
    function Eliminar_Publicidad($codigo){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="DELETE FROM publicidad WHERE codigo='$codigo'";
        $funciono=mysqli_query($conexion,$sql);
        $this->desconectarBD($conexion);
        if ($funciono){
            return true;
        }else{
            return false;
        }
    }

    function Eliminar_Usuario($usuario){
        $sql = "DELETE FROM user WHERE user.username = '$usuario'";

        return $this->querySet($sql);
    }

    function Insertar_CP_faltante($cp){
        $conexion = $this->conectarBD();

        mysqli_set_charset($conexion,"utf8");
        $sql="INSERT INTO cp_sin_servicio(cp) VALUES ('$cp')";
        mysqli_query($conexion,$sql);
        $this->desconectarBD($conexion);
    }
    function Insertar_CP_existente($cp){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="INSERT INTO cp_con_servicio(cp) VALUES ('$cp')";
        mysqli_query($conexion,$sql);
        $this->desconectarBD($conexion);
    }

    function executeScriptPhpToSendIosNotification($deviceToken,$message) {
        $url = "https://auroraservicios.es/librerias/Send_ios_notification.php";
        $postdata = http_build_query(
            array(
                'tokenDevice' => $deviceToken,
                'message' => $message
            )
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context  = stream_context_create($opts);

        $result = file_get_contents($url, false, $context);
    }

    /**
     * Send notification with title and body to many devices
     * @param null $devices
     * @param $title
     * @param $body
     * @return mixed
     */
    function sendAndroidPush($devices = null, $title, $body) {

        // API access key from Google API's Console
        // Se debe poner la API de "Credenciales del proyecto"
        define('API_ACCESS_KEY',
            "AAAA14oj3yU:APA91bHKBmIeYZU91wtDr1F_mOOJites5Cue4LV9-t9Aay_Po6nsZl9LBf5njjtGHcrcIDKir4mxv7PGuMVlL8uOV2G5cKBthEGRlkfSg4u05U5FstOmh0c_QyPedaO-X8LmPntAcrir" ); //Falta defiinir la api key de la app de android

        $fcmUrl = "https://fcm.googleapis.com/fcm/send";

        $fields = array(
            "registration_ids" => $devices,
            "priority" => "normal",
            "notification" => [
                "title" => $title,
                "body" => $body
            ],
            "data" => [
                "id" => 156416,
                "missedRequests" => 5,
                "addAnyDataHere" => 123
            ],
            "action_destination" => "http://androiddeft.com"
        );

        $headers = array(
            "Authorization:key=" . API_ACCESS_KEY,
            "Content-Type:application/json"
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);

        curl_close($ch);
        return $result;
    }


    /**
     * Test notification
     *
     * @param $token
     * @param $title
     * @param $body
     * @param $SO
     * @return mixed
     */
    public function sendNotificationPushTest($token, $title, $body, $SO)
    {
        $ch = curl_init("https://fcm.googleapis.com/fcm/send"); //The device token.

        $notification = array(
            'title' =>$title ,
            'text' => $body
        ); //This array contains, the token and the notification. The 'to' attribute stores the token.

        $arrayToSend = array(
            'to' => $token,
            'notification' => $notification,
            'priority'=>'high'
        ); //Generating JSON encoded string form the above array.

        $json = json_encode($arrayToSend); //Setup headers:
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key= $key'; // key here //Setup curl, add headers and post parameters.
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers); //Send the request
        $response = curl_exec($ch); //Close request
        curl_close($ch);

        return $response;
    }

    function send_iOS_Push($deviceToken,$message){
        $body['aps'] = array(
            'alert' => trim($message),
            'sound' => 'default',
            'badge' => 1
        );
        // Encode the payload as JSON
        $payload = json_encode($body);

        $msg = chr(0) . pack('n', 32) . pack('H*', trim($deviceToken)) . pack('n', strlen($payload)) . $payload;
        // For sandbox
//        $url = 'ssl://gateway.sandbox.push.apple.com:2195';

        // For production
        $url = 'ssl://gateway.push.apple.com:2195';


        $passphrase = '';
        //echo $passphrase;
        $ctx = stream_context_create();
        //stream_context_set_option($ctx, 'ssl', 'local_cert', '../librerias/devAPNSAurora.pem');
        stream_context_set_option($ctx, 'ssl', 'local_cert', '/librerias/prodAPNSAurora.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Connection to APNS server
        $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp) {
            echo "<br>Failed to connect in notification server : $err $errstr" . PHP_EOL;
        }

        $result = fwrite($fp, $msg, strlen($msg));
        if (!$result){
            //echo '<br>Message not delivered' . PHP_EOL;
        } else {
            //echo '<br>Message successfully delivered' . PHP_EOL;
            return $result;
        }
        // Close the connection to the server
        fclose($fp);
    }


    function Conseguir_Ultimo_Codigo_Factura($nif){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="SELECT MAX(codigo) FROM facturas where nif='$nif'";

        $resultado = mysqli_fetch_row(mysqli_query($conexion,$sql));

        $this->desconectarBD($conexion);
        switch (strlen($resultado[0])){
            case 1:
                return "000".$resultado[0];
                break;
            case 2:
                return "00".$resultado[0];
                break;
            case 3:
                return "0".$resultado[0];
                break;
        }
        return $resultado[0];
    }
    function Conseguir_Ultimo_Codigo_Factura_misa($nif){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="SELECT MAX(codigo) FROM facturas_misas where nif='$nif'";

        $resultado = mysqli_fetch_row(mysqli_query($conexion,$sql));

        $this->desconectarBD($conexion);
        switch (strlen($resultado[0])){
            case 1:
                return "000".$resultado[0];
                break;
            case 2:
                return "00".$resultado[0];
                break;
            case 3:
                return "0".$resultado[0];
                break;
        }
        return $resultado[0];
    }
    function Conseguir_CP_Faltantes(){
        $sql="SELECT cp,COunt(cp) FROM cp_sin_servicio group by cp ORDER BY COunt(cp) DESC";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);

        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;

        }

        $this->desconectarBD($conexion);

        return $array;
    }
    function Conseguir_CP_Existetes(){
        $sql="SELECT cp,COunt(cp) FROM cp_con_servicio group by cp ORDER BY COunt(cp) DESC";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);

        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;

        }

        $this->desconectarBD($conexion);

        return $array;
    }
    function Eliminar_Cp_Faltante($cp){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="DELETE FROM cp_sin_servicio WHERE cp='$cp'";
        $funciono=mysqli_query($conexion,$sql);
        $this->desconectarBD($conexion);
        if ($funciono){
            return true;
        }else{
            return false;
        }
    }

    private function updatePassword($usuario, $newPass){

        $sql = "UPDATE 
                      user 
                SET password ='".password_hash($newPass, PASSWORD_BCRYPT)."' 
                WHERE username = '$usuario'";

        $ok = $this->querySet($sql);

        if ($ok || $ok === 0){
            return true;
        }else{
            return false;
        }
    }

    function changePassword($usuario, $actualPass, $newPass){

        $sql = "SELECT password FROM user WHERE username = '$usuario'";

        $error = 0;
        $resultado = $this->queryParamsSingle($sql);

        if (password_verify($actualPass, $resultado['0'])){
            $r = $this->updatePassword($usuario, $newPass);
            if (!$r){
                $error = 2;
            }
        } else{
            $error = 1;
        }
        return $error;
    }
    function Conseguir_Ultimo_Codigo_Publicacion_CP($cp){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="SELECT MAX(publicacion.identificador) as codigo FROM publicacion INNER JOIN facturas on publicacion.dni=facturas.dni INNER JOIN empresas on facturas.nif=empresas.NIF where empresas.CP='$cp' and premiun=0 and DATEDIFF(CURDATE(),fallecimiento) <2";

        $resultado = mysqli_query($conexion,$sql);

        $this->desconectarBD($conexion);
        $array = array();
        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;
        }
        $this->desconectarBD($conexion);
        return json_encode($array);
    }
    function Conseguir_Ultimo_Codigo_Publicacion_CP_misa($cp){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="SELECT MAX(publicacion_misa.identificador) as codigo FROM publicacion_misa INNER JOIN facturas_misas on publicacion_misa.dni=facturas_misas.dni INNER JOIN empresas on facturas_misas.nif=empresas.NIF where empresas.CP='$cp' and premiun=0 and DATEDIFF(CURDATE(),fecha_ceremonia) <1";

        $resultado = mysqli_query($conexion,$sql);

        $this->desconectarBD($conexion);
        $array = array();
        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;
        }
        $this->desconectarBD($conexion);
        return json_encode($array);
    }
    function Conseguir_Ultimo_Codigo_Publicacion_Global(){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="SELECT MAX(publicacion.identificador) as codigo FROM publicacion where premiun=1 and DATEDIFF(CURDATE(),fallecimiento) <2 ORDER BY publicacion.fallecimiento DESC";
        $resultado = mysqli_query($conexion,$sql);
        $array = array();
        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;
        }
        $this->desconectarBD($conexion);
        return json_encode($array);
    }
    function Conseguir_Ultimo_Codigo_Publicacion_Global_misa(){
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $sql="SELECT MAX(publicacion_misa.identificador) as codigo FROM publicacion_misa where premiun=1 and DATEDIFF(CURDATE(),fecha_ceremonia) <1 ORDER BY publicacion_misa.fecha_ceremonia DESC";
        $resultado = mysqli_query($conexion,$sql);
        $array = array();
        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;
        }
        $this->desconectarBD($conexion);
        return json_encode($array);
    }
    function Conseguir_Facturas_Usuarios($usuario,$fecha_inicio,$fecha_fin){
        $sql="select facturas.codigo,facturas.nif,facturas.dni,facturas.precio,facturas.fecha,facturas.nombre,facturas.apellidos,facturas.direccion from facturas
              inner JOIN empresas on empresas.NIF=facturas.nif
              inner JOIN usuarios on empresas.Usuario=usuarios.usuario
              where usuarios.usuario='$usuario' and facturas.fecha>='$fecha_inicio' and facturas.fecha<='$fecha_fin'";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);

        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;

        }

        $this->desconectarBD($conexion);

        return $array;
    }
    function Conseguir_Facturas_Usuarios_misas($usuario,$fecha_inicio,$fecha_fin){
        $sql="select facturas_misas.codigo,facturas_misas.nif,facturas_misas.dni,facturas_misas.precio,facturas_misas.fecha,facturas_misas.nombre,facturas_misas.apellidos,facturas_misas.direccion from facturas_misas 
                inner JOIN empresas on empresas.NIF=facturas_misas.nif 
                inner JOIN usuarios on empresas.Usuario=usuarios.usuario 
                where usuarios.usuario='$usuario' and facturas_misas.fecha>='$fecha_inicio' and facturas_misas.fecha<='$fecha_fin'";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);

        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;

        }

        $this->desconectarBD($conexion);

        return $array;
    }


    function Conseguir_Datos_Empresa($usuario){
        $sql="select empresas.* from empresas inner JOIN usuarios on empresas.Usuario=usuarios.usuario where usuarios.usuario='$usuario'";
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $resultado = mysqli_query($conexion,$sql);

        $array = array();

        while ($fila = mysqli_fetch_array($resultado)){
            $array[] = $fila;

        }

        $this->desconectarBD($conexion);

        return $array;
    }

    /**
     * Modify price for element
     * @param $user
     * @param $element
     * @param $price
     * @param $type
     * @return bool
     */
    function setPriceElement($user, $element, $price, $type){

        $result = $this->getElementPrice($element, $type);

        if(!empty($result)){
            $sql = "UPDATE
                  precios
              SET precio = '$price', tipo = '$type', usuario_id = '$user', updated = NOW()
              WHERE elemento = '$element'
                  AND tipo = '$type'";
        }else{
            $sql = "INSERT INTO
                        precios (elemento, precio, tipo, usuario_id)
                    VALUES ('$element', '$price', '$type', '$user')";
        }

        $ok = $this->querySet($sql);

        if ($ok){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Get all elements and your prices
     * @return array
     */
    function getElementsAndPrices(){
        $sql = "SELECT 
                    p.elemento,
                    p.tipo,
                    p.precio, 
                    DATE_FORMAT(p.updated, '%d-%m-%Y') AS updated,
                    u.usuario AS usuario 
                FROM 
                    precios AS p
                INNER JOIN usuarios AS u 
                    ON p.usuario_id = u.id ";
        $response = $this->queryParamsArray($sql);
        return $response;
    }

    /**
     * Get element by table precios
     * @param $element
     * @return array
     */
    function getElementPrice($element, $type){
        $sql = "SELECT 
                    p.elemento,
                    p.precio,
                    p.tipo
                FROM 
                    precios AS p
                WHERE p.elemento = '$element' 
                    AND p.tipo = '$type'";

        $response = $this->queryParamsArray($sql);
        if($response != null){
            $response = $response[0];
        }
        return $response;
    }

    /**
     * Get user logged
     * @return array
     */
    function getUserLogged(){
        $user = trim($_SESSION["usuario"]);
        $sql = "SELECT 
                    u.id,
                    u.usuario 
                FROM 
                    usuarios AS u
                WHERE 
                    u.usuario = '$user' ";

        $response = $this->queryParamsArray($sql);
        return $response;
    }

    /**
     * Get user logged
     * @return array
     */
    public function getCompanyLogged(){
        $company = trim($_SESSION["empresa_id"]);
        $sql = "SELECT 
                    e.premium,
                    e.id
                FROM 
                    empresas AS e
                WHERE 
                    e.id = '$company' ";

        $response = $this->queryParamsArray($sql);
        return $response;
    }

    /**
     * Get a row result by query
     *
     * @param $query
     * @param array $params
     * @return array
     */
    public function queryParamsSingle($query)
    {
        $conexion = $this->conectarBD();
        $row = mysqli_fetch_row(mysqli_query($conexion, $query));
        return $row;
    }

    /**
     * Get many rows result by query
     *
     * @param $query
     * @param array $params
     * @return array
     */
    public function queryParamsArray($query)
    {
        $conexion = $this->conectarBD();
        mysqli_set_charset($conexion,"utf8");
        $rowsResult = [];
        $results = mysqli_query($conexion, $query);
        while ($row = mysqli_fetch_array($results)){
            $rowsResult[] = $row;
        }
        return $rowsResult;
    }

    /**
     * Insert or update item to table
     * @param $sql
     * @return bool
     */
    public function querySet($sql){
        $conn = $this->conectarBD();
        mysqli_set_charset($conn,"utf8");
        $ok = mysqli_query($conn, $sql);
        if ($ok){
            return $conn->insert_id;
        }else{
            return false;
        }
    }

    public function lastInsertId(){
        $conn = $this->conectarBD();
        mysqli_set_charset($conn,"utf8");
        return $conn->insert_id;
    }

    /**
     * @param $date
     * @return bool|DateTime
     */
    private function formatDate($date){
        $arrDate = explode("-", $date);
        return $arrDate[2]."-".$arrDate[1]."-".$arrDate[0];
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 6/04/19
 * Time: 14:28
 */

use modelo\Invoice;

require('fpdf181/fpdf.php');
include_once "librerias/DataInvoice.php";
include_once "modelo/Invoice.php";


class InvoiceController extends FPDF
{

    private $count = 0;
    private $totalPages = 1;
    private $subtotal = 0;

    public function generarFactura($options = []){

        $maxRowPerPage = 6;

        $this->AddPage();

        $this->SetMargins(14,14,14);
        $this->cMargin = 4;
        $this->SetAutoPageBreak(false);

        // Logo y numero de factura
        $this->Image('imagenes/titulo_2.png' , 14 , 15, 75 , 19,'PNG', 'https://auroraservicios.es/');
        $this->SetFont('Arial','B',17);
        $this->Cell(122,15,"", 0, 1);
        $this->Cell(117,15,"", 0);
        $text = 'Factura Nº: '.$options['number_invoice'];
        $txt = iconv('utf-8', 'cp1252', $text);
        $this->Cell(68,7,$txt, 0, 1, 'R');
        $this->Cell(60, 18,"", 0, 1);

        // Primera linea
        $text = $options['aurora']['name'];
        $txt = iconv('utf-8', 'cp1252', $text);
        $this->Cell(70,7,$txt, 0, 0);
        $text = $options['company']['company'];
        $txt = iconv('utf-8', 'cp1252', $text);
        $this->Cell(114,7, $txt, 0, 1, 'R');

        // Segunda linea
        $this->SetFont('Arial','B',12);
        $text = $options['aurora']['subname'];
        $txt = iconv('utf-8', 'cp1252', $text);
        $this->Cell(70,7,$txt, 0, 0);
        $text = $options['company']['address'];
        $txt = iconv('utf-8', 'cp1252', $text);
        $this->Cell(114,7,$txt, 0, 1, 'R');

        // Tercera linea
        $this->Cell(120, 7,date('d/m/Y'), 0, 0);
        $this->Cell(63, 7, $options['company']['nif'], 0, 1, "R");

        // Cuarta linea
        $this->Cell(0, 7, "", 0, 1);

        // Quinta linea
        $text = $options['aurora']['address'];
        $txt = iconv('utf-8', 'cp1252', $text);
        $this->Cell(70,7,$txt, 0, 0);
        $text = $options['company']['company_name_contact'];
        $txt = iconv('utf-8', 'cp1252', $text);
        $this->Cell(114,7,$txt, 0, 1, 'R');

        // Sexta linea
        $text = $options['aurora']['phone1'];
        $txt = iconv('utf-8', 'cp1252', $text);
        $this->Cell(70,7,$txt, 0, 0);
        $text = $options['company']['phone'];
        $txt = iconv('utf-8', 'cp1252', $text);
        $this->Cell(114,7,$txt, 0, 1, 'R');

        $this->Cell(120,10,"", 0, 1);

        $this->createHeaderTable();
        $product = [];
        $products = [];
        for($i = 0; $this->count < count($options['productos']); $i++){
            $product[0] = $options['productos'][$this->count]['producto'];
            $product[1] = $options['productos'][$this->count]['cantidad'];
            $product[2] = $options['productos'][$this->count]['precio'];
            array_push($products, $product);
            $this->count++;
            if($this->count >= count($options['productos']) || ($this->count > 0 && $this->count % $maxRowPerPage == 0)){
                break;
            }
        }

        if($this->totalPages > 1 &&
            ($this->count == count($options['productos']))){
            $this->createBodyTable($products, $options['aurora']['iva']);
            $txt = iconv('utf-8','cp1252', round(($this->subtotal*1.21), 2)." €");
            $this->Cell(93,15, "",0,0,'L');
            $this->Cell(60,15, "TOTAL",0,0,'R');
            $this->Cell(30,15, $txt,'LRTB',0,'R');
        }else{
            $this->createBodyTable($products, $options['aurora']['iva']);
        }

        if(count($options['productos']) < $maxRowPerPage){
            $txt = iconv('utf-8','cp1252', round(($this->subtotal*1.21), 2)." €");
            $this->Cell(93,15, "",0,0,'L');
            $this->Cell(60,15, "TOTAL",0,0,'R');
            $this->Cell(30,15, $txt,'LRTB',0,'R');
        }

        $this->createFooter($options['aurora']);

        if(count($options['productos']) > $maxRowPerPage && $this->count < count($options['productos'])){
            $this->totalPages++;
            $this->generarFactura($options);
        }

        $company = str_replace(' ', '', $options['company']['company']);
        
        $this->Output("fac_aurora_".date('dmY')."_".$company.".pdf", "I");
    }

    private function createHeaderTable()
    {
        $this->SetFillColor(0,0,100);
        $this->SetTextColor(255,255,255);
        $this->SetFont('Arial','B',11);
        $this->SetDrawColor(0,0,100);
        $this->SetLineWidth(.3);
        $text = "Descripción";
        $txt = iconv('utf-8', 'cp1252', $text);
        $this->Cell(93,7, $txt,1,0,'C',1);
        $header=array('Cantidad','P/U', 'Importe');

        //Colores, ancho de línea y fuente en negrita
        $this->SetFillColor(0,0,100);
        $this->SetTextColor(255,255,255);
        $this->SetDrawColor(0,0,100);
        $this->SetLineWidth(.3);
        $this->SetFont('','B');
        //Cabecera
        for($i=0;$i<count($header);$i++){
            $this->Cell(30,7,$header[$i],1,0,'C',1);
        }

    }

    private function createBodyTable($products, $iva)
    {
        $this->Ln();
        //Restauración de colores y fuentes
        $this->SetFillColor(224,235,255);
        $this->SetTextColor(0);
        $this->SetFont('');
        $fill=false;

        $totalPrice = 0;

        for($i = 0; $i < count($products); $i++){
            if($i%2 == 0){
                $fill = false;
            }else{
                $fill = true;
            }

            $txt = iconv('utf-8', 'cp1252', $products[$i][0]);
            $this->Cell(93,10, $txt,'LR',0,'L',$fill);
            $txt2 = iconv('utf-8', 'cp1252', $products[$i][1]);
            $this->Cell(30,10,$txt2,'LR',0,'R',$fill);
            if($products[$i][2] != ""){
                $q = $products[$i][2];
            }else{
                $q = 0;
            }
            $txt3 = iconv('utf-8', 'cp1252', $q." €");
            $this->Cell(30,10,$txt3,'LR',0,'R',$fill);
            $totalProduct = is_numeric($products[$i][1]) && is_numeric($products[$i][2]) ? ($products[$i][2]* $products[$i][1]) : "";
            if($totalProduct != ""){
                $x = $totalProduct;
            }else{
                $x = 0;
            }
            $txt3 = iconv('utf-8', 'cp1252', $x." €");
            $this->Cell(30,10,$txt3,'LR',0,'R',$fill);
            $totalPrice += $x;

            $this->Ln();
        }

        $txt = iconv('utf-8','cp1252', round($totalPrice,2)." €");

        $this->Cell(93,15, "",'T',0,'L',$fill != $fill);
        $this->Cell(60,15, "Subtotal (Sin IVA)",'TR',0,'R');
        $this->Cell(30,15, $txt,'LRT',1,'R');

        $txt = iconv('utf-8','cp1252', round(($totalPrice*0.21), 2)." €");
        $this->Cell(93,15, "",0,0,'L',$fill != $fill);
        $this->Cell(60,15, "IVA ".$iva."%",'R',0,'R');
        $this->Cell(30,15, $txt,'LRT',1,'R');

        $txt = iconv('utf-8','cp1252', round(($totalPrice*1.21), 2)." €");
        $this->Cell(93,15, "",0,0,'L',$fill != $fill);
        $this->Cell(60,15, "Subtotal",0,0,'R');
        $this->Cell(30,15, $txt,'LRTB',0,'R');

        $this->subtotal += $totalPrice;
        $this->Ln();
    }

    private function createFooter($options = null){

        $conditionsPay = $options['text_conditions_pay'];
        $timePay = $options['days_pay'];
        $iban = $options['iban'];


        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(.6);
        $this->SetY(-65);
        $this->Cell(0, 5, "", 'T', 1);
        $txt = iconv('utf-8', 'cp1252', $conditionsPay);
        $this->Cell(70,8, $txt,0,1,'L');
        $this->Ln();
        $txt2 = iconv('utf-8', 'cp1252', $timePay);
        $this->Cell(70,8,$txt2,0,1,'L');
//        $txt3 = iconv('utf-8', 'cp1252', $iban);
//        $this->Cell(70,15,$txt3,0,1,'L');
        $this->Cell(70, 15, "", 0, 1);
        $txt4 = iconv('utf-8', 'cp1252', "Página ");
        $this->Cell(0,12,$txt4.$this->PageNo(),'T', 0,'C');
    }

    public function updateStatusInvoice($invo, $status, $item, $user = null, $generatePdf = false){
        $repoInvoice = new DataInvoice();
        $invoice = new Invoice();

        $arrInvoice = explode("__", $invo);

        $statuInvoice = $repoInvoice->getStatuByName($status);

        $invoice->setStatus($statuInvoice);
        $invoice->setReferencia($arrInvoice[0]);
        $invoice->setId($arrInvoice[1]);

        $repoInvoice->updateStatusInvoice($invoice, $item);

        if(!$generatePdf){
            $message = "Estado de la factura cambiado correctamente";
            header("Location: ../ver_factura.php?ref=$arrInvoice[0]&user=$user&m=$message");
        }
    }

    public function updateStatusManyInvoices($invoices = [], $status, $url){
        $repoInvoice = new DataInvoice();
        $invoice = null;
        $message = "Facturas cambiadas correctamente de estado";
        $statuInvoice = $repoInvoice->getStatuByName($status);
        if(!empty($invoices)){
            for ($i = 0; $i < count($invoices); $i++){
                $arrInvoice = explode("__", $invoices[$i]);
                $invoice = new Invoice();
                $invoice->setStatus($statuInvoice);
                $invoice->setReferencia($arrInvoice[1]);
                $invoice->setId($arrInvoice[0]);
                $repoInvoice->updateStatusInvoice($invoice);
            }
        }

        header("Location: ..".$url."?m=$message");
    }

    /**
     * Generate a invoice by publications
     * @param $item
     * @param array $publications
     * @param array $options
     */
    public function generateInvoiceToManyItems($item, $publications = [], $options = []){
        $repoInvoice = new DataInvoice();
        $publication = null;
        $statuInvoice = 2;

        if(!empty($publications)){
            $options['productos'] = [];
            $invoice = new Invoice();
            $invoice->setStatus(2);

            $lastInvoice = $repoInvoice->getLastInvoice();
            $newReference = $repoInvoice->getCurrentInvoice($lastInvoice);

            $invoice->setReferencia($newReference);
            $options['number_invoice'] = $newReference;

            $idInvoice = $repoInvoice->insertInvoice($invoice);
            $options['invoice'] = $lastInvoice;
            for ($i = 0; $i < count($publications); $i++){
                $publication = new Invoice();
                $publication->setCode(explode("__", $publications[$i])[0]);
                $result = $repoInvoice->getPublicationByCode($item, $publication->getCode());
                $publication->setName($result[0]['name']);
                $publication->setLastName($result[0]['last_name']);
                $publication->setPhone($result[0]['phone']);
                $publication->setCreated($result[0]['created']);
                $publication->setPrice($result[0]['price']);
                $publication->setStatus($statuInvoice);

                /** @var Invoice $publication */
                $repoInvoice->updatePublication($publication, $item, $idInvoice);
                $options['productos'][$i]['producto'] = $item === 'death' ? 'Publicación de defunción' : 'Publicación de misa';
                $options['productos'][$i]['cantidad'] = 1;
                $options['productos'][$i]['precio'] = $publication->getPrice();
            }
        }

        $this->generarFactura($options);
    }
}
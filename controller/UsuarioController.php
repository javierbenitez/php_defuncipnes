<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"&&$_SESSION['rango']!="especial"){
    header("Location:index.php");
}

require("Conexion.php");
include '../librerias/Encriptador.php';
require_once('../librerias/DataUser.php');
require_once('../librerias/DataCompany.php');

function existUser($username){
    $dataUser = new DataUser();
    $user = new User();
    $user->setUsername($username);
    $result = $dataUser->getUserByCPAndUsername($user);

    if($result != null){
        return true;
    }
    return false;
}

function createUserMain(){
    $_SESSION['created_usuariop'] = trim($_POST["usuario"]);
    $_SESSION['created_contrasena'] = trim($_POST["contrasena"]);
    $_SESSION['created_correo'] = trim($_POST["correo"]);
    $_SESSION['created_rango'] = trim($_POST["rango"]);
    $_SESSION['created_precio'] = trim($_POST["precio"]);

    $_SESSION['created_nombre'] = $_POST["nombre"];
    $_SESSION['created_numero'] = $_POST["numero"];
    $_SESSION['created_nif'] = $_POST["nif"];
    $_SESSION['created_direccion'] = $_POST["direccion"];
    $_SESSION['created_localidad'] = $_POST["localidad"];
    $_SESSION['created_cp'] = $_POST["cp"];

    $_SESSION['created_cuenta'] = $_POST["cuenta"];
    $_SESSION['created_gerente'] = $_POST["gerente"];

    if(strlen(trim($_POST['usuario'])) < 2 || strlen(trim($_POST['contrasena'])) < 4 || strlen(trim($_POST['correo'])) < 4
        || strlen(trim($_POST['nombre'])) < 2 || strlen(trim($_POST['numero'])) < 5 || strlen(trim($_POST['nif'])) < 5
        || strlen(trim($_POST['direccion'])) < 2 || strlen(trim($_POST['localidad'])) < 2 || strlen(trim($_POST['cp'])) < 4){
        $message = "Debe rellenar todos los campos.";
        header("Location: Alta_usuario?m=$message");
    }

    if(isset($_POST['contra_admin'])){
        $pass = trim($_POST['contra_admin']);
        $dataUser = new DataUser();
        $dataCompany = new DataCompany();
        $userLogged = $dataUser->getUserLogged();
        if(trim($_POST["contrasena"]) !== trim($_POST["rep_contrasena"])){
            $message = "Las contraseñas no coinciden.";
            header("Location: ../Alta_usuario?m=$message");
        }else{
            if(password_verify($pass, $userLogged[2])){
                if(existUser(trim($_POST["usuario"]))){
                    $message = "Ya existe un usuario con ese nombre.";
                    header("Location: ../Alta_usuario?m=$message");
                }else{
                    $hash = "qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";
                    $user = new User();
                    $company = new Company();
                    $user->setCreated(new \DateTime());
                    $user->setFechaCreacion(new \DateTime());
                    $user->setUsername(trim($_POST["usuario"]));
                    $user->setPassword(password_hash(trim($_POST["contrasena"]), PASSWORD_BCRYPT));
                    $user->setEnabled(1);
                    $user->setEmail(trim($_POST["correo"]));
                    $user->setRango(trim($_POST["rango"]));
                    $user->setLocalidad(trim($_POST["localidad"]));
                    $user->setCp(trim($_POST["cp"]));
                    $user->setRoles(null);
                    $user->setUser("");
                    $user->setPrice(trim($_POST['precio']));
                    $lastId = $dataUser->insertUser($user);

                    $company->setCreated(new \DateTime());
                    $company->setName(trim($_POST["nombre"]));
                    $company->setPhone1(trim($_POST["numero"]));
                    $company->setNif(trim($_POST["nif"]));
                    $company->setAddress(trim($_POST["direccion"]));
                    $company->setAccount(Encriptador::encriptar(trim($_POST["cuenta"]), $hash));
                    $company->setNameContact(trim($_POST["gerente"]));
                    $company->setUser($lastId);
                    $dataCompany->insertCompany($company);

                    if($user->getRango() !== "grupo"){
                        $message = "Usuario creado correctamente.";
                        header("Location: ../inicio.php?m=$message");
                    }else{
                        $_SESSION['created_usergroup'] = $lastId;
                        header("Location: ../alta_usuarios.php");
                    }
                }
            }else{
                $message = "Contraseña de super administrador incorrecta.";
                header("Location: ../Alta_usuario?m=$message");
            }
        }
    }else{
        $message = "Debe poner la contraseña para poder crear un usuario.";
        header("Location: ../Alta_usuario?m=$message");
    }
}

function createUserGroup(){
    $_SESSION['created_usuario'] = trim($_POST["usuario"]);
    $_SESSION['created_contrasena'] = trim($_POST["contrasena"]);
    $_SESSION['created_localidad'] = $_POST["localidad"];
    $_SESSION['created_cp'] = $_POST["cp"];

    if(trim($_POST["contrasena"]) !== trim($_POST["rep_contrasena"])){
        $message = "Las contraseñas no coinciden.";
        header("Location: ../alta_usuarios.php?m=$message");
    }else{
        $dataUser = new DataUser();
        include_once "../librerias/Random.php";
        $email = $_SESSION['created_correo']."_".Random::getStringRandom(1);
        $hash = "qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";
        $user = new User();
        $user->setCreated(new \DateTime());
        $user->setFechaCreacion(new \DateTime());
        $user->setUsername(trim($_POST["usuario"]));
        $user->setPassword(password_hash(trim($_POST["contrasena"]), PASSWORD_BCRYPT));
        $user->setEnabled(1);
        $user->setLocalidad(trim($_POST["localidad"]));
        $user->setCp(trim($_POST["cp"]));
        $user->setEmail($email);
        $user->setUser($_SESSION['created_usergroup']);
        $user->setRango("normal");
        $user->setRoles(null);
        $user->setPrice(trim($_POST['precio']));

        $lastId = $dataUser->insertUser($user);
        $_SESSION['users_group'] = $dataUser->getUsersByUser($user->getUser());
        if($lastId > 0){
            $message = "Usuario añadido correctamente";
            header("Location: ../alta_usuarios.php?info=$message");
        }else{
            $message = "Error intentando añadir el usuario";
            header("Location: ../alta_usuarios.php?m=$message");
        }
    }
}

if(isset($_POST['usuario']) && isset($_POST['contrasena'])){

    if(isset($_POST['rango'])){
        createUserMain();
    }else{
        createUserGroup();
    }
}

if(isset($_POST['user']) && isset($_POST['cp']) && !isset($_POST['user_id'])){
    $dataUser = new DataUser();
    $user = new User();
    $user->setUsername(trim($_POST['user']));
    $user->setCp(trim($_POST['cp']));

    $row = $dataUser->getUserByCPAndUsername($user)[0];

    if($row[0] == 0){
        $active = 1;
        $result['active'] = true;
    }else{
        $active = 0;
        $result['active'] = false;
    }

    $dataUser->setActiveUser($user, $active);

    return json_encode($result);
}

/**
 * View user and users by user
 */
if(isset($_POST['username']) && isset($_POST['cp']) && isset($_POST['user_id'])){
    $dataUser = new DataUser();
    $us = new User();
    $us->setUsername(trim($_POST['username']));
    $us->setCp(trim($_POST['cp']));
    $user = $dataUser->getUserByCPAndUsername($us);
    if($user[0]['user_id'] != null){
        $userFather = $dataUser->getUserById($user[0]['user_id'])[0];
        $_SESSION['user_father'] = $userFather;
    }
    $_SESSION['user_view'] = $user;
    $users = $dataUser->getUsersByUser(trim($_POST['user_id']));
    if(count($users) > 1){
        $_SESSION['users_group'] = $users;
    }
    header("Location: ../ver_usuario.php");
}


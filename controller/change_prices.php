<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}

require "../controller/Conexion.php";
$logeo = new Logeo();

$_SESSION['results_prices'] = $logeo->getElementsAndPrices();

if(isset($_POST['price']) && isset($_POST['element']) && isset($_POST['type'])){
    $element = $_POST["element"];
    $price = $_POST["price"];
    $type = $_POST["type"];

    if($price < 0.00){
        header("Location:../cambiar_precios.php?e=Ko");
    }else{
        $priceArr = explode(".", $price);

        if(count($priceArr) > 1){
            if(count($priceArr[1]) < 2){
                $dec = $priceArr[1]."0";
            }else{
                $dec = $priceArr[1][0].$priceArr[1][1];
            }
            $price = $priceArr[0].".".$dec;

        }

        $user = $logeo->getUserLogged();

        if ($logeo->setPriceElement($user[0]['id'], $element, $price, $type)){
            $_SESSION['results_prices'] = $logeo->getElementsAndPrices();
            header("Location:../cambiar_precios.php?e=Ok");
        }else{
            header("Location:../cambiar_precios.php?e=Ko");
        }
    }

}else{
    header("Location: ../cambiar_precios.php");
}




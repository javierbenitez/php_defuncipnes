<?php
session_start();
if (!isset($_SESSION['usuario']) && !isset($_SESSION['rango'])) {
    header("Location: index.php");
}
$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
    <link rel="stylesheet" href="css/publicacion.css">
</head>

<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container">
    <form  class="form-horizontal m-5-arriba" id="formulario" name="formulario" action="confirmar_misa.php" enctype="multipart/form-data" method="POST">
    <div class="container" >
        <section class="row justify-content-center">
            <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba"  >
                <h1>Roguemos a dios por...</h1>
                <input required readonly="readonly" name ="fecha_alta" type="hidden" value="<?php echo date('Y-m-d');?>" class="form-control m-0" id="fecha_alta" aria-describedby="" placeholder="Fecha alta">
            </div>
            <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba" >
                <div class="row">
                    <div class="col-md-12" >
                        <h4>Datos Publicacion</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" >
                        <input required name ="nombre" type="text" class="form-control" id="nombre" aria-describedby="" placeholder="Nombre">
                    </div>
                    <div class="col-md-6" >
                        <input required name ="apellidos" type="text" class="form-control" id="apellidos" aria-describedby="" placeholder="Apellidos">
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-6" >
                        <input name ="apodo" type="text" class="form-control" id="apodo" aria-describedby="" placeholder="Apodo">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <input required name="edad" type="number" class="form-control" id="edad" aria-describedby="" placeholder="Edad" >
                    </div>
                </div>

                <div class="row  m-2-arriba">
                    <div class="col-md-7" >
                        <div class="row">
                            <div class="col-md-4 text-xl-right" >
                                <label for="fecha_fallecimiento" class="m-0">
                                    <p>Fecha fallecimiento</p>
                                </label>
                            </div>
                            <div class="col-md-8" >
                                <input required autocomplete="off" name="fecha_fallecimiento" type="text" class="form-control" id="fecha_fallecimiento" aria-describedby="" placeholder="Fecha fallecimiento">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 align-self-center">
                        <label for="premiun"  title="Premium">
                            <input name ="premiun" type="checkbox" class="form-check-input" id="premiun" aria-describedby="">
                            Premium
                        </label>
                    </div>
                </div>
                <div class="row m-5-arriba">
                    <div class="col-md-4"><input type="text" title="Código postal principal, localidad donde vivía el fallecido" class="form-control" name="codigo_postal1" id="codigo_postal1" required placeholder="Código postal"></div>
                    <div class="col-md-4"><input type="text" class="form-control" name="codigo_postal2" id="codigo_postal2" placeholder="Código postal 2"></div>
                    <div class="col-md-4"><input type="text" class="form-control" name="codigo_postal3" id="codigo_postal3" placeholder="Código postal 3"></div>
                </div>
                <div class="row m-5-arriba">
                    <div class="col-md-6" >
                        <input name ="marido" type="text" class="form-control" id="marido" aria-describedby="" placeholder="Cónyuge@">
                    </div>
                    <div class="col-md-6" >
                        <input  name="padres" type="text" class="form-control" id="padres" aria-describedby="" placeholder="Padres">
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-6" >
                        <textarea class="form-control" style="resize: none" rows="3" id="hijos" name="hijos" placeholder="Hij@s"></textarea>
                    </div>
                    <div class="col-md-6" >
                        <textarea class="form-control" style="resize: none" rows="3" id="hijos_politicos" name="hijos_politicos" placeholder="Hij@s Politic@s"></textarea>
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-6" >
                        <textarea class="form-control" style="resize: none" rows="3" id="hermanos" name="hermanos" placeholder="Herman@s"></textarea>
                    </div>
                    <div class="col-md-6" >
                        <textarea class="form-control" style="resize: none" rows="3" id="hermanos_politicos" name="hermanos_politicos" placeholder="Herman@s Politic@s"></textarea>
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-6" >
                        <textarea class="form-control" style="resize: none" rows="3" id="familiares"  name="familiares" placeholder="Familiares"></textarea>
                    </div>
                    <div class="col-md-6" >
                        <input  name="pareja" type="text" class="form-control" id="pareja" aria-describedby="" placeholder="Pareja">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba"  >
                <div class="row">
                    <div class="col-md-12" >
                        <h4>Datos ceremonia</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3" >
                        <input required name ="lugar" type="text" class="form-control" id="lugar" aria-describedby="" placeholder="Lugar ceremonia">
                    </div>
                    <div class="col-md-5" >
                        <div class="row">
                            <div class="col-md-4 text-xl-right" >
                                <label for="fecha_entierro" class="m-0">
                                    <p>Fecha</p>
                                </label>
                            </div>
                            <div class="col-md-8" >
                                <input required  autocomplete="off" name ="fecha_ceremonia" type="text" class="form-control" id="fecha_ceremonia" aria-describedby="" placeholder="Fecha ceremonia">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" >
                        <div class="row">
                            <div class="col-md-5 text-xl-right" >
                                <label for="hora" class="m-0">
                                    <p>Hora</p>
                                </label>
                            </div>
                            <div class="col-md-7" >
                                <input required name ="hora" type="time" class="form-control" id="hora" aria-describedby="" placeholder="Hora">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-2-arriba">
                    <div class="col-md-6" >
                    </div>
                    <div class="col-md-6 align-self-center">
                        <label for="anual" title="Aniversario">
                            <input name ="anual" type="checkbox" class="form-check-input" id="anual" aria-describedby="" placeholder="anual">
                            Aniversario
                        </label>
                    </div>
                </div>
                <div class="checkbox">
                    <label for="otra_ceremonia"><input type="checkbox" id="otra_ceremonia" name="otra_ceremonia">Hay otra ceremonia</label>
                </div>
            </div>
            <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba" id="ceremonia2" >
                <div class="row">
                    <div class="col-md-12" >
                        <h4>Datos ceremonia 2</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3" >
                        <input name ="lugar2" type="text" class="form-control" id="lugar2" aria-describedby="" placeholder="Lugar ceremonia">
                    </div>
                    <div class="col-md-5" >
                        <div class="row">
                            <div class="col-md-4 text-xl-right" >
                                <label for="fecha_entierro" class="m-0">
                                    <p>Fecha</p>
                                </label>
                            </div>
                            <div class="col-md-8" >
                                <input autocomplete="off" name ="fecha_cerem2" type="text" class="form-control" id="fecha_cerem2" aria-describedby="" placeholder="Fecha ceremonia">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" >
                        <div class="row">
                            <div class="col-md-5 text-xl-right" >
                                <label for="hora" class="m-0">
                                    <p>Hora</p>
                                </label>
                            </div>
                            <div class="col-md-7" >
                                <inputname ="hora2" type="time" class="form-control" id="hora2" aria-describedby="" placeholder="Hora">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <button name="boton" type="submit" class="btn btn-primary align-self-center">Enviar</button>
    </div>
</form>
</div>
<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script>
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $(function () {

        var fechaFallecimiento, fechaCeremonia, fechaCeremonia2, checkboxCeremonia2;

        function initVars(){
            fechaFallecimiento = $("#fecha_fallecimiento");
            fechaCeremonia = $("#fecha_ceremonia");
            fechaCeremonia2 = $("#fecha_cerem2");
            checkboxCeremonia2 = $('#otra_ceremonia');
        }

        initVars();

        fechaFallecimiento.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            endDate: "today",
            maxDate: "today",
            language: 'es'
        });

        fechaCeremonia.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            startDate: 'today',
            language: 'es'
        });

        fechaCeremonia2.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            startDate: 'today',
            language: 'es'
        });

        function addRequiredAttribute(input) {
            input.prop('required', 'required');
        }

        checkboxCeremonia2.on('click', function (e) {
            var val = $(this).is(':checked');
            if(!val){
                fechaCeremonia2.datepicker({
                    format: 'dd-mm-yyyy',
                    autoclose: true,
                    startDate: 'today',
                    language: 'es'
                });
                $('#ceremonia2').fadeOut('fast');
                $('input#lugar2').removeAttr("required");
                $('input#lugar_iglesia2').removeAttr("required");
                $('input#fecha_entierro2').removeAttr("required");
                $('input#hora2').removeAttr("required");
            }else{
                $('#ceremonia2').fadeIn('fast');
                addRequiredAttribute($('input#lugar2'));
                addRequiredAttribute($('input#lugar_iglesia2'));
                addRequiredAttribute($('input#fecha_entierro2'));
                addRequiredAttribute($('input#hora2'));
            }
        });

    });
</script>

</body>
</html>
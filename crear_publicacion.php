<?php
session_start();
if (!isset($_SESSION['usuario']) && !isset($_SESSION['rango'])) {
    header("Location: index.php");
}

include_once "librerias/Insurances.php";

$insurances = Insurances::getAll();

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include ("css/basic_style.php"); ?>
    <link rel="stylesheet" href="css/publicacion.css">
    <script src="ckeditor/ckeditor.js"></script>
</head>

<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container">
    <form  class="form-horizontal m-5-arriba" id="formulario" name="formulario" action="confirmar_publicacion.php" enctype="multipart/form-data" method="POST">
        <div class="container" >
            <section class="row justify-content-center">
                <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba"  >
                    <h1>Roguemos a dios por...</h1>
                </div>
                <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba" >
                    <div class="row">
                        <div class="col-md-12" >
                            <h4>Datos publicacion</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4" >
                            <input required name ="nombre" type="text" class="form-control" id="nombre" aria-describedby="" placeholder="Nombre">
                        </div>
                        <div class="col-md-4" >
                            <input required name ="apellidos" type="text" class="form-control" id="apellidos" aria-describedby="" placeholder="Apellidos">
                        </div>
                        <div class="col-md-4" >
                            <input name ="apodo" type="text" class="form-control" id="apodo" aria-describedby="" placeholder="Apodo">
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-md-8" >
                            <input required name ="direccion" type="text" class="form-control" id="direccion" aria-describedby="" placeholder="Direccion">
                        </div>
                        <div class="col-md-4" >
                            <input required name ="localidad" type="text" class="form-control" id="localidad" aria-describedby="" placeholder="Localidad">
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-md-6" >
                            <div class="row">
                                <div class="col-md-5 text-xl-right" >
                                    <label for="fecha_fallecimiento" class="m-0">
                                        <p>Fecha Fallecimiento</p>
                                    </label>
                                </div>
                                <div class="col-md-7" >
                                    <input required  autocomplete="off" name="fecha_fallecimiento" type="text" class="form-control" id="fecha_fallecimiento" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 align-self-center">
                            <div class="row">
                                <div class="col-md-5 text-xl-right align-self-center">
                                    <label for="fecha_alta" class="m-0 align-self-center">
                                        <p class="align-self-center">Fecha Alta</p>
                                    </label>
                                </div>
                                <div class="col-md-7" >
                                    <input required readonly="readonly" name ="fecha_alta" type="date" value="<?php echo date('Y-m-d');?>" class="form-control m-0" id="fecha_alta" aria-describedby="" placeholder="fecha_alta">
                                </div>
                            </div>
                        </div>
                        <div class="col-md align-self-center">
                            <input required name="edad" type="number" class="form-control" id="edad" aria-describedby="" placeholder="Edad" >
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-sm-6">
                            <div class="checkbox">
                                <label for="is_insurance"><input type="checkbox" id="is_insurance" name="is_insurance">Asegurado</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <select name="insurance" id="insurance" class="form-control">
                                <?php
                                foreach ($insurances as $insurance){
                                    ?>
                                    <option value="<?php echo $insurance['name']; ?>"><?php echo $insurance['name']; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row  m-2-arriba">
                        <div class="col-md-6" >
                            <input type="text" title="Este campo es opcional" name="contact_phone1" id="contact_phone1" class="form-control" placeholder="Teléfono contacto">
                        </div>
                        <div class="col-md-6" >
                            <input type="text" title="Este campo es opcional"  name="contact_phone2" id="contact_phone2" class="form-control" placeholder="Otro teléfono contacto">
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-md-10" >
                            <label for="imagen" class="m-0">
                                Imagen del tanatorio  <input required type="file" name="imagen" id="imagen">
                            </label>
                        </div>
                        <div class="col-md align-self-center">
                            <input name ="premiun" type="checkbox" class="form-check-input" id="premiun" aria-describedby="" placeholder="premiun">Premium
                        </div>
                    </div>
                    <div class="row  m-2-arriba">
                        <div class="col-md-7" >
                            <label for="imagen2" class="m-0">
                                Imagen del fallecido  <input title="No es obligatoria, pero si se sube debe ser una imagen del difunto" type="file" name="imagen2" id="imagen2">
                            </label>
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-md-4"><input type="text" title="Código postal principal, localidad donde vivía el fallecido" class="form-control" name="codigo_postal1" id="codigo_postal1" required placeholder="Código postal"></div>
                        <div class="col-md-4"><input type="text" class="form-control" name="codigo_postal2" id="codigo_postal2" placeholder="Código postal 2"></div>
                        <div class="col-md-4"><input type="text" class="form-control" name="codigo_postal3" id="codigo_postal3" placeholder="Código postal 3"></div>
                    </div>
                    <div class="row m-5-arriba">
                        <div class="col-md-6" >
                            <input name ="conyuge" type="text" class="form-control" id="conyuge" aria-describedby="" placeholder="Cónyuge">
                        </div>
                        <div class="col-md-6" >
                            <input  name="padres" type="text" class="form-control" id="padres" aria-describedby="" placeholder="Padres">
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-md-6" >
                            <textarea class="form-control" style="resize: none" rows="3" id="hijos" name="hijos" placeholder="Hij@s"></textarea>
                        </div>
                        <div class="col-md-6" >
                            <textarea class="form-control" style="resize: none" rows="3" id="hijos_politicos" name="hijos_politicos" placeholder="Hij@s Politic@s"></textarea>
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-md-6" >
                            <textarea class="form-control" style="resize: none" rows="3" id="hermanos" name="hermanos" placeholder="Herman@s"></textarea>
                        </div>
                        <div class="col-md-6" >
                            <textarea class="form-control" style="resize: none" rows="3" id="hermanos_politicos" name="hermanos_politicos" placeholder="Herman@s Politic@s"></textarea>
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-md-6" >
                            <textarea class="form-control" style="resize: none" rows="3" id="familiares"  name="familiares" placeholder="Familiares"></textarea>
                        </div>
                        <div class="col-md-6">
                            <input  name="pareja" type="text" class="form-control" id="pareja" aria-describedby="" placeholder="Pareja">
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-md-6" >
                            <input  name="d_recibe" type="text" class="form-control" id="d_recibe" aria-describedby="" placeholder="Duelo recibe">
                        </div>
                        <div class="col-md-6" >
                            <input  name="d_despide" type="text" class="form-control" id="d_despide" aria-describedby="" placeholder="Duelo despide">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba"  >
                    <div class="row">
                        <div class="col-md-12" >
                            <h4>Datos ceremonia</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" >
                            <input required name ="lugar" type="text" class="form-control" id="lugar" aria-describedby="" placeholder="Lugar tanatorio">
                        </div>
                        <div class="col-md-6" >
                            <input required name ="lugar_iglesia" type="text" class="form-control" id="lugar_iglesia" aria-describedby="" placeholder="Lugar Iglesia">
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-md-6" >
                            <div class="row">
                                <div class="col-md-5 text-xl-right" >
                                    <label for="fecha_entierro" class="m-0">
                                        <p>Fecha Entierro</p>
                                    </label>
                                </div>
                                <div class="col-md-7" >
                                    <input required  autocomplete="off" name ="fecha_entierro" type="text" class="form-control" id="fecha_entierro" aria-describedby="" placeholder="Fecha entierro">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" >
                            <div class="row">
                                <div class="col-md-5 text-xl-right" >
                                    <label for="hora" class="m-0">
                                        <p>Hora</p>
                                    </label>
                                </div>
                                <div class="col-md-7" >
                                    <input required name ="hora" type="time" class="form-control" id="hora" aria-describedby="" placeholder="Hora">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="checkbox">
                        <label for="otra_ceremonia"><input type="checkbox" id="otra_ceremonia" name="otra_ceremonia">Hay otra ceremonia</label>
                    </div>
                </div>
                <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba" id="ceremonia2" >
                    <div class="row">
                        <div class="col-md-12" >
                            <h4>Datos ceremonia 2</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" >
                            <input name ="lugar2" type="text" class="form-control" id="lugar2" aria-describedby="" placeholder="Lugar tanatorio">
                        </div>
                        <div class="col-md-6" >
                            <input name ="lugar_iglesia2" type="text" class="form-control" id="lugar_iglesia2" aria-describedby="" placeholder="Lugar Iglesia">
                        </div>
                    </div>
                    <div class="row m-2-arriba">
                        <div class="col-md-6" >
                            <div class="row">
                                <div class="col-md-5 text-xl-right" >
                                    <label for="fecha_entierro2" class="m-0">
                                        <p>Fecha Entierro</p>
                                    </label>
                                </div>
                                <div class="col-md-7" >
                                    <input  autocomplete="off" name="fecha_entierro2" type="text" class="form-control" id="fecha_entierro2" aria-describedby="" placeholder="Fecha entierro">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" >
                            <div class="row">
                                <div class="col-md-5 text-xl-right" >
                                    <label for="hora2" class="m-0">
                                        <p>Hora</p>
                                    </label>
                                </div>
                                <div class="col-md-7" >
                                    <input name ="hora2" type="time" class="form-control" id="hora2" aria-describedby="" placeholder="Hora">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba"  >
                    <div class="row">
                        <div class="col-md-12" >
                            <h4>Resumen</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" >
                            <textarea required placeholder="Texto" name="Texto" id="Texto" >

                            </textarea>
                        </div>
                    </div>
                </div>

            </section>
            <br>
            <button name="boton" type="submit" class="btn btn-primary align-self-center">Enviar</button>
        </div>
    </form>
</div>
<?php include ("includes/footer.php"); ?>
<?php include ("js/basic_js.php"); ?>
<script>
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $(function () {

        var fechaFallecimiento, fechaEntierro, fechaEntierro2, checkboxCeremonia2, checkboxInsurance, selectInsurance;

        function initVars(){
            fechaFallecimiento = $("#fecha_fallecimiento");
            fechaEntierro = $("#fecha_entierro");
            fechaEntierro2 = $("#fecha_entierro2");
            checkboxCeremonia2 = $('#otra_ceremonia');
            checkboxInsurance = $('#is_insurance');
            selectInsurance = $('#insurance');
        }

        initVars();

        fechaFallecimiento.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            endDate: "today",
            maxDate: "today",
            language: 'es'
        });

        fechaEntierro.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            startDate: 'today',
            language: 'es'
        });

        fechaEntierro2.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            startDate: 'today',
            language: 'es'
        });

        function addRequiredAttribute(input) {
            input.prop('required', 'required');
        }

        checkboxCeremonia2.on('click', function (e) {
            var val = $(this).is(':checked');
            if(!val){
                $('#ceremonia2').fadeOut('fast');
                $('input#lugar2').removeAttr("required");
                $('input#lugar_iglesia2').removeAttr("required");
                $('input#fecha_entierro2').removeAttr("required");
                $('input#hora2').removeAttr("required");
            }else{
                $('#ceremonia2').fadeIn('fast');
                addRequiredAttribute($('input#lugar2'));
                addRequiredAttribute($('input#lugar_iglesia2'));
                addRequiredAttribute($('input#fecha_entierro2'));
                addRequiredAttribute($('input#hora2'));
            }
        });

        checkboxInsurance.on('click', function (e) {
            var val = $(this).is(':checked');
            if(!val){
                selectInsurance.fadeOut('fast');
                selectInsurance.removeAttr("required");
            }else{
                selectInsurance.fadeIn('fast');
                addRequiredAttribute(selectInsurance);
            }
        });
    });
</script>
<script>
    CKEDITOR.replace( 'Texto',{
        filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
        filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserWindowWidth: '1000',
        filebrowserWindowHeight: '700',
        height: "60vh"
    }  );


    CKFinder.widget( 'ckfinder-widget', {
        readOnly: true,
        width: '100%',
    } );
</script>
</body>
</html>

<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"){
    header("Location:index.php");
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

$nif=$_SESSION['nif'];
require("controller/Conexion.php");
include 'librerias/Encriptador.php';
$pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

$Conexion=new Logeo();

$Lista=$Conexion->Conseguir_Usuarios();


?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>
<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container">
    <section class="row m-5-arriba">
        <form  class="form-horizontal m-5-arriba" id="formulario" name="formulario" action="generar_tabla_facturas.php" enctype="multipart/form-data" method="POST">
            <div class="container ">
                <section class="row justify-content-center">
                    <div class="col-md-3" >
                        <h5>
                            Usuario
                        </h5>
                        <input required name ="usuario" type="text" class="form-control" id="usuario" aria-describedby="" placeholder="Usuario">
                    </div>
                    <div class="col-md-3" >
                        <h5>
                            Fecha Inicio
                        </h5>
                        <input required name ="fecha_p" type="date" class="form-control" id="fecha_p" aria-describedby="" placeholder="Fecha Inicio">
                    </div>
                    <div class="col-md-3" >
                        <h5>
                            Fecha Fin
                        </h5>
                        <input required name ="fecha_s" type="date" class="form-control" id="fecha_s" aria-describedby="" placeholder="Fecha Fin">
                    </div>
                    <div class="col-md-3" >
                        <h5>
                            Comision
                        </h5>
                        <input required name ="comision" value="10" type="number" class="form-control" id="comision" aria-describedby="" placeholder="Comision">
                    </div>
                </section>
                <br>
                <button name="boton" type="submit" class="btn btn-primary align-self-center">Enviar</button>
            </div>
        </form>
            <div class="col-md-12">
                <table class="table">
                    <tr class="header thead-dark text-center">
                        <th colspan="6"><h3>Buscar por...</h3></th>
                    </tr>
                    <tr class="header thead-dark">
                        <th><input class="buscar_por" type="text" id="nombre" onkeyup="Primer_campo()" placeholder="NIF" title="NIF"></th>
                        <th><input class="buscar_por" type="text" id="dni" onkeyup="Segundo_campo()" placeholder="Nombre" title="Nombre"></th>
                        <th><input class="buscar_por" type="text" id="fallecimiento" onkeyup="Tercer_campo()" placeholder="Codigo Postal" title="Codigo Postal"></th>
                        <th><input class="buscar_por" type="text" id="ceremonia" onkeyup="Cuarto_campo()" placeholder="Direccion" title="Direccion"></th>
                        <th><input class="buscar_por" type="text" id="lugar" onkeyup="Quinto_campo()" placeholder="Usuario" title="Usuario"></th>
                    </tr>
                </table>
            </div>
        </form>
    </section>
    <section class="row">
        <div class="col-md-12">
            <table id="Tabla_contenido" class="table table-hover table-striped">
                <thead>
                    <tr class="header thead-dark text-center">
                        <th>NIF</th>
                        <th>Nombre</th>
                        <th>Codigo Postal</th>
                        <th>Direccion</th>
                        <th>Cuenta</th>
                        <th>Usuario</th>
                        <th>Gerente</th>
                        <th>Numero</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach ($Lista as $fila){
                    echo "<tr>";
                        echo "<td class='align-middle text-center'>";
                            echo Encriptador::desencriptar($fila[0],$pass);
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[1];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[2];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[3];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo Encriptador::desencriptar($fila[4],$pass);
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[5];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[6];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[7];
                        echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
</div>
<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script>
    function Primer_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("nombre");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Segundo_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("dni");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Tercer_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("fallecimiento");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Cuarto_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("ceremonia");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[3];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Quinto_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("lugar");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[5];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Por_hora() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("hora");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[6];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>
</body>
</html>
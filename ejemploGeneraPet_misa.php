<?php
session_start();
$result['estado'] = 1;
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"&& $_SESSION['rango']!="misas"){
    header("Location:index.php");
}
	// Se incluye la librería
	include 'librerias/apiRedsys.php';
    include_once "librerias/Random.php";
	require("controller/Conexion.php");

    $libreria=new Logeo();
	// Se crea Objeto
	$miObj = new RedsysAPI;
	$nif=$_SESSION["nif"];

    $type = "normal";
    if(isset($_SESSION['f_premiun'])){
        if($_SESSION['f_premiun'] == 1){
            $type = "premium";
        }
    }

    $precio = $libreria->getElementPrice("misas", $type)[1];
    $precioArr = explode(".", $precio);
    if(count($precioArr) > 1){
        if(strlen($precioArr[1]) < 2){
            $price = $precioArr[0].$precioArr[1][0]."0";
        }else{
            $price = $precioArr[0].$precioArr[1][0].$precioArr[1][1];
        }
    }else{
        $price = $precioArr[0]."00";
    }

    $codigo = Random::getStringRandom();
    $_SESSION['factura_Codigo'] = $codigo;
    $_SESSION["codigo"] = $codigo;

	$libreria->crearFacturaMisas($nif, $precio);

//	$Cod_Factura=$libreria->Conseguir_Ultimo_Codigo_Factura_misa($nif);

	// Valores de entrada que no hemos cmbiado para ningun ejemplo
    $company = "Aurora Servicios";
	$fuc="348221342";
	$terminal="001";
	$moneda="978";
	$trans="0";
	$url="https://sis.redsys.es/sis/realizarPago";
	$urlKO="https://auroraservicios.es/insertar_misa.php?cod=".$codigo;
	$urlOK="https://auroraservicios.es/ko.php";

	//estos dos valores los vamos cambiando en cada ejemplo
    $amount=$_SESSION['misa_f_precio']*100;

    // Se Rellenan los campos
    $miObj->setParameter("DS_MERCHANT_AMOUNT", $precio);
    $miObj->setParameter("DS_MERCHANT_ORDER", $codigo);
    $miObj->setParameter("DS_MERCHANT_MERCHANTCODE", $fuc);
    $miObj->setParameter("DS_MERCHANT_MERCHANTNAME", $company);
    $miObj->setParameter("DS_MERCHANT_CURRENCY", $moneda);
    $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $trans);
    $miObj->setParameter("DS_MERCHANT_TERMINAL", $terminal);
    $miObj->setParameter("DS_MERCHANT_MERCHANTURL", $url);
    $miObj->setParameter("DS_MERCHANT_URLOK", $urlKO);
    $miObj->setParameter("DS_MERCHANT_URLKO", $urlOK);

    //Datos de configuración
    $version = "HMAC_SHA256_V1";
    $kc = 'TMttQyUyN4aEDcxUS+MPvP2ODiclJLMH';//Clave recuperada de CANALES
    // Se generan los parámetros de la petición
    $request = "";
    $params = $miObj->createMerchantParameters();
    $signature = $miObj->createMerchantSignature($kc);

    $result['estado'] = 2;

    echo json_encode($result);
?>
<html lang="es">
<head>
    <script languaje="javascript">
        function funcion_javascript(){
            // alert ("Esto es javascript");
            document.getElementById("frm").submit();
        }
    </script>
</head>
<body>
<form id="frm" name="frm" action="<?php echo $url?>" method="POST">
<input type="hidden" name="Ds_SignatureVersion" value="<?php echo $version; ?>"/></br>
<input type="hidden" name="Ds_MerchantParameters" value="<?php echo $params; ?>"/></br>
<input type="hidden" name="Ds_Signature" value="<?php echo $signature; ?>"/></br>
    <script languaje="javascript">
        funcion_javascript();
    </script>
</form>

</body>
</html>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <title>Aurora Servicios</title>
    <link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>

<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container">
    <h1>Página no encontrada.</h1>
    <h2>Escribe una URL válida.</h2>
</div>
<?php include "includes/footer.php"; ?>
</body>
</html>

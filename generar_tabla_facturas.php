<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"){
    header("Location:index.php");
}
$usuario=$_POST['usuario'];
$fecha_inicio=$_POST['fecha_p'];
$fecha_fin=$_POST['fecha_s'];
$comision=$_POST['comision'];


require("controller/Conexion.php");
include 'librerias/Encriptador.php';
$pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

$Conexion=new Logeo();

$Lista_factura=$Conexion->Conseguir_Facturas_Usuarios($usuario,$fecha_inicio,$fecha_fin);
$Lista_factura_2=$Conexion->Conseguir_Facturas_Usuarios_misas($usuario,$fecha_inicio,$fecha_fin);

$Lista_Empresa=$Conexion->Conseguir_Datos_Empresa($usuario);

//print_r($Lista_factura);
//print_r($Lista_Empresa);
$Cantidad_Facturas=0;
$Cantidad_Facturas_2=0;

header('Content-type: application/vnd.ms-excel;charset=iso-8859-15');
header('Content-Disposition: attachment; filename=Facturacion_de_'.$usuario.'_del_'.$fecha_inicio.'_al_'.$fecha_fin.'.xls');

?>

<table border="1" cellpadding="2" cellspacing="0" width="100%">
    <caption>Datos empresa</caption>
    <tr style="background-color: #000000;color:white;">
        <td >NIF</td>
        <td>Nombre</td>
        <td>CP</td>
        <td>Direccion</td>
        <td>Cuenta Bancaria</td>
        <td>Gerente</td>
        <td>Numero contacto</td>
        <td>Usuario</td>

    </tr>
    <?php
    foreach ($Lista_Empresa as $fila){
        echo "<tr>";
            echo "<td class='align-middle text-center'>";
                echo Encriptador::desencriptar($fila[0],$pass);
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo $fila[1];
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo $fila[2];
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo $fila[3];
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo Encriptador::desencriptar($fila[4],$pass);
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo $fila[5];
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo $fila[6];
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo $fila[7];
            echo "</td>";
        echo "</tr>";
    }
    ?>
</table>
<br><br><br>
<h1>Publicaciones</h1>
<table border="1" cellpadding="2" cellspacing="0" width="100%">
    <caption>Datos facturacion</caption>
    <tr style="background-color: #000000;color:white;">
        <td>Codigo</td>
        <td>NIF</td>
        <td>DNI</td>
        <td>Precio</td>
        <td>Fecha</td>
        <td>Nombre</td>
        <td>Apellidos</td>
        <td>Direccion</td>

    </tr>
    <?php
    foreach ($Lista_factura as $fila){
        $Cantidad_Facturas++;
        echo "<tr>";
            echo "<td class='align-middle text-center'>";
                echo $fila[0];
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo Encriptador::desencriptar($fila[1],$pass);
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo Encriptador::desencriptar($fila[2],$pass);
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo $fila[3];
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo $fila[4];
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo $fila[5];
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo $fila[6];
            echo "</td>";
            echo "<td class='align-middle text-center'>";
                echo $fila[7];
            echo "</td>";
        echo "</tr>";
    }
    ?>
</table>
<br><br><br>
<table border="1" cellpadding="2" cellspacing="0" align="right">
    <caption>Total a pagar</caption>
    <tr>
        <td>Precio unidad</td>
        <td><?echo $comision?></td>
    </tr>
    <tr>
        <td>Total publicaciones</td>
        <td><?echo $Cantidad_Facturas?></td>
    </tr>
    <tr>
        <td>Total</td>
        <td><?echo $comision*$Cantidad_Facturas?></td>
    </tr>
</table>

<h1>Misas</h1>
<table border="1" cellpadding="2" cellspacing="0" width="100%">
    <caption>Datos facturacion</caption>
    <tr style="background-color: #000000;color:white;">
        <td>Codigo</td>
        <td>NIF</td>
        <td>DNI</td>
        <td>Precio</td>
        <td>Fecha</td>
        <td>Nombre</td>
        <td>Apellidos</td>
        <td>Direccion</td>

    </tr>
    <?php
    foreach ($Lista_factura_2 as $fila){
        $Cantidad_Facturas_2++;
        echo "<tr>";
        echo "<td class='align-middle text-center'>";
        echo $fila[0];
        echo "</td>";
        echo "<td class='align-middle text-center'>";
        echo Encriptador::desencriptar($fila[1],$pass);
        echo "</td>";
        echo "<td class='align-middle text-center'>";
        echo Encriptador::desencriptar($fila[2],$pass);
        echo "</td>";
        echo "<td class='align-middle text-center'>";
        echo $fila[3];
        echo "</td>";
        echo "<td class='align-middle text-center'>";
        echo $fila[4];
        echo "</td>";
        echo "<td class='align-middle text-center'>";
        echo $fila[5];
        echo "</td>";
        echo "<td class='align-middle text-center'>";
        echo $fila[6];
        echo "</td>";
        echo "<td class='align-middle text-center'>";
        echo $fila[7];
        echo "</td>";
        echo "</tr>";
    }
    ?>
</table>
<br><br><br>
<table border="1" cellpadding="2" cellspacing="0" align="right">
    <caption>Total a pagar</caption>
    <tr>
        <td>Precio unidad</td>
        <td><?echo $comision?></td>
    </tr>
    <tr>
        <td>Total publicaciones</td>
        <td><?echo $Cantidad_Facturas_2?></td>
    </tr>
    <tr>
        <td>Total</td>
        <td><?echo $comision*$Cantidad_Facturas_2?></td>
    </tr>
</table>



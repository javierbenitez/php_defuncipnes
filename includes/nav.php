<?php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 21/01/19
 * Time: 21:21
 */

?>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="../">Inicio</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarsExample04">
        <ul class="navbar-nav mr-auto">
            <?php if(isset($rango) && $rango != null){
                if($rango == "admin"){ ?>
                    <li class="nav-item dropdown">
                        <a  class="nav-link dropdown-toggle Blanco" id="dropdown04" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Usuarios</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown04">
                            <a class="dropdown-item" href="Alta_usuario">Crear usuario</a>
                            <a class="dropdown-item" href="listar_usuarios.php">Ver usuarios</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle Blanco" id="dropdown04" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Publicidad</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown04">
                            <a class="dropdown-item" href="subir_publicidad.php">Subir publicidad</a>
                            <a class="dropdown-item" href="listar_publicidad.php">Ver publicidad</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle Blanco" id="dropdown04" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Facturas</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown04">
                            <a class="dropdown-item" href="listar_facturas.php">Ver facturas defunciones</a>
                            <a class="dropdown-item" href="listar_facturas_misas.php">Ver facturas misas</a>
                        </div>
                    </li>
                <?php } ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle Blanco" id="dropdown04" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Publicaciones</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="crear_publicacion.php">Crear publicacion</a>
                        <a class="dropdown-item" href="<?php if($rango == "admin"){ echo "listar_defunciones.php";} else{ echo "publicacionesUsuario.php";}  ?>">Ver publicaciones</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a  class="nav-link dropdown-toggle Blanco" id="dropdown04" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Misas</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="crear_misa.php">Crear misa</a>
                        <a class="dropdown-item" href="<?php if($rango == "admin"){ echo "listar_misas.php";} else{ echo "misasUsuario.php";}  ?>">Ver publicaciones misa</a>
                    </div>
                </li>

            <?php }else{ ?>
            <li class="nav-item active">
                <a class="nav-link" href="condiciones.php">Condiciones</span></a>
            </li>
            <?php }
            if(isset($rango) && $rango != null){?>
                <li class="nav-item active">
                    <a class="nav-link" href="cerrar_sesion.php">Salir</span></a>
                </li>
            <?php } ?>
        </ul>
    </div>
</nav>
<?php

if(isset($_GET['m'])){
    echo '<div id="panel-info"><div class="alert alert-info" ><p>'.$_GET['m'].'</p></div></div>';
}

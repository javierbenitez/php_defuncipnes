<?php
    session_start();
    if (!isset($_SESSION['usuario'])){
        if (isset($_GET["Error"])){
            $error=true;
        }else{
            $error=false;
        }
 ?>
        <html>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <title>Aurora Servicios</title>
            <link rel="icon" href="imagenes/logo.ico">
            <?php include "css/basic_style.php"; ?>
        </head>
        <body style="background-color: #4dbaff">
        <form id="formulario" name="formulario" action="iniciar_sesion.php" enctype="text-plain" method="POST">
            <div class="container" >
                <br><br>
                <section class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <img style="width: 100%" src="imagenes/titulo_2.png">
                    </div>
                </section>
                <br><br>
                <section class="row justify-content-center">
                    <div class="col-sm-4 col-sm-offset-4 text-center">
                        <?php if($error) echo "<label class='Label_Erroneo' for=\"usuario\">Nombre y/o contraseña erroneos</label>"?>
                        <input required name ="usuario" type="text" class="form-control <?php if($error) echo "Input_Erroneo"?>" id="usuario" aria-describedby="emailHelp" placeholder="Usuario">
                        <br>
                        <input required  name ="contrasena" type="password" class="form-control <?php if($error) echo "Input_Erroneo"?>" id="contraseña" aria-describedby="emailHelp" placeholder="Contraseña">
                        <br>
                        <button name="boton" type="submit" class="btn btn-primary align-self-center">Enviar</button>
                    </div>
                </section>
            </div>
        </form>


        <?php include "js/basic_js.php"; ?>
        </body>
        </html>


<?php
    }else{
        header("Location:inicio.php");
    }
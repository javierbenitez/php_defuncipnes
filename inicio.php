<?php
    session_start();
    if (!isset($_SESSION['usuario'])) {
        header("Location:index.php");
    }

    if(isset($_SESSION['created_usuariop'])){
        unset($_SESSION['created_usuariop']);
        unset($_SESSION['created_contrasena']);
        unset($_SESSION['created_correo']);
        unset($_SESSION['created_rango']);
        unset($_SESSION['created_precio']);

        unset($_SESSION['created_nombre']);
        unset($_SESSION['created_numero']);
        unset($_SESSION['created_nif']);
        unset($_SESSION['created_direccion']);
        unset($_SESSION['created_localidad']);
        unset($_SESSION['created_cp']);

        unset($_SESSION['created_cuenta']);
        unset($_SESSION['created_gerente']);
        unset($_SESSION['created_usergroup']);
        unset($_SESSION['users_group']);
    }

    $usuario=$_SESSION['usuario'];
    $correo=$_SESSION['correo'];
    $rango=$_SESSION['rango'];
    $nif=$_SESSION['nif'];
    $nombre=$_SESSION['nombre'];
    $cp=$_SESSION['cp'];

    ?>

<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <title>Aurora Servicios</title>
        <link rel="icon" href="imagenes/logo.ico">
        <?php include "css/basic_style.php"; ?>
    </head>
    <body style="background-color: #4dbaff">
    <?php include "includes/nav.php"; ?>
    <div class="container text-center justify-content-center">
        <?php if ($rango=="admin"){?>
        <div class="row justify-content-center">
            <a href="Alta_usuario" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/usuario.png" width="70%">
                    <br><br>
                    <p>Crear Usuario</p>
                </div>
            </a>
            <a href="crear_publicacion.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/publicacion.png" width="70%">
                    <br><br>
                    <p>Crear Publicacion</p>
                </div>
            </a>
            <a href="crear_misa.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/misa.png" width="70%">
                    <br><br>
                    <p>Crear Misa</p>
                </div>
            </a>
            <a href="listar_usuarios.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/ver_usuario.png" width="70%">
                    <br><br>
                    <p>Ver Usuarios</p>
                </div>
            </a>
            <a href="listar_defunciones.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/factura.png" width="70%">
                    <br><br>
                    <p>Ver publicaciones</p>
                </div>
            </a>
            <a href="listar_misas.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/ver_misa.png" width="70%">
                    <br><br>
                    <p>Ver Misas</p>
                </div>
            </a>
            <a href="listar_facturas.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/factura.png" width="70%">
                    <br><br>
                    <p>Ver facturas</p>
                </div>
            </a>
            <a href="listar_facturas_misas.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/factura.png" width="70%">
                    <br><br>
                    <p>Ver facturas misas</p>
                </div>
            </a>
            <a href="subir_publicidad.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/publicidad.png" width="70%">
                    <br><br>
                    <p>Subir publicidad</p>
                </div>
            </a>
            <a href="listar_publicidad.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/ver_publicidad.png" width="70%">
                    <br><br>
                    <p>Ver publicidad</p>
                </div>
            </a>
            <a href="listar_cp_pendientes.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/cp.png" width="70%">
                    <br><br>
                    <p>Ver codigos postales</p>
                </div>
            </a>
            <a href="cambiar_pass.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/pass.png" width="70%">
                    <br><br>
                    <p>Cambiar contraseña</p>
                </div>
            </a>
            <a href="controller/change_prices.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                <div class="">
                    <img src="imagenes/coin.png" width="70%">
                    <br><br>
                    <p>Precios</p>
                </div>
            </a>
        </div>
        <?php }  else if ($rango=="misas"){?>

            <div class="row justify-content-center">
                <a href="crear_misa.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                    <div class="">
                        <img src="imagenes/misa.png" width="70%">
                        <br><br>
                        <p>Crear Misa</p>
                    </div>
                </a>
                <a href="listar_contenido_propio_misas.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                    <div class="">
                        <img src="imagenes/ver_misa.png" width="70%">
                        <br><br>
                        <p>Ver Misas</p>
                    </div>
                </a>
                <a href="cambiar_pass.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                    <div class="">
                        <img src="imagenes/pass.png" width="70%">
                        <br><br>
                        <p>Cambiar contraseña</p>
                    </div>
                </a>
            </div>
        <?php }else{?>
            <div class="row justify-content-center">
                <a href="crear_publicacion.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                    <div class="">
                        <img src="imagenes/publicacion.png" width="70%">
                        <br><br>
                        <p>Crear Publicacion</p>
                    </div>
                </a>
                <a href="crear_misa.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                    <div class="">
                        <img src="imagenes/misa.png" width="70%">
                        <br><br>
                        <p>Crear Misa</p>
                    </div>
                </a>
                <a href="cambiar_pass.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                    <div class="">
                        <img src="imagenes/pass.png" width="70%">
                        <br><br>
                        <p>Cambiar contraseña</p>
                    </div>
                </a>
            </div>
            <div class="row justify-content-center">
                <a href="publicacionesUsuario.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                    <div class="">
                        <img src="imagenes/factura.png" width="70%">
                        <br><br>
                        <p>Ver publicaciones</p>
                    </div>
                </a>
                <a href="misasUsuario.php" class="col-sm-3 bg-light m-3 p-3 Borde_difuminado_blanco">
                    <div class="">
                        <img src="imagenes/misa.png" width="70%">
                        <br><br>
                        <p>Ver misas</p>
                    </div>
                </a>
            </div>
        <?php }?>
        <br>
        <br>
        <br>
        <a href="cerrar_sesion.php" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Cerrar sesion</a>

    </div>
    <?php include "includes/footer.php"; ?>
    <?php include "js/basic_js.php"; ?>
 </body>
</html>


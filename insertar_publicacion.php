<?php
session_start();

if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"&&$_SESSION['rango']!="normal"){
    header("Location:index.php");
}

function deleteSessions()
{
    unset($_SESSION['factura_Codigo']);
    unset($_SESSION['typePublication']);

    unset($_SESSION['f_nombre']);
    unset($_SESSION['f_apellidos']);
    unset($_SESSION['f_apodo']);

    unset($_SESSION['f_direccion']);
    unset($_SESSION['f_localidad']);
    unset($_SESSION['f_DNI']);
    unset($_SESSION['f_edad']);
    unset($_SESSION['f_insurance']);
    unset($_SESSION['contact_phone1']);
    unset($_SESSION['contact_phone2']);

    unset($_SESSION['f_fecha_fallecimiento']);
    unset($_SESSION['f_fecha_alta']);

    unset($_SESSION['f_imagen']);
    unset($_SESSION['f_imagen2']);

    unset($_SESSION['f_premiun']);
    unset($_SESSION['f_precio']);

    unset($_SESSION['cp1']);
    unset($_SESSION['cp2']);
    unset($_SESSION['cp3']);

    unset($_SESSION['f_conyuge']);

    unset($_SESSION['f_hijos']);
    unset($_SESSION['f_hijos_politicos']);


    unset($_SESSION['f_hermanos']);
    unset($_SESSION['f_hermanos_politicos']);


    unset($_SESSION['f_familiares']);
    unset($_SESSION['f_pareja']);
    unset($_SESSION['f_padres']);

    unset($_SESSION['f_d_recibe']);
    unset($_SESSION['f_d_despide']);

    unset($_SESSION['f_lugar']);
    unset($_SESSION['f_lugar_iglesia']);
    unset($_SESSION['f_fecha_entierro']);
    unset($_SESSION['f_hora']);

    unset($_SESSION['f_lugar2']);
    unset($_SESSION['f_lugar_iglesia2']);
    unset($_SESSION['f_fecha_entierro2']);
    unset($_SESSION['f_hora2']);
    unset($_SESSION['f_Texto']);

    unset($_SESSION['factura_Nombre']);
    unset($_SESSION['factura_Apellidos']);
    unset($_SESSION['factura_Telefono']);
}

if (!empty( $_GET["cod"] ) ) {
    if ($_SESSION["factura_Codigo"] === $_GET["cod"]){

        require("controller/Conexion.php");
        include 'librerias/Encriptador.php';

        $usuario=$_SESSION['usuario'];
        $correo=$_SESSION['correo'];
        $rango=$_SESSION['rango'];
        $nif=$_SESSION['nif'];
        $nombre=$_SESSION['nombre'];
        $cp=$_SESSION['cp'];

        $nombre=$_SESSION['f_nombre'];
        $apellidos=$_SESSION['f_apellidos'];
        $apodo=$_SESSION['f_apodo'];

        $direccion=$_SESSION['f_direccion'];
        $localidad=$_SESSION['f_localidad'];
        $edad=$_SESSION['f_edad'];

        $insurance = null;
        if(isset($_SESSION['f_insurance']) && $_SESSION['f_insurance'] != null){
            $insurance = $_SESSION['f_insurance'];
        }

        $phone1 = $_SESSION['contact_phone1'];
        $phone2 = $_SESSION['contact_phone2'];

        $fecha_fallecimiento=$_SESSION['f_fecha_fallecimiento'];
        $fecha_alta=$_SESSION['f_fecha_alta'];

        $Imagen_Ruta=$_SESSION['f_imagen'];
        $Imagen_Ruta2 = $_SESSION['f_imagen2'];

        $premiun = $_SESSION['f_premiun'];
        $precio = $_SESSION['f_precio'];

        $cp1 = $_SESSION['cp1'];
        $cp2 = $_SESSION['cp2'];
        $cp3 = $_SESSION['cp3'];

        $conyuge=$_SESSION['f_conyuge'];

        $hijos=$_SESSION['f_hijos'];
        $hijos_politicos=$_SESSION['f_hijos_politicos'];

        $hermanos=$_SESSION['f_hermanos'];
        $hermanos_politicos=$_SESSION['f_hermanos_politicos'];

        $familiares=$_SESSION['f_familiares'];
        $pareja=$_SESSION['f_pareja'];
        $padres=$_SESSION['f_padres'];

        $d_recibe=$_SESSION['f_d_recibe'];
        $d_despide=$_SESSION['f_d_despide'];

        $lugar=$_SESSION['f_lugar'];
        $lugar_iglesia=$_SESSION['f_lugar_iglesia'];
        $fecha_entierro=$_SESSION['f_fecha_entierro'];
        $hora=$_SESSION['f_hora'];

        $arrCeremonia2 = [];
        if(isset($_SESSION['f_lugar2']) && $_SESSION['f_lugar2'] != null){
            $arrCeremonia2[0] = $_SESSION['f_lugar2'];
            $arrCeremonia2[1] = $_SESSION['f_lugar_iglesia2'];
            $arrCeremonia2[2] = $_SESSION['f_fecha_entierro2'];
            $arrCeremonia2[3] = $_SESSION['f_hora2'];
        }

        $Texto=$_SESSION['f_Texto'];

        $fNombre = $_SESSION['factura_Nombre'];
        $fApellidos = $_SESSION['factura_Apellidos'];
        $fTelefono = $_SESSION['factura_Telefono'];

        $fCodigo = $_SESSION['factura_Codigo'];

        $Libreria=new Logeo();

        $pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

        if ($Libreria->Insertar_Publicacion(
            $nombre, $apellidos, $apodo, $direccion, $localidad, $fecha_fallecimiento, $premiun, $Imagen_Ruta,
            $Imagen_Ruta2, $marido, $hijos, $hijos_politicos, $hermanos, $hermanos_politicos, $familiares, $pareja,
            $padres, $fecha_entierro, $lugar, $hora, $Texto, $fNombre, $fApellidos, $fTelefono,
            $fCodigo, $d_recibe, $d_despide, $lugar_iglesia, $edad, $cp1, $cp2, $cp3,
            $arrCeremonia2, $insurance, $phone1, $phone2)){

            deleteSessions();
            $message = "Publicación creada correctamente";
            header("Location: inicio.php?m=$message");
        }else{
            $message = "Error al intentar crear la publicación";
            header("Location: inicio.php?m=$message");
        }
    }else{
        deleteSessions();
        header("Location: inicio.php");
    }
}else{
    echo "codigo no introducido";
}




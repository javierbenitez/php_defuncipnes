<?php
session_start();

if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"){
    header("Location:index.php");
}
require("controller/Conexion.php");
require ("librerias/Imagen.php");

if (!isset($_FILES['imagen'])){
    header("Location: inicio.php?m=IeoK");
}

$ancho_nuevo = 1268;
$alto_nuevo = 380;

$uid = uniqid();

$ruta="./imagenes/publicidad/".$_FILES["imagen"]["name"];
$ruta2="./imagenes/publicidad/".$uid.$_FILES["imagen"]["name"];

$Codigo_Postal = trim($_POST["codigo_postal"]);
$Nombre = trim($_POST["nombre_publi"]);
$cp2 = trim($_POST['cp2']);
$cp3 = trim($_POST['cp3']);
$url = trim($_POST['url']);
$premium = 0;
if(isset($_POST['premium'])){
    $premium = 1;
}

$urlparts= parse_url($url);
$scheme = $urlparts['scheme'];


if (strlen($url) >= 1 && ($scheme !== 'https' && $scheme !== 'http')) {
    $_SESSION['nombre_publi'] = $Nombre;
    $_SESSION['cp1'] = $Codigo_Postal;
    $_SESSION['cp2'] = $cp2;
    $_SESSION['cp3'] = $cp3;
    $_SESSION['premium'] = $premium;
    header("Location: subir_publicidad.php?error=Debes poner una web válida.");
}else{
    $Conexion=new Logeo();
    if ($Conexion->Insertar_Publicidad($Codigo_Postal, $Nombre, $ruta2, $cp2, $cp3, $url, $premium)){
        move_uploaded_file($_FILES["imagen"]["tmp_name"],$ruta);
        $okImage = Imagen::resizeImage($ruta,$ruta2,$ancho_nuevo,$alto_nuevo);
        unset($_SESSION['nombre_publi']);
        unset($_SESSION['cp1']);
        unset($_SESSION['cp2']);
        unset($_SESSION['cp3']);
        unset($_SESSION['premium']);
        $message = "Publicidad subida correctamente";
        header("Location: inicio.php?m=$message");
    }else{
        header("Location: inicio.php?m=IeoK");
    }
}



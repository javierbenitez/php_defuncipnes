
window.addEventListener("load", function(){
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#383a7b"
            },
            "button": {
                "background": "#caaf00"
            }
        },
        "content": {
            "message": "Este sitio web utiliza cookies para mejorar la experiencia de usuario. Si sigue navegando consideramos que acepta su uso.",
            "dismiss": "Aceptar",
            "link": "Más información",
            "href": "https://es.wikipedia.org/wiki/Cookie_(inform%C3%A1tica)"
        }
    })});
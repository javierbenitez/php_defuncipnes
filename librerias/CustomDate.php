<?php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 26/01/19
 * Time: 22:05
 */

class CustomDate
{

    /**
     * Get date formatted
     * @param $date
     * @return string
     */
    static function formatDate($date){
        $arrDate = explode("-", $date);
        return $arrDate[2]."-".$arrDate[1]."-".$arrDate[0];
    }
}
<?php
// /librerias/DataUser.php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 25/01/19
 * Time: 22:28
 */

if(!isset($_GET['ref'])){
    require_once("../controller/Conexion.php");
    require_once("../modelo/Company.php");
}else{
    require_once("controller/Conexion.php");
    require_once("modelo/Company.php");
}

class DataCompany
{

    private $conn;

    private function getEntityManager(){
        return new Logeo();
    }

    /**
     * Insert new company
     * @param Company $company
     * @return
     */
    public function insertCompany($company){
        $this->conn = $this->getEntityManager();


        $sql = "INSERT INTO 
                          company
                          (name, 
                          name_contact,
                          phone1, 
                          address, 
                          email, 
                          nif, 
                          account, 
                          user_id, 
                          created)
                  VALUES (
                          '".$company->getName()."',
                          '".$company->getNameContact()."',
                          '".$company->getPhone1()."',
                          '".$company->getAddress()."',
                          '".$company->getEmail()."',
                          '".$company->getNif()."',
                          '".$company->getAccount()."',
                          ".$company->getUser().",
                          '".$company->getCreated()->format('Y-m-d H:i')."'
                          )";

        return $this->conn->querySet($sql);
    }

    /**
     * Insert new company
     * @param $user
     * @return
     */
    public function getCompany($user){
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                      c.name AS name_company, 
                      c.address, 
                      c.phone1,
                      c.nif AS nif_company
                  FROM 
                      company AS c 
                  WHERE 
                      c.user_id = '".$user."'";

        return $this->conn->queryParamsArray($sql)[0];
    }

    public function lastInsertId(){
        $this->conn = $this->getEntityManager();
        return $this->conn->lastInsertId();
    }
}
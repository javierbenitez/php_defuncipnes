<?php
// /librerias/DataDevice.php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 25/01/19
 * Time: 22:28
 */
include_once "../controller/Conexion.php";
include_once "../modelo/Device.php";

class DataDevice
{

    private $conn;

    private function getEntityManager(){
        return new Logeo();
    }

    /**
     * Insert new Device
     * @param Device $device
     * @return
     */
    public function insertInfoDevice($device){
        $this->conn = $this->getEntityManager();

        $sql = "INSERT INTO info_device
                          (token_device, 
                          so_device, 
                          cp_1, 
                          cp_2,
                          cp_3,
                          created,
                          updated)
                  VALUES (
                          '".$device->getTokenDevice()."',
                          '".$device->getSo()."',
                          '".$device->getCp1()."', 
                           ".$device->getCp2().",
                          '".$device->getCp3()."', 
                           NOW(),
                           NOW())";

        return $this->conn->querySet($sql);
    }

    /**
     * Update Device
     * @param Device $device
     * @return
     */
    public function updateInfoDevice($device){
        $this->conn = $this->getEntityManager();

        $sql = "UPDATE 
                        info_device 
                    SET 
                        cp_1 = '".$device->getCp1()."', 
                        cp_2 = '".$device->getCp2()."', 
                        cp_3 = '".$device->getCp3()."',
                        so_device = '".$device->getSo()."',
                        updated = NOW()
                    WHERE 
                        token_device = '".$device->getTokenDevice()."' ";

        return $this->conn->querySet($sql);
    }

    /**
     * Get a device by token
     * @param Device $device
     * @return
     */
    public function getInfoDeviceByToken($device){
        $this->conn = $this->getEntityManager();

        $sql="SELECT 
                  id.token_device 
              FROM 
                  info_device AS id 
              WHERE 
                  id.token_device = '".$device->getTokenDevice()."'";

        return $this->conn->queryParamsSingle($sql);
    }

    /**
     * Get all Devices by postal code
     * @param string $cpPrincipal
     * @param null $cp2
     * @param null $cp3
     * @param $SO
     * @return array
     */
    function getDevices($cpPrincipal = "", $cp2 = null, $cp3 = null, $SO) {
        $this->conn = $this->getEntityManager();

        if ($cpPrincipal == "") {
            // Get all devices by SO
            $sql="SELECT 
                        id.token_device, 
                        id.so_device 
                  FROM 
                        info_device AS id 
                  WHERE 
                        id.so_device = '$SO' ";
        } else {
            // Get only devices by postal code
            $sql="SELECT 
                        id.token_device, 
                        id.so_device 
                  FROM 
                        info_device AS id 
                  WHERE 
                        (id.cp_1 = '$cpPrincipal' 
                        OR id.cp_2 = '$cpPrincipal' 
                        OR id.cp_3 = '$cpPrincipal' ";

            if($cp2 != null){
                $sql .= " OR id.cp_1 = '$cp2' 
                        OR id.cp_2 = '$cp2' 
                        OR id.cp_3 = '$cp2'";
            }
            if($cp3 != null){
                $sql .= " OR id.cp_1 = '$cp3' 
                        OR id.cp_2 = '$cp3' 
                        OR id.cp_3 = '$cp3'";
            }

            $sql .= ")  
                    AND id.so_device = '$SO' ";
        }

        return $this->conn->queryParamsArray($sql);
    }
}
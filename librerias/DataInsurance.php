<?php

if(isset($_SESSION['factura_Nombre']) || isset($_SESSION['fact_nombre'])){
    include_once "controller/Conexion.php";
    include_once "modelo/Insurance.php";
}else{
    include_once "../controller/Conexion.php";
    include_once "../modelo/Insurance.php";
}

class DataInsurance
{

    private $conn;

    private function getEntityManager(){
        return new Logeo();
    }

    /**
     * Get misa
     * @param Misa $misa
     * @return
     */
    public function getAll(){
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    c.name
                  FROM company c
                  WHERE c.insurance = 1 ";

        return $this->conn->queryParamsArray($sql);
    }

    public function getInsurance($insurance)
    {
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    c.id
                  FROM company c
                  WHERE 
                      c.insurance = 1 
                      AND c.name = '$insurance' ";

        return $this->conn->queryParamsArray($sql)[0]['id'];
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: javi
 * Date: 25/01/19
 * Time: 22:28
 */

use modelo\Invoice;

require_once("controller/Conexion.php");
require_once("modelo/Invoice.php");

class DataInvoice
{

    private $conn;

    private function getEntityManager(){
        return Logeo::singleton();
    }

    /**
     * Insert new invoice
     * @param Invoice $invoice
     * @return
     */
    public function insertInvoice($invoice){
        $this->conn = $this->getEntityManager();

        $sql = "INSERT INTO invoice 
                    ( referencia,  
                        created,
                        status_id) 
                     VALUES ( 
                        '".$invoice->getReferencia()."',
                        NOW(), 
                        '".$invoice->getStatus()."')";

        return $this->conn->querySet($sql);
    }

    /**
     * Update status invoice
     * @param Invoice $invoice
     * @param string $item
     * @return bool
     */
    public function updateStatusInvoice(Invoice $invoice, $item = "death"){
        $this->conn = $this->getEntityManager();

        $inv = $this->getInvoiceByReference($item, $invoice->getReferencia())[0];

        if($inv != null){
            $invoice->setReferencia($inv['referencia']);
            $sql = "UPDATE invoice AS i  
                    SET i.status_id = '".$invoice->getStatus()."', 
                    i.updated = NOW()
                    WHERE referencia = '".$invoice->getReferencia()."' 
                    AND id = ".$invoice->getId()." ";

            return $this->conn->querySet($sql);
        }else{
            return false;
        }
    }

    public function updatePublication(Invoice $publication, $item, $invoice){
        $this->conn = $this->getEntityManager();

        $sql = "UPDATE $item SET invoice_id = $invoice WHERE code = '".$publication->getCode()."'";

        return $this->conn->querySet($sql);
    }

    /**
     * Get all invoices by company
     * @param Misa $misa
     * @return
     */
    public function getInvoiceByCompany($company){
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    i.referencia,
                    i.name,
                    i.lastName,
                    i.phone,
                    i.price,
                    DATE_FORMAT(i.created, '%d-%m-%Y %H:%i') AS created, 
                    d.nombre,
                    d.apellidos,
                    d.cp_1,
                    d.cp_2,
                    d.cp_3
                  FROM invoice i
                  LEFT JOIN death AS d 
                  ON i.death_id = d.id
                  LEFT JOIN company AS c 
                  ON d.insurance_id = c.id
                  WHERE c.id = $company";

        return $this->conn->queryParamsArray($sql);
    }

    /**
     * Get last reference invoice by company
     * @param $item
     * @param $user
     * @param null $code
     * @return mixed
     */
    public function getInvoiceByUser($item, $user, $code = null)
    {
        $this->conn = $this->getEntityManager();

        if($item == "death"){
            $a = "d";
            $b = "death";
        }else{
            $a = "c";
            $b = "ceremony";
        }

        $sql = "SELECT 
                    u.username,
                    i.referencia,
                    $a.name,
                    $a.last_name,
                    $a.phone,
                    DATE_FORMAT($a.created, '%d-%m-%Y %H:%i') AS created, 
                    $a.code,
                    $a.price,
                    co.name AS name_company,
                    co.nif AS nif_company,
                    co.phone1,
                    u.cp,
                    $a.id AS ".$b."_id,
                    si.status AS status_invoice,
                    co.address,
                    u.user_id
                FROM $b $a 
                LEFT JOIN user AS u 
                        ON $a.user_id = u.id 
                LEFT JOIN invoice AS i 
                    ON $a.invoice_id = i.id 
                LEFT JOIN company AS co 
                    ON u.id = co.user_id
                LEFT JOIN status_invoice AS si 
                    ON i.status_id = si.id 
                WHERE $a.user_id = $user ";

        if($code != null){
            $sql .= "AND $a.code = '$code' ";
        }

        return $this->conn->queryParamsArray($sql)[0];
    }

    /**
     * Get last reference invoice by company
     * @param $company
     * @return mixed
     */
    public function getLastInvoice()
    {
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    MAX(i.referencia) AS last_reference
                  FROM invoice i ";

        return $this->conn->queryParamsArray($sql)[0]['last_reference'];
    }

    /**
     * Get last reference invoice by company
     * @param $lastInvoice
     * @return mixed
     */
    public function getCurrentInvoice($lastInvoice)
    {
        $year = date('Y');
        $newReference = (1001)."/".$year;
        if($lastInvoice != null){
            $arrInvoice = explode("/", $lastInvoice);
            if($year != $arrInvoice[1]){
                $newReference = (1001)."/".$year;
            }else{
                $newReference = (int)($arrInvoice[0]+1)."/".$year;
            }
            return $newReference;
        }
        return $newReference;
    }

    /**
     * Get last reference invoice by company
     * @param $item
     * @param User $user
     * @param null $date1
     * @param null $date2
     * @return mixed
     * @throws Exception
     */
    public function getAllPublications($item, $user = null, $date1 = null, $date2 = null)
    {
        $this->conn = $this->getEntityManager();

        if($item == "death"){
            $a = "d";
            $b = "death";
        }else{
            $a = "c";
            $b = "ceremony";
        }

        $sql = "SELECT 
                    u.username,
                    $a.id AS item_id,
                    $a.name,
                    $a.last_name,
                    $a.phone,
                    DATE_FORMAT($a.created, '%d-%m-%Y %H:%i') AS created, 
                    $a.code,
                    $a.price,
                    u.id,
                    $a.cp_1,
                    u.cp, 
                    $a.invoice_id AS invoice,
                    co.id AS company_id,
                    co.name AS company,
                    co.name_contact AS company_name_contact,
                    co.address,
                    co.phone1 AS phone,
                    co.account,
                    co.nif,
                    co.email AS company_email,
                    co.created AS company_created
                FROM $b $a 
                LEFT JOIN user AS u 
                    ON $a.user_id = u.id 
                LEFT JOIN company AS co 
                    ON u.id = co.user_id
                WHERE $a.created > '2019-03-01' 
                  AND $a.code != '' 
                  AND ($a.invoice_id IS NULL OR $a.invoice_id = '' )";

        $date3 = new DateTime($date2);
        $date3->modify("+1 day");
        $date3 = $date3->format("Y-m-d");

        if($date1 != null && $date2 != null && $date1 != "" && $date2 != ""){
            $sql .= " AND $a.created BETWEEN '".$date1."' AND '".$date3."' ";
        }

        if($user != null) {
            if ($user->getRango() == "normal") {
                $sql .= " AND $a.user_id = " . $user->getId() . "";
            } elseif ($user->getRango() == "grupo") {
                $sql .= "  AND ($a.user_id = " . $user->getId() . " OR u.user_id = " . $user->getId() . ")";
            }
        }

        return $this->conn->queryParamsArray($sql);
    }

    public function getAllInvoices($date1 = null, $date2 = null, $item = "death")
    {
        $this->conn = $this->getEntityManager();

        if($item == "death"){
            $a = "d";
            $b = "death";
        }else{
            $a = "c";
            $b = "ceremony";
        }

        $sql = "SELECT 
                    i.id,
                    i.referencia,
                    DATE_FORMAT (i.created, '%d/%m/%Y %H:%i') AS created, 
                    si.status,
                    i.updated,
                  	co.name AS company
                FROM $b $a 
                LEFT JOIN invoice AS i 
                	ON $a.invoice_id = i.id 
                LEFT JOIN status_invoice AS si  
                    ON i.status_id = si.id 
                LEFT JOIN user AS u 
                	ON $a.user_id = u.id
                LEFT JOIN company AS co 
                	ON u.id = co.user_id
                WHERE i.referencia != '' ";

        if($date1 != null && $date2 != null && $date1 != "" && $date2 != ""){
            $date3 = new DateTime($date2);
            $date3->modify("+1 day");
            $date3 = $date3->format("Y-m-d");
            $sql .= " AND i.created BETWEEN '".$date1."' AND '".$date3."' ";
        }

        $sql .= ' GROUP BY
                    i.id, 
                	i.referencia,
                    i.created,
                    si.status,
                    i.updated,
                    co.name ';

        return $this->conn->queryParamsArray($sql);
    }

    public function getInvoiceByReference($item, $reference = null)
    {
        $this->conn = $this->getEntityManager();

        if($item == "death"){
            $a = "d";
            $b = "death";
        }else{
            $a = "c";
            $b = "ceremony";
        }

        $sql = "SELECT 
                    u.username,
                    i.referencia,
                    $a.name,
                    $a.last_name,
                    $a.phone,
                    DATE_FORMAT(i.created, '%d-%m-%Y %H:%i') AS created, 
                    $a.code,
                    $a.price,
                    $a.user_id,
                    u.id,
                    si.status,
                    $a.cp_1,
                    u.cp,
                    si.id AS status_id,
                    $a.invoice_id,
                    co.id AS company_id,
                    co.name AS company,
                    co.name_contact AS company_name_contact,
                    co.address,
                    co.phone1 AS phone,
                    co.account,
                    co.nif,
                    co.email AS company_email,
                    co.created AS company_created,
                    i.id AS invoice_id
                FROM invoice i 
                LEFT JOIN $b AS $a 
                    ON i.id = $a.invoice_id
                LEFT JOIN user AS u 
                    ON $a.user_id = u.id  
                LEFT JOIN status_invoice AS si 
                  ON i.status_id = si.id 
                LEFT JOIN company AS co 
                  ON u.id = co.user_id 
                WHERE i.referencia = '".$reference."' ";

        return $this->conn->queryParamsArray($sql);
    }

    public function getPublicationByCode($item, $code)
    {
        $this->conn = $this->getEntityManager();

        if($item == "death"){
            $a = "d";
            $b = "death";
        }else{
            $a = "c";
            $b = "ceremony";
        }

        $sql = "SELECT 
                    $a.name,
                    $a.last_name,
                    $a.phone,
                    DATE_FORMAT($a.created, '%d-%m-%Y %H:%i') AS created, 
                    $a.code,
                    $a.price,
                    u.id,
                    $a.cp_1,
                    u.cp,
                    $a.invoice_id
                FROM $b $a 
                LEFT JOIN user AS u 
                    ON $a.user_id = u.id  
                WHERE $a.code = '".$code."' ";

        return $this->conn->queryParamsArray($sql);
    }

    /**
     * Get last reference invoice by company
     * @param $item
     * @param $user
     * @param $code
     * @return mixed
     */
    public function getPublication($item, $user, $code)
    {
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                      d.id,
                      d.nombre, 
                      d.apellidos, 
                      d.apodo, 
                      d.edad, 
                      DATE_FORMAT(d.fecha_fallecimiento, '%d-%m-%Y') AS fecha_fallecimiento,
                      d.premium, 
                      d.cp_1, 
                      d.cp_2, 
                      d.cp_3, 
                      d.conyuge, 
                      d.hijos, 
                      d.hijos_politicos, 
                      d.hermanos, 
                      d.hermanos_politicos, 
                      d.familiares, 
                      d.pareja, 
                      d.padres, 
                      d.localidad_ceremonia, 
                      d.localidad_ceremonia2, 
                      DATE_FORMAT(d.fecha_ceremonia, '%d-%m-%Y') AS fecha_ceremonia, 
                      d.hora_ceremonia, 
                      DATE_FORMAT(d.fecha_ceremonia2, '%d-%m-%Y') AS fecha_ceremonia2, 
                      d.hora_ceremonia2, 
                      DATE_FORMAT(d.created, '%d-%m-%Y') AS created, 
                      u.username, 
                      d.invoice_id,
                      d.code, 
                      d.name AS name_contact,
                      d.last_name AS last_name_contact,
                      d.phone AS phone_contact, ";

        if($item === "death"){
            $sql .= " d.foto, 
                      d.foto2, 
                      d.direccion, 
                      d.insurance_id, 
                      d.localidad, 
                      d.texto, 
                      d.duelo_recibe, 
                      d.duelo_despide,
                      d.iglesia_ceremonia,
                      d.iglesia_ceremonia2,
                      d.contact_phone1,
                      d.contact_phone2
                      FROM 
                        death AS d 
                        LEFT JOIN invoice AS i
                        ON d.invoice_id = i.id";
        }else{
            $sql .= "   d.aniversario,
                        d.paid
                      FROM 
                        ceremony AS d 
                        LEFT JOIN invoice AS i
                        ON d.invoice_id = i.id";
        }

        $sql .= " LEFT JOIN user AS u 
                  ON d.user_id = u.id
                WHERE 
                    u.username = '$user' ";

        if($code != null){
            $sql .= " AND d.code = '$code' ";
        }

        return $this->conn->queryParamsArray($sql)[0];
    }

    /**
     * Get last reference invoice by company
     * @param $item
     * @param $user
     * @param $code
     * @return mixed
     */
    public function updateFieldsPublication($idP, $publi)
    {
        $this->conn = $this->getEntityManager();

        $sql = "UPDATE 
                    death AS d 
                SET 
                     d.nombre = '".$publi['nombre']."', 
                      d.apellidos = '".$publi['apellidos']."', 
                      d.apodo = '".$publi['apodo']."', 
                      d.localidad = '".$publi['localidad']."', 
                      d.edad = '".$publi['edad']."', 
                      d.fecha_fallecimiento = '".$publi['fecha_fallecimiento']."',
                      d.premium = '".$publi['premium']."', 
                      d.cp_1 = '".$publi['cp_1']."', 
                      d.cp_2 = '".$publi['cp_2']."', 
                      d.cp_3 = '".$publi['cp_3']."', 
                      d.conyuge = '".$publi['conyuge']."', 
                      d.hijos = '".$publi['hijos']."', 
                      d.hijos_politicos = '".$publi['hijos_politicos']."', 
                      d.hermanos = '".$publi['hermanos']."', 
                      d.hermanos_politicos = '".$publi['hermanos_politicos']."', 
                      d.familiares = '".$publi['familiares']."', 
                      d.pareja = '".$publi['pareja']."', 
                      d.padres = '".$publi['padres']."', 
                      d.localidad_ceremonia = '".$publi['localidad_ceremonia']."', 
                      d.iglesia_ceremonia = '".$publi['iglesia_ceremonia']."', 
                      d.fecha_ceremonia = '".$publi['fecha_ceremonia']."', 
                      d.hora_ceremonia = '".$publi['hora_ceremonia']."', 
                      d.localidad_ceremonia2 = '".$publi['localidad_ceremonia2']."', 
                      d.iglesia_ceremonia2 = '".$publi['iglesia_ceremonia2']."', ";

            if($publi['fecha_ceremonia2'] != null){
                    $sql .= "d.fecha_ceremonia2 = '".$publi['fecha_ceremonia2']."', ";
            }
                $sql .= "d.hora_ceremonia2 = '".$publi['hora_ceremonia2']."', 
                      d.name = '".$publi['name_contact']."',
                      d.last_name = '".$publi['last_name_contact']."',
                      d.phone = '".$publi['phone_contact']."',
                      d.texto = '".$publi['texto']."'                    
                WHERE d.id = '".$idP."'";

        print_r($sql);
        return $this->conn->querySet($sql);
    }

    /**
     * Get last reference invoice by company
     * @param $item
     * @param $user
     * @param $code
     * @return mixed
     */
    public function delInvoice($referencia)
    {
        $this->conn = $this->getEntityManager();

        $sql = "DELETE FROM invoice WHERE referencia = '".$referencia."'";

        return $this->conn->queryParamsArray($sql)[0];
    }

    /**
     * Get all data by company Aurora
     * @return mixed
     */
    public function getDataAurora(){
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    ad.name,
                    ad.subname,
                    ad.eslogan,
                    ad.address,
                    ad.phone1,
                    ad.phone2,
                    ad.text_conditions_pay,
                    ad.days_pay,
                    ad.iva,
                    ad.iban,
                    ad.email
                FROM aurora_data AS ad ";

        return $this->conn->queryParamsArray($sql)[0];
    }

    /**
     * Get data company by
     * @return mixed
     */
    public function getDataCompanyById($id){
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    co.id AS company_id,
                    co.name AS company,
                    co.name_contact AS company_name_contact,
                    co.address,
                    co.phone1 AS phone,
                    co.account,
                    co.nif,
                    co.email AS company_email,
                    co.created AS company_created
                FROM company AS co 
                WHERE co.id = $id ";

        return $this->conn->queryParamsArray($sql)[0];
    }

    /**
     * Get all posibles status for invoices
     * @return array
     */
    public function getAllStatus(){
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    si.status,
                    si.description
                FROM status_invoice AS si ";

        return $this->conn->queryParamsArray($sql);
    }

    /**
     * Get name by status
     * @param $status
     * @return mixed
     */
    public function getStatuByName($status){
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    si.id,
                    si.status
                FROM status_invoice AS si 
                WHERE si.status = '".$status."'";

        return $this->conn->queryParamsArray($sql)[0]['id'];
    }
}
<?php
// /librerias/DataMisa.php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 25/01/19
 * Time: 22:28
 */
include_once "../controller/Conexion.php";
include_once "../modelo/Misa.php";

class DataMisa
{

    private $conn;

    private function getEntityManager(){
        return new Logeo();
    }

    /**
     * Insert new misa
     * @param Misa $misa
     * @return
     */
    public function insertMisa($misa){
        $this->conn = $this->getEntityManager();

        $price = $this->conn->getElementPrice("misas móviles", "normal")['precio'];
        $sql = "INSERT INTO 
                      ceremony
                      (nombre,
                      apellidos, 
                      apodo, 
                      edad, 
                      fecha_fallecimiento, 
                      premium, 
                      conyuge, 
                      hijos, 
                      hijos_politicos, 
                      hermanos, 
                      hermanos_politicos, 
                      familiares, 
                      pareja, 
                      padres, 
                      localidad_ceremonia, 
                      fecha_ceremonia, 
                      hora_ceremonia, 
                      aniversario,
                      cp_1,
                      cp_2,
                      cp_3,
                      localidad_ceremonia2,
                      fecha_ceremonia2,
                      hora_ceremonia2,
                      paid, created,
                      phone, price )
              VALUES (
                      '".$misa->getName()."',
                      '".$misa->getLastName()."',
                      '".$misa->getNickName()."', 
                       ".$misa->getAge().",
                      '".$misa->getDateDeath()."',
                      '".$misa->getPremium()."',
                      '".$misa->getConyuge()."',
                      '".$misa->getChildren()."',
                      '".$misa->getPoliticalChildren()."',
                      '".$misa->getBrothers()."',
                      '".$misa->getPoliticalBrothers()."',
                      '".$misa->getFamily()."',
                      '".$misa->getHusband()."',
                      '".$misa->getPartner()."',
                      '".$misa->getPlaceCeremony()."',
                      '".$misa->getDateCeremony()."',
                      '".$misa->getHourCeremony()."',
                      '".$misa->getAniversary()."',
                      '".$misa->getCp1()."',
                      '".$misa->getCp2()."', 
                      '".$misa->getCp3()."', 
                      '".$misa->getPlaceCeremony2()."',
                      '".$misa->getDateCeremony2()."',
                      '".$misa->getHourCeremony2()."', 
                      '".$misa->getPay()."',
                      NOW(),
                      '".$misa->getPhone()."',
                      '".$price."')";

        return $this->conn->querySet($sql);
    }

    /**
     * Update misa paid to true
     * @param Misa $misa
     * @return
     */
    public function updateMisa($misa){
        $this->conn = $this->getEntityManager();

        $sql = "UPDATE ceremony 
                SET paid = 1 
                WHERE nombre = '".$misa->getName()."' 
                AND apellidos = '".$misa->getLastName()."' 
                AND edad = '".$misa->getAge()."' ";

        return $this->conn->querySet($sql);
    }

    /**
     * Get misa
     * @param Misa $misa
     * @return
     */
    public function getMisa($misa){
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    c.cp_1,
                    c.cp_2,
                    c.cp_3
                   FROM ceremony c
                  WHERE c.nombre = '".$misa->getName()."' 
                  AND c.apellidos = '".$misa->getLastName()."' 
                  AND c.edad = '".$misa->getAge()."' ";

        return $this->conn->queryParamsSingle($sql);
    }

    public function lastInsertId(){
        $this->conn = $this->getEntityManager();
        return $this->conn->lastInsertId();
    }
}
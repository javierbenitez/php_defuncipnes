<?php
// /librerias/DataUser.php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 25/01/19
 * Time: 22:28
 */

if(isset($_GET['cod'])){
    require_once("controller/Conexion.php");
    require_once("modelo/User.php");
}else{
    require_once("../controller/Conexion.php");
    require_once("../modelo/User.php");
}


class DataUser
{

    private $conn;

    private function getEntityManager(){
        return new Logeo();
    }

    /**
     * Insert new user
     * @param User $user
     * @return
     */
    public function insertUser($user){
        $this->conn = $this->getEntityManager();

        $sql = "INSERT INTO 
                          user
                          (username, 
                          username_canonical, 
                          email, 
                          email_canonical, 
                          enabled, 
                          password, 
                          rango, 
                          roles, 
                          sessions, 
                          user_id, 
                          price, 
                          cp, 
                          localidad, 
                          created)
                  VALUES (
                          '".$user->getUsername()."',
                          '".$user->getUsername()."',
                          '".$user->getEmail()."',
                          '".$user->getEmail()."',
                          1, 
                          '".$user->getPassword()."',
                          '".$user->getRango()."',
                          '".$user->getRoles()."',
                          '".$user->getSessions()."',
                          ".$user->getUser().",
                          '".$user->getPrice()."',
                          '".$user->getCp()."',
                          '".$user->getLocalidad()."',
                          '".$user->getCreated()->format('Y-m-d H:i')."'
                          )";

        return $this->conn->querySet($sql);
    }

    /**
     * Change user to active
     * @param User $user
     * @return
     */
    public function setActiveUser($user, $act){
        $this->conn = $this->getEntityManager();

        $sql = "UPDATE user u
                SET u.enabled = $act 
                WHERE 
                    u.username = '".$user->getUsername()."' ";

        if($user->getCp() != null && $user->getCp() != ""){
            $sql .= "AND u.cp = '".$user->getCp()."'";
        }

        return $this->conn->queryParamsSingle($sql);
    }


    /**
     * Get user by cp and username
     * @param User $user
     * @return
     */
    public function getUserByCPAndUsername($user){
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    u.enabled,
                    u.username,
                    u.email,
                    u.password,
                    u.roles,
                    u.rango,
                    u.cp,
                    u.localidad,
                    u.created,
                    u.updated,
                    c.name,
                    c.phone1,
                    c.account,
                    c.address,
                    c.nif,
                    c.name_contact,
                    u.user_id,
                    u.id,
                    u.price
                  FROM user u 
                  LEFT JOIN company AS c 
                  ON u.id = c.user_id
                  WHERE 
                    u.username = '".$user->getUsername()."' ";

        if($user->getCp() != null && $user->getCp() != ""){
            $sql .= "AND u.cp = '".$user->getCp()."'";
        }

        return $this->conn->queryParamsArray($sql);
    }

    /**
     * Get user
     * @param User $user
     * @return
     */
    public function getUserLogged(){
        $this->conn = $this->getEntityManager();
        $user = $_SESSION['usuario'];

        $sql = "SELECT 
                    u.username,
                    u.email,
                    u.password As pass,
                    u.roles,
                    u.created,
                    u.updated
                   FROM user u
                  WHERE u.username = '".$user."' ";

        return $this->conn->queryParamsSingle($sql);
    }

    public function getUsersByUser($user)
    {
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    u.username,
                    u.localidad AS localidad,
                    u.cp AS codigo_postal,
                    u.created AS alta,
                    u.user_id AS userId,
                    u.price AS precio,
                    u.id
                  FROM user u
                  LEFT JOIN company AS c 
                  ON u.id = c.user_id
                  WHERE u.user_id = '$user' 
                      OR u.id = '$user' 
                  ORDER BY u.user_id ASC";

        return $this->conn->queryParamsArray($sql);
    }

    public function getUserById($user)
    {
        $this->conn = $this->getEntityManager();

        $sql = "SELECT 
                    u.id,
                    u.username,
                    u.localidad AS localidad,
                    u.cp AS codigo_postal
                  FROM user u 
                  WHERE u.id = '$user' ";

        return $this->conn->queryParamsArray($sql);
    }

    public function lastInsertId(){
        $this->conn = $this->getEntityManager();
        return $this->conn->lastInsertId();
    }
}
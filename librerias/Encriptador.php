<?php
	define('METHOD','AES-256-CBC');
    define('SECRET_IV','1017112');
    class Encriptador {
        public static function encriptar($string,$SECRET_KEY){

            $key=hash('sha256', $SECRET_KEY);
            $iv=substr(hash('sha256', SECRET_IV), 0, 16);
            $output=openssl_encrypt($string, METHOD, $key, 0, $iv);
            $output=base64_encode($output);
            return $output;
        }
        public static function  desencriptar($string, $SECRET_KEY){
            $key=hash('sha256', $SECRET_KEY);
            $iv=substr(hash('sha256', SECRET_IV), 0, 16);
            $output=openssl_decrypt(base64_decode($string), METHOD, $key, 0, $iv);
            return $output;
        }
}
<?php
require("../controller/Conexion.php");
$result['estado'] = 1; // No request method POST

if($_SERVER['REQUEST_METHOD'] == 'POST') {

    $Librea = new Logeo();
    $conexion = $Librea->conectarBD();

    $body = json_decode(file_get_contents("php://input"), true);

    if($body != null){// Android
        if($body['typeOfDevice'] == 2){
            $SO = "Android";
        }else{
            $SO = $body['typeOfDevice'];
        }
        $tokenDevice = $body['tokenDevice']; //device Token para las notificaciones
        $cp1 = $body['cp1'];
        $cp2 = $body['cp2'];
        $cp3 = $body['cp3'];
    }else{//iOS
        if($_POST['typeOfDevice'] == 1){
            $SO = "iOS";
        }else{
            $SO = $_POST['typeOfDevice'];
        }
        $tokenDevice = $_POST['tokenDevice']; //device Token para las notificaciones
        $cp1 = $_POST['cp1'];
        $cp2 = $_POST['cp2'];
        $cp3 = $_POST['cp3'];
    }
    $typeDevice = $_SERVER["HTTP_USER_AGENT"];
    $date = new \DateTime();
    $date = $date->format('d-m-Y H:i:s');

    $result['estado'] = 2; // No token received

    mysqli_set_charset($conexion,"utf8");
    if ($tokenDevice != null) {
        //Buscar el codigo en la tabla de token y si esta actualizar los cp
        $sql="SELECT 
                  id.token_device 
              FROM 
                  info_device AS id 
              WHERE 
                  id.token_device = '$tokenDevice'";

        $row = mysqli_fetch_row(mysqli_query($conexion,$sql));


        if ($row[0] != null) {
            $result['estado'] = 3; //El token existe en la base de datos, por lo tanto modifico los códigos postales 
            
            $sql = "UPDATE 
                        info_device 
                    SET 
                        cp_1 = '$cp1', 
                        cp_2 = '$cp2', 
                        cp_3 = '$cp3',
                        so_device = '$SO',
                        updated = NOW()
                    WHERE 
                        token_device = '$tokenDevice' ";
            $result['type_device'] = $typeDevice; 
            mysqli_query($conexion,$sql);

        } else {
            $result['estado'] = 4; //El token no existe en la base de datos, por lo tanto añado un registro nuevo con los códigos postales
            $sql="INSERT INTO 
                        info_device (token_device, so_device, cp_1, cp_2, cp_3) 
                        VALUES ('$tokenDevice', '$SO', '$cp1', '$cp2', '$cp3')";
            mysqli_query($conexion,$sql);

        }
    }
    $Librea->desconectarBD($conexion);

//    // Insertar datos en Firebase
//    $data = '{
//                "token_device":"'.$tokenDevice.'",
//                "device":"'.$typeDevice.'",
//                "created":"'.$date.'"
//            }';
//    $url = "https://auroraservicios-e0f24.firebaseio.com/token_users.json";
//
//    $headers = [
//        "Authorization: key=".API_ACCESS_KEY,
//        "Content-Type: text/plain"
//    ];
//
//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, $url);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($ch, CURLOPT_POST, true);
//    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//    $response =  curl_exec($ch);
//
//    if(curl_errno($ch)){
//        $result = 'Error: '.curl_errno($ch);
//    }else{
//        $result = 5;
//    }
//    curl_close($ch);
    // Fin insertar datos en Firebase
}

echo json_encode($result);


<?php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 3/02/19
 * Time: 19:49
 */

require_once("controller/Conexion.php");

class Insurances
{

    public static function getAll()
    {
        $conn = self::getEntityManager();

        $sql = "SELECT 
                    c.name
                  FROM company c
                  WHERE c.insurance = 1 ";

        return $conn->queryParamsArray($sql);
    }

    private static function getEntityManager(){
        return new Logeo();
    }
}
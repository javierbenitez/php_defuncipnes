<?php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 3/02/19
 * Time: 19:49
 */

class Random
{

    public static function getStringRandom($length = 2){
        $dat = new DateTime();
        $numRand = rand(10,99);
        $charRand = self::generateRandomString($length);
        $codigo = substr($dat->format('sihjm'), 0, 8).$numRand.$charRand;
        return $codigo;
    }

    private static function generateRandomString($length = 2){
        return substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }
}
<?php
require("Conexion.php");
include_once "Encriptador.php";
$result['estado'] = 1; // No request method POST

if($_SERVER['REQUEST_METHOD'] == 'POST') {

    $Librea = new Logeo();
    $conexion = $Librea->conectarBD();

    $companyNif = $_POST['company'];
    $nif = Encriptador::encriptar($companyNif, "qCQb+@E#LGaLe2E;+c,8xphSK4J_!,");


    mysqli_set_charset($conexion,"utf8");

    // Search into table empresas if exists that company
    $sql="SELECT 
                    e.NIF, 
                    e.premium AS premium
              FROM 
                    empresas AS e 
              WHERE e.NIF = '$nif' ";

    $result['estado'] = 2; // Company not exists

    $row = mysqli_fetch_row(mysqli_query($conexion,$sql));

    if ($row != null) {
        $result['estado'] = 3; // Company exists

        if($row[1] == 0){
            $premium = 1;
            $result['premium'] = true;
        }else{
            $premium = 0;
            $result['premium'] = false;
        }

        $sql = "UPDATE 
                    empresas AS e 
                SET 
                    premium = '$premium',
                    updated = NOW()
                WHERE 
                    e.NIF = '$nif' ";

        $ok = mysqli_query($conexion,$sql);

        if ($ok){
            $result['estado'] = 4; // Company modified to premium/not premium
        }else{
            $result['estado'] = 5; // Company not modified to premium/not premium
        }
        $Librea->desconectarBD($conexion);
    }
}

echo json_encode($result);


<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

$nif=$_SESSION['nif'];
require("controller/Conexion.php");
include 'librerias/Encriptador.php';
$pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

$Conexion=new Logeo();

$Lista=$Conexion->Conseguir_CP_Faltantes();
$Lista_e=$Conexion->Conseguir_CP_Existetes();

?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>
<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container">
    <section class="row m-5-arriba">
        <div class="col-md-6">
            <table class="table">
                <tr class="header thead-dark text-center">
                    <th colspan="6"><h3>Buscar por...</h3></th>
                </tr>
                <tr class="header thead-dark">
                    <th><input class="buscar_por" type="text" id="nombre" onkeyup="Primer_campo()" placeholder="Codigo postal" title="Codigo postal"></th>
                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table">
                <tr class="header thead-dark text-center">
                    <th colspan="6"><h3>Buscar por...</h3></th>
                </tr>
                <tr class="header thead-dark">
                    <th><input class="buscar_por" type="text" id="nombre_2" onkeyup="Segundo_campo()" placeholder="Codigo postal" title="Codigo postal"></th>
                </tr>
            </table>
        </div>
    </section>
    <section class="row justify-content-center">
        <div class="col-md-6">
            <table id="Tabla_contenido" class="table table-hover table-striped">
                <thead>
                <tr class="header thead-dark text-center">
                    <th colspan="3">CODIGOS POSTALES SIN SERVICIO</th>
                </tr>
                <tr class="header thead-dark text-center">
                    <th>CP</th>
                    <th>Cantidad de peticiones</th>
                    <th>Eliminar</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($Lista as $fila){
                    echo "<tr>";
                    echo "<td class='align-middle text-center'>";
                    echo $fila[0];
                    echo "</td>";
                    echo "<td class='align-middle text-center'>";
                    echo $fila[1];
                    echo "</td>";
                    echo "<td class='align-middle text-center'>";
                    echo "<a href=\"eliminar_cp_faltante.php?u=$fila[0]\" class=\"btn btn-secondary btn-lg active\" role=\"button\" aria-pressed=\"true\">Eliminar</a>";
                    echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <table id="Tabla_contenido_2" class="table table-hover table-striped">
                <thead>
                <tr class="header thead-dark text-center">
                    <th colspan="3">CODIGOS POSTALES CON SERVICIO</th>
                </tr>
                <tr class="header thead-dark text-center">
                    <th>CP</th>
                    <th>Cantidad de peticiones</th>
                    <th>Eliminar</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($Lista_e as $fila){
                    echo "<tr>";
                    echo "<td class='align-middle text-center'>";
                    echo $fila[0];
                    echo "</td>";
                    echo "<td class='align-middle text-center'>";
                    echo $fila[1];
                    echo "</td>";
                    echo "<td class='align-middle text-center'>";
                    echo "<a href=\"eliminar_cp_faltante.php?u=$fila[0]\" class=\"btn btn-secondary btn-lg active\" role=\"button\" aria-pressed=\"true\">Eliminar</a>";
                    echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
</div>
<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script>
    function Primer_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("nombre");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Segundo_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("nombre_2");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido_2");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Tercer_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("fallecimiento");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Cuarto_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("ceremonia");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[3];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Quinto_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("lugar");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[5];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Por_hora() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("hora");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[6];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>
</body>
</html>
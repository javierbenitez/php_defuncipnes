<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"){
    header("Location:index.php");
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];
$d1 = null;
$d2 = null;

include 'librerias/Encriptador.php';
$pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

include_once "librerias/DataInvoice.php";
$repoInvoice = new DataInvoice();
include_once "controller/InvoiceController.php";
$controller = new InvoiceController();

if(isset($_POST['action_inovices']) && isset($_POST['invoices_mark']) && !isset($_GET['generate'])){
    $invoices = explode(",", $_POST['invoices_mark']);
    $status = $_POST["action_inovices"];

    $controller->updateStatusManyInvoices($invoices, $status, $_SERVER['PHP_SELF']);
}

if (isset($_GET['sudates']) && isset($_GET['since_invoice'])
    && strlen(trim($_GET['since_invoice'])) > 9&& strlen(trim($_GET['until_invoice'])) > 9){
    $dateStart = DateTime::createFromFormat('d-m-Y',  trim($_GET['since_invoice']));
    $dateEnd = DateTime::createFromFormat('d-m-Y',  trim($_GET['until_invoice']));

    $d1 = $dateStart->format('Y-m-d');
    $d2 = $dateStart->format('Y-m-d');
}

$Lista = $repoInvoice->getAllInvoices($d1, $d2, "death");

$allStatus = $repoInvoice->getAllStatus();

?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>
<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container m-5-arriba">
    <section class="row">
        <header class="sheader">
            <h2>Facturas</h2>
        </header>
        <div class="panel panel-default sheader">
            <div class="panel-body">
                <form autocomplete="off" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" class="form-inline form-dates">
                    <div class="form-group">
                        <label for="email">Desde: </label>
                        <input autocomplete="off" type="text" name="since_invoice" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Hasta: </label>
                        <input autocomplete="off" type="text" name="until_invoice" class="form-control">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="sudates" class="btn btn-info"> Buscar</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-12">
            <table id="Tabla_contenido" class="table">
                <thead>
                    <tr class="header thead-dark text-center">
                        <th>Marcar</th>
                        <th>Referencia</th>
                        <th>Estado</th>
                        <th>Empresa</th>
                        <th>Fecha</th>
                        <th>Ver Factura</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach ($Lista as $fila){
                    echo "<tr>";
                        echo "<td class='align-middle text-center' >
                                <input type='checkbox' name='invoice_mark[]' class='mark' value='".$fila["id"]."__".$fila["referencia"]."' data-val='".$fila["id"]."__".$fila["referencia"]."' />";
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila["referencia"];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            if($fila["status"] != null){
                                echo $fila["status"];
                            }else{
                                echo "Sin acción";
                            }
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila["company"];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila["created"];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo "<a href=\"ver_factura.php?de=1&ref=$fila[referencia]\" target='_blank' class=\"btn btn-secondary\" role=\"button\" aria-pressed=\"true\">Ver Factura</a>";
                        echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="panel panel-default sheader">
            <div class="panel-body">
                <form autocomplete="off" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" class="form-inline form-dates">
                    <div class="form-group">
                        <input type="checkbox" id="check_all" name="check_all" class="mark">
                        <input type="hidden" name="invoices_mark" id="invoices_mark">
                        <label for="check_all" class="mark">Seleccionar todos </label>
                    </div>
                    <div class="form-group">
                        <select name="action_inovices" id="action_invoices" class="form-control">
                            <?php
                            foreach ($allStatus as $status){
                                echo '<option value="'.$status['status'].'"  title="'.$status['description'].'" ';
//                                if($status['status'] == $result['status_invoice']) echo ' selected ';
                                echo '>'.$status['status'].'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" id="sustatus" name="sustatus" class="btn btn-info" value="Aplicar"/>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script>
    $.datepicker.setDefaults($.datepicker.regional['es']);
    (function () {
        var dateStart, dateEnd, buttonApply;

        function initVars(){
            dateStart = $("input[name=since_invoice]");
            dateEnd = $("input[name=until_invoice]");
            buttonApply = $("#sustatus");
        }

        initVars();

        dateStart.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            endDate: "today",
            maxDate: "today",
            language: 'es'
        });

        dateEnd.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            endDate: "today",
            maxDate: "today",
            language: 'es'
        });

        $('#check_all').change(function () {
            $("input.mark").prop('checked', $(this).prop("checked"));
        });

        buttonApply.on('click', function (e) {
            e.preventDefault();
            var form = $(this).parent('div').parent('form');
            Swal.fire({
                title: '¿Estás seguro que quieres cambiar el estado de la/s factura/s?',
                text: "No debes cambiarlo a menos que estés seguro de lo que estás haciendo.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, cambiarlo'
            }).then((result) => {
                if (result.value) {
                    var inputCheck = $('#invoices_mark');
                    var arr = $('[name="invoice_mark[]"]:checked').map(function () {
                        return this.value;
                    }).get();
                    inputCheck.val(arr);
                    form.submit();
                }
            });
        });

        <?php
        if(isset($_GET['since_invoice'])){ ?>
            dateStart.val('<?php echo trim($_GET['since_invoice']); ?>');
        <?php }
        if(isset($_GET['until_invoice'])){ ?>
            dateEnd.val('<?php echo trim($_GET['until_invoice']); ?>');
        <?php } ?>
    })();
</script>
</body>
</html>
<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"){
    header("Location:index.php");
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];


include 'librerias/Encriptador.php';
$pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

include_once "librerias/DataInvoice.php";
$repoInvoice = new DataInvoice();
include_once "controller/InvoiceController.php";
$controller = new InvoiceController();

if (isset($_GET['sudates']) && isset($_GET['since_invoice'])
    && strlen(trim($_GET['since_invoice'])) > 9&& strlen(trim($_GET['until_invoice'])) > 9){
    $dateStart = DateTime::createFromFormat('d-m-Y',  trim($_GET['since_invoice']));
    $dateEnd = DateTime::createFromFormat('d-m-Y',  trim($_GET['until_invoice']));

    $Lista = $repoInvoice->getAllPublications("ceremony", null, $dateStart->format('Y-m-d'), $dateEnd->format('Y-m-d'));
}else{
    $Lista = $repoInvoice->getAllPublications("ceremony");
}

if(isset($_GET['generate']) && $_GET['generate'] == 1){
    $options = [];
    $b = true;
    $options['aurora'] = $repoInvoice->getDataAurora();

    $publications = explode(",", $_POST['invoices_mark']);
    $aux = explode("__", $publications[0])[1];
    $flag = true;
    if(strlen($_POST['invoices_mark']) > 1) {
        for ($i = 0; $i < count($publications); $i++) {
            $arrPublication = explode("__", $publications[$i]);
            $publication = $repoInvoice->getPublicationByCode("ceremony", $arrPublication[0])[0];
            if($publication['invoice_id'] > 0){
                $message = "Ya has generado esta factura, puedes verla en la sección 'ver facturas misas' ";
                header("Location: $_SERVER[PHP_SERVER]?m=$message");
                $flag = false;
                break;
            }else if ($aux != $arrPublication[1]) {
                $message = "No puedes facturar publicaciones de distintos usuarios";
                header("Location: $_SERVER[PHP_SERVER]?m=$message");
                $flag = false;
                break;
            }
        }
        if($flag === true){
            $options['company'] = $repoInvoice->getDataCompanyById($aux);
            $controller->generateInvoiceToManyItems("ceremony", $publications, $options);
        }
    }else{
        $message = "Debes seleccionar alguna publicación";
        header("Location: $_SERVER[PHP_SERVER]?m=$message");
    }
}

$allStatus = $repoInvoice->getAllStatus();

?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>
<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container m-5-arriba">
    <section class="row">
        <header class="sheader">
            <h2>MISAS</h2>
        </header>
        <div class="panel panel-default sheader">
            <div class="panel-body">
                <form autocomplete="off" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" class="form-inline form-dates">
                    <div class="form-group">
                        <label for="email">Desde: </label>
                        <input autocomplete="off" type="text" name="since_invoice" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Hasta: </label>
                        <input autocomplete="off" type="text" name="until_invoice" class="form-control">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="sudates" class="btn btn-info"> Buscar</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-12">
            <table id="Tabla_contenido" class="table">
                <thead>
                    <tr class="header thead-dark text-center">
                        <th>Marcar</th>
                        <th>Usuario</th>
                        <th>Apellidos, Nombre</th>
                        <th>Teléfono</th>
                        <th>Fecha</th>
                        <th>CP 1</th>
                        <th>Precio</th>
                        <th>Ver publicacion</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach ($Lista as $fila){
                    echo "<tr>";
                    echo "<td class='align-middle text-center' >
                                <input type='checkbox' name='invoice_mark[]' class='mark' value='".$fila["code"]."__".$fila["company_id"]."__".$fila["item_id"]."' data_company='".$fila["company"]."' />";
                    echo "</td>";
                    echo "<td class='align-middle text-center'>";
                    echo $fila["username"];
                    echo "</td>";
                    echo "<td class='align-middle text-center'>";
                    echo $fila["last_name"].", ".$fila["name"];
                    echo "</td>";
                    echo "<td class='align-middle text-center'>";
                    echo $fila["phone"];
                    echo "</td>";
                    echo "<td class='align-middle text-center'>";
                    echo $fila["created"];
                    echo "</td>";
                    echo "<td class='align-middle text-center'>";
                    echo $fila["cp_1"];
                    echo "</td>";
                    echo "<td class='align-middle text-center'>";
                    if($fila["price"] == "" || $fila["price"] == null) echo "0"; else echo $fila["price"];
                    echo " €";
                    echo "</td>";
                    echo "<td class='align-middle text-center'>";
                    echo "<a href=\"ver_publicacion.php?code=$fila[code]&user=$fila[username]\" target='_blank' class=\"btn btn-secondary\" role=\"button\" aria-pressed=\"true\">Ver publicacion</a>";
                    echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="panel panel-default sheader">
            <div class="panel-body">
                <form autocomplete="off" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" class="form-inline form-dates">
                    <div class="form-group">
                        <input type="checkbox" id="check_all" name="check_all" class="mark">
                        <input type="hidden" name="invoices_mark" id="invoices_mark" value="0">
                        <label for="check_all" class="mark">Seleccionar todos </label>
                    </div>
                    <?php if($rango == "admin"){  ?>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" name="sugenerate" id="sugenerate" value="Generar" />
                        </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </section>
</div>
<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script>
    $.datepicker.setDefaults($.datepicker.regional['es']);
    (function () {
        var dateStart, dateEnd, btnGenerateInvoice;

        function initVars(){
            dateStart = $("input[name=since_invoice]");
            dateEnd = $("input[name=until_invoice]");
            btnGenerateInvoice = $("#sugenerate");
        }

        initVars();

        dateStart.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            endDate: "today",
            maxDate: "today",
            language: 'es'
        });

        dateEnd.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            endDate: "today",
            maxDate: "today",
            language: 'es'
        });

        $('#check_all').change(function () {
            $("input.mark").prop('checked', $(this).prop("checked"));
        });

        btnGenerateInvoice.on('click', function (e) {
            e.preventDefault();

            var b = true;
            var arrCheckeds = $('[name="invoice_mark[]"]:checked').map(function () {
                return this.value.split("__")[1];
            }).get();

            var aux = arrCheckeds[0];
            for(var i = 1; i < arrCheckeds.length; i++){
                console.log(arrCheckeds[i]);
                if(aux  !== arrCheckeds[i]){
                    b = false;
                }
            }

            if(arrCheckeds.length <= 0){
                Swal.fire('Debes seleccionar alguna publicación');
            }else if(!b){
                Swal.fire('No puedes facturar publicaciones de usuarios distintos');
            }else{
                var form = $(this).parent('div').parent('form');
                var action = form.attr("action");
                action = action+"?generate=1";
                form.attr("action", action);
                $(this).value = "Generar";
                $(this).val("Generar");
                Swal.fire({
                    title: '¿Estás seguro que quieres facturas estas publicaciones?',
                    text: "No debes cambiarlo a menos que estés seguro de lo que estás haciendo.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: 'Si, cambiarlo'
                }).then((result) => {
                    if (result.value) {
                        var inputCheck = $('#invoices_mark');
                        var arr = $('[name="invoice_mark[]"]:checked').map(function () {
                            return this.value;
                        }).get();
                        inputCheck.val(arr);
                        form.submit();
                    }
                });
            }
        });

        <?php
        if(isset($_GET['since_invoice'])){ ?>
        dateStart.val('<?php echo trim($_GET['since_invoice']); ?>');
        <?php }
        if(isset($_GET['until_invoice'])){ ?>
        dateEnd.val('<?php echo trim($_GET['until_invoice']); ?>');
        <?php } ?>
    })();
</script>
</body>
</html>
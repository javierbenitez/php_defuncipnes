<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"){
    header("Location:index.php");
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

$nif=$_SESSION['nif'];
require("controller/Conexion.php");
include 'librerias/Encriptador.php';
$pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

$Conexion=new Logeo();

$Lista=$Conexion->Conseguir_Publicidad();


?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>
<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container m-5-arriba">

    <section class="row">
        <div class="col-md-12">
            <table width="100%" id="Tabla_contenido" class="table table">
                <thead>
                    <tr class="header thead-dark text-center">
                        <th>Codigo Postal</th>
                        <th>Nombre</th>
                        <th>Imagen</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach ($Lista as $fila){
                    echo "<tr>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[1];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[2];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo "<img width='200px' src='$fila[3]'>";
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo "<a data-href='eliminar_publicidad.php?c=$fila[0]' class='btn btn-secondary btn-del' role='button' >Eliminar Publicidad</a>";
                        echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
</div>
<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    (function () {
        var buttonDelete;

        function initVars(){
            buttonDelete = $('a.btn-del');
        }

        initVars();

        buttonDelete.on('click', function (e) {
            e.preventDefault();
            var href = $(this).attr("data-href");
            Swal.fire({
                title: '¿Estás seguro que quieres eliminar esta publicidad?',
                text: "No la podrás recuperar.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, eliminarla'
            }).then((result) => {
                if (result.value) {
                    window.location.href = href;
                }
            })
        });
    })();
</script>
</body>
</html>
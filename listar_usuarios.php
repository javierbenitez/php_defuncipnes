<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"){
    header("Location:index.php");
}

if(isset($_SESSION['users_group'])){
    unset($_SESSION['users_group']);
}

if(isset($_SESSION['user_father'])){
    unset($_SESSION['user_father']);
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

$nif=$_SESSION['nif'];
require("controller/Conexion.php");
include 'librerias/Encriptador.php';
$pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

$Conexion=new Logeo();

$Lista = $Conexion->Conseguir_Usuarios();

?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>
<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container">
    <section class="row">
        <header>
            <h2>Lista de usuarios</h2>
        </header>
        <div class="col-md-12">
            <table id="Tabla_contenido" class="table">
                <thead>
                    <tr class="header thead-dark text-center">
                        <th>Nombre</th>
                        <th>Contacto</th>
                        <th>Localidad</th>
                        <th>Direccion</th>
                        <th>Usuario</th>
                        <th>Telefono</th>
                        <th>Act/Desact</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach ($Lista as $fila){
                    echo "<tr>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[0];
                        echo "</td>";

                        echo "<td class='align-middle text-center'>";
                            echo $fila[1];
                        echo "</td>";

                        echo "<td class='align-middle text-center'>";
                            echo $fila[3].', '.$fila[2];
                        echo "</td>";

                        echo "<td class='align-middle text-center'>";
                            echo $fila[4];
                        echo "</td>";

                        echo "<td class='align-middle text-center'>";
                            echo '<form action="controller/UsuarioController.php" method="post" >
                                    <input type="hidden" value="'.$fila[6].'" name="username" />
                                    <input type="hidden" value="'.$fila[3].'" name="cp" />
                                    <input type="hidden" value="'.$fila[12].'" name="user_id" />
                                    <button type="submit" class="no-style" >'.$fila[6].'</button></form>';
                        echo "</td>";

                        echo "<td class='align-middle text-center'>";
                            echo $fila[7];
                        echo "</td>";

                        echo "<td class='align-middle text-center'><input class='cbx_active' data-user='".$fila[6]."' data-cp='".$fila[2]."' type='checkbox' ";
                        if($fila[8] == 1){
                            echo 'checked ';
                        }
                        echo '/>';
                        echo "</td>";

                        echo "<td class='align-middle text-center'>";
                            echo "<a href=\"eliminar_usuario.php?u=$fila[6]\" class=\"btn btn-secondary btn-lg active\" role=\"button\" aria-pressed=\"true\">Eliminar</a>";
                        echo "</td>";

                        echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
</div>
<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    (function () {
        $('.cbx_active').change(function(e){
            e.preventDefault();
            var that = $(this);
            var user = that.attr('data-user');
            var cp = that.attr('data-cp');


            $.ajax({
                type: "POST",
                url: "https://auroraservicios.es/controller/UsuarioController.php",
                data: {'user': user, 'cp': cp },
                dataType: 'json',
                success: function(data) {

                    if(data['active'] !== true){
                        status = 'activo';
                    }else{
                        status = 'no activo';
                    }

                    that.attr('title', 'Poner este usuario como '+status);
                    that.attr("checked", data);
                }
            });
        });
    })();
</script>

</body>
</html>
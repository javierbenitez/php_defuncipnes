<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"&&$_SESSION['rango']!="especial"){
    header("Location:index.php");
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

$nif=$_SESSION['nif'];
require("controller/Conexion.php");
include 'librerias/Encriptador.php';
$pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

$Conexion=new Logeo();

$Lista=$Conexion->Conseguir_Usuarios();


//print_r($Lista);
//foreach ($Lista as $fila){
//    print_r($fila);
//}
//echo "nada";
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">-->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/personalizado.css">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
</head>
<body style="background-color: #4dbaff">
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="https://auroraservicios.es">Inicio</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarsExample04">
        <ul class="navbar-nav mr-auto">
            <?php if($rango=="admin"){?>
                <li class="nav-item dropdown">
                    <a  class="nav-link dropdown-toggle Blanco" id="dropdown04" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Usuarios</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="Alta_usuario">Crear usuario</a>
                        <a class="dropdown-item" href="listar_usuarios.php">Ver usuarios</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle Blanco" id="dropdown04" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Publicaciones</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="crear_publicacion.php">Crear publicacion</a>
                        <a class="dropdown-item" href="listar_defunciones.php">Ver facturas</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a  class="nav-link dropdown-toggle Blanco" id="dropdown04" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Misas</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="crear_misa.php">Crear misa</a>
                        <a class="dropdown-item" href="listar_misas.php">Ver facturas misa</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle Blanco" id="dropdown04" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Publicidad</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="subir_publicidad.php">Subir publicidad</a>
                        <a class="dropdown-item" href="listar_publicidad.php">Ver publicidad</a>
                    </div>
                </li>
            <?php }else if($rango=="especial"){?>
                <li class="nav-item active">
                    <a class="nav-link" href="Alta_usuario">Crear Usuario</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="listar_usuarios_especial.php">Ver Usuarios</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="listar_publicidad_especial.php">Ver Publicidad</span></a>
                </li>
            <?php }else if($rango=="misas"){?>
                <li class="nav-item active">
                    <a class="nav-link" href="crear_misa.php">Crear Misa</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="listar_contenido_propio_misas.php">Ver Misas</span></a>
                </li>
            <?php }else{?>
                <li class="nav-item active">
                    <a class="nav-link" href="crear_publicacion.php">Crear Publicación</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="listar_contenido_propio.php">Ver publicaciones</span></a>
                </li>
            <?php }?>
        </ul>
    </div>
</nav>
<div class="container">
    <section class="row m-5-arriba">
        <div class="col-md-12">
            <table class="table">
                <tr class="header thead-dark text-center">
                    <th colspan="6"><h3>Buscar por...</h3></th>
                </tr>
                <tr class="header thead-dark">
                    <th><input class="buscar_por" type="text" id="nombre" onkeyup="Primer_campo()" placeholder="NIF" title="NIF"></th>
                    <th><input class="buscar_por" type="text" id="dni" onkeyup="Segundo_campo()" placeholder="Nombre" title="Nombre"></th>
                    <th><input class="buscar_por" type="text" id="fallecimiento" onkeyup="Tercer_campo()" placeholder="Codigo Postal" title="Codigo Postal"></th>
                    <th><input class="buscar_por" type="text" id="ceremonia" onkeyup="Cuarto_campo()" placeholder="Direccion" title="Direccion"></th>
                    <th><input class="buscar_por" type="text" id="lugar" onkeyup="Quinto_campo()" placeholder="Usuario" title="Usuario"></th>
                </tr>
            </table>
        </div>
    </section>
    <section class="row">
        <div class="col-md-12">
            <table id="Tabla_contenido" class="table table-hover table-striped">
                <thead>
                    <tr class="header thead-dark text-center">
                        <th>NIF</th>
                        <th>Nombre</th>
                        <th>Codigo Postal</th>
                        <th>Direccion</th>
                        <th>Cuenta</th>
                        <th>Usuario</th>
                        <th>Gerente</th>
                        <th>Numero</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach ($Lista as $fila){
                    echo "<tr>";
                        echo "<td class='align-middle text-center'>";
                            echo Encriptador::desencriptar($fila[0],$pass);
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[1];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[2];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[3];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo Encriptador::desencriptar($fila[4],$pass);
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[5];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[6];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[7];
                        echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
        </div>
    </section>
</div>


<script>
    function Primer_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("nombre");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Segundo_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("dni");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Tercer_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("fallecimiento");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Cuarto_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("ceremonia");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[3];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Quinto_campo() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("lugar");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[5];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function Por_hora() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("hora");
        filter = input.value.toUpperCase();
        table = document.getElementById("Tabla_contenido");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[6];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>
<!--//BLOQUE COOKIES-->
<div id="barraaceptacion" style="display: block;">
        <div class="inner">
                Solicitamos su permiso para obtener datos estadísticos de su navegación en esta web, en cumplimiento del Real Decreto-ley 13/2012. Si continúa navegando consideramos que acepta el uso de cookies.<a href="javascript:void(0);" class="ok" onclick="PonerCookie();"><b>OK</b></a>|<a href="https://auroraservicios.es/politica_cookies.php" target="_blank" class="info">Más información</a>
            </div>
</div>
 
<script>
    function getCookie(c_name){
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1){
            c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1){
            c_value = null;
        }else{
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1){
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start,c_end));
        }
        return c_value;
    }

    function setCookie(c_name,value,exdays){
        var exdate=new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
        document.cookie=c_name + "=" + c_value;
    }

    if(getCookie('tiendaaviso')!="1"){
        document.getElementById("barraaceptacion").style.display="block";
    }
    function PonerCookie(){
        setCookie('tiendaaviso','1',365);
        document.getElementById("barraaceptacion").style.display="none";
    }
</script>
<!--//FIN BLOQUE COOKIES-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
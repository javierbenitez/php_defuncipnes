<?php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 26/01/19
 * Time: 21:36
 */

/**
 * Class Device
 */
class Device
{

    private $id;
    private $tokenDevice;
    private $so;
    private $cp1;
    private $cp2;
    private $cp3;
    private $created;
    private $updated;

    /**
     * Device constructor.
     * @param $tokenDevice
     * @param $created
     * @param $updated
     */
    public function __construct()
    {
        $this->created = new DateTime();
        $this->updated = new DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTokenDevice()
    {
        return $this->tokenDevice;
    }

    /**
     * @param mixed $tokenDevice
     */
    public function setTokenDevice($tokenDevice)
    {
        $this->tokenDevice = $tokenDevice;
    }

    /**
     * @return mixed
     */
    public function getSo()
    {
        return $this->so;
    }

    /**
     * @param mixed $so
     */
    public function setSo($so)
    {
        $this->so = $so;
    }

    /**
     * @return mixed
     */
    public function getCp1()
    {
        return $this->cp1;
    }

    /**
     * @param mixed $cp1
     */
    public function setCp1($cp1)
    {
        $this->cp1 = $cp1;
    }

    /**
     * @return mixed
     */
    public function getCp2()
    {
        return $this->cp2;
    }

    /**
     * @param mixed $cp2
     */
    public function setCp2($cp2)
    {
        $this->cp2 = $cp2;
    }

    /**
     * @return mixed
     */
    public function getCp3()
    {
        return $this->cp3;
    }

    /**
     * @param mixed $cp3
     */
    public function setCp3($cp3)
    {
        $this->cp3 = $cp3;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }



}
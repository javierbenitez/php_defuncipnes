<?php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 25/01/19
 * Time: 21:57
 */

/**
 * Class Misa
 */
class Misa
{

    private $aniversary;
    private $name;
    private $lastName;
    private $nickName;
    private $age;
    private $dateDeath;
    private $cp1;
    private $cp2;
    private $cp3;
    private $premium;
    private $conyuge;
    private $husband;
    private $widow;
    private $children;
    private $politicalChildren;
    private $brothers;
    private $politicalBrothers;
    private $family;
    private $partner;
    private $phone;
    private $placeCeremony;
    private $dateCeremony;
    private $hourCeremony;
    private $placeCeremony2 = "";
    private $dateCeremony2 = "";
    private $hourCeremony2 = "";
    private $pay;
    private $price;

    /**
     * Misa constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getPlaceCeremony2()
    {
        return $this->placeCeremony2;
    }

    /**
     * @param mixed $placeCeremony2
     */
    public function setPlaceCeremony2($placeCeremony2)
    {
        $this->placeCeremony2 = $placeCeremony2;
    }

    /**
     * @return mixed
     */
    public function getDateCeremony2()
    {
        return $this->dateCeremony2;
    }

    /**
     * @param mixed $dateCeremony2
     */
    public function setDateCeremony2($dateCeremony2)
    {
        $this->dateCeremony2 = $dateCeremony2;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }



    /**
     * @return mixed
     */
    public function getHourCeremony2()
    {
        return $this->hourCeremony2;
    }

    /**
     * @param mixed $hourCeremony2
     */
    public function setHourCeremony2($hourCeremony2)
    {
        $this->hourCeremony2 = $hourCeremony2;
    }

    /**
     * @return mixed
     */
    public function getConyuge()
    {
        return $this->conyuge;
    }

    /**
     * @param mixed $conyuge
     */
    public function setConyuge($conyuge)
    {
        $this->conyuge = $conyuge;
    }



    /**
     * @return mixed
     */
    public function getPay()
    {
        return $this->pay;
    }

    /**
     * @param mixed $pay
     */
    public function setPay($pay)
    {
        $this->pay = $pay;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $aniversary
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }


    /**
     * @return mixed
     */
    public function getAniversary()
    {
        return $this->aniversary;
    }

    /**
     * @param mixed $aniversary
     */
    public function setAniversary($aniversary)
    {
        $this->aniversary = $aniversary;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getNickName()
    {
        return $this->nickName;
    }

    /**
     * @param mixed $nickName
     */
    public function setNickName($nickName)
    {
        $this->nickName = $nickName;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getDateDeath()
    {
        return $this->dateDeath;
    }

    /**
     * @param mixed $dateDeath
     */
    public function setDateDeath($dateDeath)
    {
        $this->dateDeath = $dateDeath;
    }

    /**
     * @return mixed
     */
    public function getCp1()
    {
        return $this->cp1;
    }

    /**
     * @param mixed $cp1
     */
    public function setCp1($cp1)
    {
        $this->cp1 = $cp1;
    }

    /**
     * @return mixed
     */
    public function getCp2()
    {
        return $this->cp2;
    }

    /**
     * @param mixed $cp2
     */
    public function setCp2($cp2)
    {
        $this->cp2 = $cp2;
    }

    /**
     * @return mixed
     */
    public function getCp3()
    {
        return $this->cp3;
    }

    /**
     * @param mixed $cp3
     */
    public function setCp3($cp3)
    {
        $this->cp3 = $cp3;
    }

    /**
     * @return mixed
     */
    public function getPremium()
    {
        return $this->premium;
    }

    /**
     * @param mixed $premium
     */
    public function setPremium($premium)
    {
        $this->premium = $premium;
    }

    /**
     * @return mixed
     */
    public function getHusband()
    {
        return $this->husband;
    }

    /**
     * @param mixed $husband
     */
    public function setHusband($husband)
    {
        $this->husband = $husband;
    }

    /**
     * @return mixed
     */
    public function getWidow()
    {
        return $this->widow;
    }

    /**
     * @param mixed $widow
     */
    public function setWidow($widow)
    {
        $this->widow = $widow;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $chidren
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return mixed
     */
    public function getPoliticalChildren()
    {
        return $this->politicalChildren;
    }

    /**
     * @param mixed $politicalChildren
     */
    public function setPoliticalChildren($politicalChildren)
    {
        $this->politicalChildren = $politicalChildren;
    }

    /**
     * @return mixed
     */
    public function getBrothers()
    {
        return $this->brothers;
    }

    /**
     * @param mixed $brothers
     */
    public function setBrothers($brothers)
    {
        $this->brothers = $brothers;
    }

    /**
     * @return mixed
     */
    public function getPoliticalBrothers()
    {
        return $this->politicalBrothers;
    }

    /**
     * @param mixed $politicalBrothers
     */
    public function setPoliticalBrothers($politicalBrothers)
    {
        $this->politicalBrothers = $politicalBrothers;
    }

    /**
     * @return mixed
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * @param mixed $family
     */
    public function setFamily($family)
    {
        $this->family = $family;
    }

    /**
     * @return mixed
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param mixed $partner
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
    }

    /**
     * @return mixed
     */
    public function getPlaceCeremony()
    {
        return $this->placeCeremony;
    }

    /**
     * @param mixed $placeCeremony
     */
    public function setPlaceCeremony($placeCeremony)
    {
        $this->placeCeremony = $placeCeremony;
    }

    /**
     * @return mixed
     */
    public function getDateCeremony()
    {
        return $this->dateCeremony;
    }

    /**
     * @param mixed $dateCeremony
     */
    public function setDateCeremony($dateCeremony)
    {
        $this->dateCeremony = $dateCeremony;
    }

    /**
     * @return mixed
     */
    public function getHourCeremony()
    {
        return $this->hourCeremony;
    }

    /**
     * @param mixed $hourCeremony
     */
    public function setHourCeremony($hourCeremony)
    {
        $this->hourCeremony = $hourCeremony;
    }

}
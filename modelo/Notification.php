<?php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 26/01/19
 * Time: 21:04
 */

class Notification
{

    /**
     * Send notification with title and body to many devices
     * @param null $devices
     * @param $title
     * @param $body
     * @return mixed
     */
    function sendAndroidPush($devices = null, $title, $body) {

        $fcmUrl = "https://fcm.googleapis.com/fcm/send";
        define('API_ACCESS_KEY_GOOGLE',
            "AAAA14oj3yU:APA91bHKBmIeYZU91wtDr1F_mOOJites5Cue4LV9-t9Aay_Po6nsZl9LBf5njjtGHcrcIDKir4mxv7PGuMVlL8uOV2G5cKBthEGRlkfSg4u05U5FstOmh0c_QyPedaO-X8LmPntAcrir" ); //Falta defiinir la api key de la app de android



        $fields = array(
            "registration_ids" => $devices,
            "priority" => "normal",
            "notification" => [
                "title" => $title,
                "body" => $body
            ],
            "data" => [
                "id" => 156416,
                "missedRequests" => 5,
                "addAnyDataHere" => 123
            ],
            "action_destination" => "http://androiddeft.com"
        );

        $headers = array(
            "Authorization:key=" . API_ACCESS_KEY_GOOGLE,
            "Content-Type:application/json"
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);

        curl_close($ch);

//        return $result;
    }

    function sendIOSPush($deviceToken, $message){
        $body['aps'] = array(
            'alert' => trim($message),
            'sound' => 'default',
            'badge' => 1
        );
        // Encode the payload as JSON
        $payload = json_encode($body);

        $msg = chr(0) . pack('n', 32) . pack('H*', trim($deviceToken)) . pack('n', strlen($payload)) . $payload;
        // For sandbox
//        $url = 'ssl://gateway.sandbox.push.apple.com:2195';

        // For production
        $url = 'ssl://gateway.push.apple.com:2195';


        $passphrase = '';
        //echo $passphrase;
        $ctx = stream_context_create();
        //stream_context_set_option($ctx, 'ssl', 'local_cert', '../librerias/devAPNSAurora.pem');
        stream_context_set_option($ctx, 'ssl', 'local_cert', '/librerias/prodAPNSAurora.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Connection to APNS server
        $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp) {
            echo "<br>Failed to connect in notification server : $err $errstr" . PHP_EOL;
        }

        $result = fwrite($fp, $msg, strlen($msg));
        if (!$result){
            //echo '<br>Message not delivered' . PHP_EOL;
        } else {
            //echo '<br>Message successfully delivered' . PHP_EOL;
            return $result;
        }
        // Close the connection to the server
        fclose($fp);
    }
}
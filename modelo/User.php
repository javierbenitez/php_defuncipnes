<?php
/**
 * Created by PhpStorm.
 * User: javi
 * Date: 25/01/19
 * Time: 21:57
 */

/**
 * Class User
 */
class User
{

    private static $arrayRoles = ['ROLE_TANATORIO', 'ROLE_GROUP_TANATORIO'];
    private static $arrayRangos = ['admin', 'grupo', 'normal'];

    private $id;
    private $username;
    private $email;
    private $enabled;
    private $password;
    private $roles = [];
    private $sessions;
    private $created;
    private $updated;
    private $rango;
    private $fechaCreacion;
    private $localidad;
    private $cp;
    private $user;
    private $price;

    /**
     * Misa constructor.
     */
    public function __construct()
    {
        $this->created = new  \DateTime();
        $this->sessions = 1;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param mixed $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param array $role
     */
    public function setRoles($role)
    {
        if(!in_array($role, self::$arrayRoles)){
            $this->roles = self::$arrayRoles[0];
        }else{
            $this->roles = $role;
        }
    }

    /**
     * @return int
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * @param int $sessions
     */
    public function setSessions($sessions)
    {
        $this->sessions = $sessions;
    }

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getRango()
    {
        return $this->rango;
    }

    /**
     * @param mixed $rango
     */
    public function setRango($rango)
    {

        if(!in_array($rango, self::$arrayRangos)){
            $this->rango = self::$arrayRangos[2];
        }else{
            $this->rango = $rango;
        }

    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    }

    /**
     * @return mixed
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * @param mixed $localidad
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;
    }

    /**
     * @return mixed
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * @param mixed $cp
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        if($user != ""){
            $this->user = $user;
        }else{
            $this->user = "NULL";
        }
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }



}
<?php
session_start();
$result = [];
$result['estado'] = 1;

if(isset($_GET['p1']) && isset($_GET['p2']) && isset($_GET['p3']) && isset($_GET['p4']) && isset($_GET['p5'])){
    $keyClient = trim($_GET['p1']);
    $name = trim($_GET['p2']);
    $lastName = trim($_GET['p3']);
    $age = trim($_GET['p4']);
    $date = trim($_GET['p5']);
}

    // Se incluye la librería
    include 'librerias/apiRedsys.php';
    include_once "librerias/Random.php";
    include 'controller/Conexion.php';

    $libreria = new Logeo();
    // Se crea Objeto
    $miObj = new RedsysAPI;
    $nif = $_SESSION["nif"];

    $type = "normal";
//    if(isset($_SESSION['f_premiun'])){
////        if($_SESSION['f_premiun'] == 1){
////            $type = "premium";
////        }
////    }
    $precio = $libreria->getElementPrice("misas móviles", $type)['precio'];

    $precioArr = explode(".", $precio);
    $price = "0001";

    if(count($precioArr) > 1){
        if(strlen($precioArr[1]) < 2){
            $price = $precioArr[0].$precioArr[1][0]."0";
        }else{
            $price = $precioArr[0].$precioArr[1][0].$precioArr[1][1];
        }
    }else{
        $price = $precioArr[0]."00";
    }

    $codigo = Random::getStringRandom();
    $libreria->createFactMisaMobile($nif, $codigo, $price);

    $_SESSION["codigo"] = $codigo;

    // Valores de entrada que no hemos cmbiado para ningun ejemplo
    $company = "AURORA WEB";
    $fuc="326225190";
    $terminal="001";
    $moneda="978";
    $trans="0";
//	$url="https://sis-t.redsys.es:25443/sis/realizarPago"; // Test
    $url="https://sis.redsys.es/sis/realizarPago";
    $urlOK="https://auroraservicios.es/ko.php";
//    $urlKO="http://192.168.1.133/api/insertar_misa.php?p1=".$keyClient."&p2=".$name."&p3=".$lastName."&p4=".$age."&p5=".$date;
    $urlKO="https://auroraservicios.es/api/insertar_misa.php?p1=".$keyClient."&p2=".$name."&p3=".$lastName."&p4=".$age."&p5=".$date;

    // Se Rellenan los campos
    $miObj->setParameter("DS_MERCHANT_AMOUNT", $price);
    $miObj->setParameter("DS_MERCHANT_ORDER", $codigo);
    $miObj->setParameter("DS_MERCHANT_MERCHANTCODE", $fuc);
    $miObj->setParameter("DS_MERCHANT_MERCHANTNAME", $company);
    $miObj->setParameter("DS_MERCHANT_CURRENCY", $moneda);
    $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $trans);
    $miObj->setParameter("DS_MERCHANT_TERMINAL", $terminal);
    $miObj->setParameter("DS_MERCHANT_MERCHANTURL", $url);
    $miObj->setParameter("DS_MERCHANT_URLOK", $urlKO);
    //    $miObj->setParameter("DS_MERCHANT_URLKO", $urlOK);

    //Datos de configuración
    $version = "HMAC_SHA256_V1";
//    $kc = 'TMttQyUyN4aEDcxUS+MPvP2ODiclJLMH';//Clave recuperada de CANALES
    $kc = 'u7WqEfoXx/vg+I4kQVKHephX95J1PG5v';//Clave recuperada de CANALES
    // Se generan los parámetros de la petición
    $request = "";
    $params = $miObj->createMerchantParameters();
    $signature = $miObj->createMerchantSignature($kc);

    $result['estado'] = 2;

    echo json_encode($result);
    ?>

    <html lang="es">
    <head>
        <script languaje="javascript">
            function funcion_javascript(){
                // alert ("Esto es javascript");
                document.getElementById("frm").submit();
            }
        </script>
    </head>
    <body>
    <form id="frm" name="frm" action="<?php echo $url?>" method="POST">
        <input type="hidden" name="Ds_SignatureVersion" value="<?php echo $version; ?>"/></br>
        <input type="hidden" name="Ds_MerchantParameters" value="<?php echo $params; ?>"/></br>
        <input type="hidden" name="Ds_Signature" value="<?php echo $signature; ?>"/></br>
        <script languaje="javascript">
            funcion_javascript();
        </script>
    </form>

    </body>
    </html>
<?php

session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}
	// Se incluye la librería
	include 'librerias/apiRedsys.php';
    include_once "librerias/Random.php";
    include_once "librerias/DataInsurance.php";
    include_once "librerias/DataInvoice.php";
    use modelo\Invoice;
    include_once "controller/Conexion.php";
    $libreria=new Logeo();
	$miObj = new RedsysAPI;
	$repoInvoice = new DataInvoice();

	$nif = $_SESSION["nif"];
	$code = $_SESSION['factura_Codigo'];
	$typePublication = $_SESSION['typePublication'];

    $type = "normal";
	if(isset($_SESSION['f_premiun'])){
	    if($_SESSION['f_premiun'] == 1){
	        $type = "premium";
        }
    }

	if($typePublication != 'death'){
	    $t = "defunciones";
    }else{
	    $t = "misas";
    }

    $precio = $libreria->getElementPrice($t, $type);

	if($precio != null) {
	    $precio = $precio[1];
        $precioArr = explode(".", $precio);

        if (count($precioArr) > 1) {
            if (strlen($precioArr[1]) < 2) {
                $price = $precioArr[0] . $precioArr[1][0] . "0";
            } else {
                $price = $precioArr[0] . $precioArr[1][0] . $precioArr[1][1];
            }
        } else {
            $price = $precioArr[0] . "00";
        }
    }else{
        $price = "1000";
    }

    $lastInvoice = $repoInvoice->getLastInvoice();
    $newReference = $repoInvoice->getCurrentInvoice($lastInvoice);
    $invoice = new Invoice();
    $invoice->setStatus(3);
    $idInvoice = $repoInvoice->insertInvoice($invoice);
    $publication = new Invoice();
    $publication->setCode($code);
    $repoInvoice->updatePublication($publication, $typePublication, $idInvoice);

	// Valores de entrada que no hemos cmbiado para ningun ejemplo
    $company = "AURORA WEB";
	$fuc="326225190";
	$terminal="001";
	$moneda="978";
	$trans="0";
//	$price = 1005;
	$url="https://sis-t.redsys.es:25443/sis/realizarPago"; // Test
//	$url="https://sis.redsys.es/sis/realizarPago";
    $PATH = "http://defuncioneslocal.com/";
//    $PATH = "https://auroraservicios.es/";
    if($typePublication != 'death'){
        $urlOK = $PATH."insertar_misa.php?cod=".$code;
    }else{
        $urlOK = $PATH."insertar_publicacion.php?cod=".$code;
    }

	$urlKO="http://defuncioneslocal.com/ko.php";
	
	// Se Rellenan los campos
	$miObj->setParameter("DS_MERCHANT_AMOUNT",$price);
	$miObj->setParameter("DS_MERCHANT_ORDER",$code);
	$miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$fuc);
	$miObj->setParameter("DS_MERCHANT_CURRENCY",$moneda);
	$miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$trans);
	$miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
	$miObj->setParameter("DS_MERCHANT_MERCHANTURL",$url);
	$miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);
	$miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);

	//Datos de configuración
	$version="HMAC_SHA256_V1"; //qwertyasdf0123456789
	$kc = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7';//
	// Se generan los parámetros de la petición
	$request = "";
	$params = $miObj->createMerchantParameters();
	$signature = $miObj->createMerchantSignature($kc);


?>
<html lang="es">
<head>
    <script languaje="javascript">
        function funcion_javascript(){
            // alert ("Esto es javascript");
            document.getElementById("frm").submit();
        }
    </script>
</head>
<body>
<form id="frm" name="frm" action="<?php echo $url?>" method="POST">
<input type="hidden" name="Ds_SignatureVersion" value="<?php echo $version; ?>"/></br>
<input type="hidden" name="Ds_MerchantParameters" value="<?php echo $params; ?>"/></br>
<input type="hidden" name="Ds_Signature" value="<?php echo $signature; ?>"/></br>
    <script languaje="javascript">
        funcion_javascript();
    </script>
</form>
</body>
</html>

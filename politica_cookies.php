<?php
?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>;
</head>

<body style="background-color: #4dbaff">
<div class="container">
    <section class="row m-5-arriba">
        <div class="col-md-12 Borde_difuminado">
            <p><h5>Terminos de servicio</h5>
            Auroraweb S.L no se hace cargo de devoluciones o cancelaciones de ningún tipo, una vez realizado el pago, el cliente acepta la NO devolución del dinero y la NO cancelación de la publicación dada en la aplicación de auroraweb S.L.
            <br>
            Los datos facilitados por el cliente no serán difundidos a terceros bajo ningún concepto.
            <br>
            Auroraweb S.L no se hace responsable de publicaciones no veraces, siendo el cliente que ha dado dicha información el MAXIMO responsable de las sanciones legales y económicas que pudiera repercutir.
            <br>

            Si tiene dudas sobre esta política de cookies, puede contactar con auroraservicios en info@auroraservicios.es
            <br>
            <br>

        </div>
    </section>
    <section class="row m-5-arriba">
        <div class="col-md-12 Borde_difuminado">
            <p><h5>Aviso Información Legal y condiciones de Auroraweb S.L</h5>
            <br>

            BIENVENIDOS/AS, a Auroraweb S.L


            <br><br><br>
            Aviso Legal, Términos y Condiciones Generales de Uso, Particulares, Contratación y Política de Privacidad.
            <br>
            Condiciones de servicio de Auroraweb S.L
            <br>
            Última actualización: 19.10.2018

            <br><br>
            CÓDIGO DE HONOR..

            <br><br>
            El respeto a la privacidad, al igual que su seguridad, el buen uso, etc. es nuestro Código de Honor y uno de nuestros principios más importantes.



            <br><br><br><br><br>
            OBJETO Auroraweb S.L.

            <br><br>
            Nuestro objetivo es Innovar, modernizar el servicio de esquelas e informar a familiares, compañeros, conocidos, vecinos, ciudadanos, de forma rápida, sencilla y cómoda del fallecimiento de una persona o un ser querido en la App de su teléfono móvil.
            <br>
            Es muy frecuente que las personas hayan residido en varios sitios durante su vida, por medio de las esquelas es un medio de comunicación que los familiares, compañeros, amigos del fallecido puedan enterarse de su fallecimiento.
            <br>
            Auroraweb S.L es una aplicación directa y abierta para comunicar un fallecimiento. Ofrece la posibilidad de insertar la esquela de un ser querido a cualquier Usuario/a y recibir el aviso del fallecimiento en la App en el teléfono móvil.
            <br>
            El usuario podrá recibir en su App las esquelas de las zonas geográficas que seleccione o desea recibir, como por ejemplo su pueblo o ciudad y los pueblos vecinos, etc
            <br>
            Nos esforzamos al máximo diariamente para asegurarnos que la información que aparece en Auroraweb S.L es correcta y actual.
            <br>
            Auroraweb S.L le garantiza que las esquelas que publique, serán mantenidas online durante un plazo mínimo de 48 horas.






            <br><br><br><br><br>
            PARTES CONTRATANTES.

            <br><br>
            DISTRIBUCIONES CARDADOR MU OZ S.L, es el titular de Auroraservicios.es con CIF B14597728, Calle Gongora nº 9 local, Pozoblanco (14400), Cordoba, España.
            <br>
            Los usuarios/as serán la parte contratante de los servicios ofrecidos por Auroraweb S.L, que aportarán sus datos personales a través de los formularios correspondientes, al igual que serán los encargados de aportar los datos de las esquelas que se van a publicar. Cualquier falsedad, tanto en los datos personales como en los datos de las esquelas, son responsabilidad exclusiva del usuario/a final.
            <br>
            El uso de los servicios implica la aceptación sin reservas de todas y cada una de estas Condiciones Generales de Uso, Particulares, Contratación y Política de Privacidad, aquí reflejadas sin excepción. Te recomendamos que periódicamente las leas detenidamente.
            <br>
            Este aviso legal regula el acceso y uso de la web y su App Auroraservivios.es en adelante al decir Auroraweb, nuestros, nosotros, nuestro, nos estamos refiriendo a Demetrio Cardador Muñoz propiedad de Auroraweb S.L que está a disposición de los Usuarios/as de internet y damos acceso a los Usuarios/as a la múltiple, variada información e imágenes, programas, datos, servicio, en adelante “Contenido/s” pertenecientes a Auroraweb S.L o de terceros a los que el Usuario/a puede tener acceso.
            <br>
            El Aviso Legal y las Condiciones Generales de Uso, Política de Privacidad de este documento solo y únicamente tendrán derecho y validez en el idioma Español y no se acepta ninguna traducción.
            <br>
            Demetrio Cardador Habas se reserva el derecho de modificar en cualquier momento el Aviso Legal, Términos y Condiciones Generales de Uso, Particulares, Contratación y Política de Privacidad.




            <br><br><br><br><br>
            OBLIGACIONES DE LOS USUARIOS/AS.


            <br><br>
            Las Empresas de Servicios Funerarios tendrán su propio sitio en Auroraweb S.L en el cual podrán presentar y ofrecer sus servicios e informar de los fallecimientos a los Usuarios/as de Auroraweb por la inserción de una esquela de un fallecido en el sitio Web Auroraweb y/o recibir el aviso del fallecimiento en la App de su teléfono móvil.
            <br>
            El Usuario se compromete a hacer un uso diligente de Auroraweb y de los servicios accesibles desde este sitio Web, con total sujeción a las Condiciones Generales de Uso y Condiciones Particulares, a las buenas costumbres, buenas prácticas y a la ley.
            <br>
            Las citadas condiciones serán de aplicación independiente de las Condiciones Generales de Contratación que en su caso resulten de obligado cumplimiento.
            <br>
            Los usuarios/as son los únicos responsables de la veracidad de los datos aportados a Auroraweb y por consiguiente de las publicaciones con dichos datos.
            <br>
            Los Usuarios/as de Auroraweb que faciliten e inserten datos, fotos e imágenes de personas fallecidas se declaran autorizados, con licencia y/o permiso por los familiares que consienten y autorizan para comunicar públicamente a través de Auroraweb la publicación de estos datos. Dichos Usuarios/as son los únicos responsables de los contenidos insertados en Auroraweb al mismo tiempo eximiendo de toda responsabilidad a D. Demetrio Cardador Habas (Auroraweb) por los datos, contenidos, información, etc. insertados y aportados por estos.
            <br>
            El Usuario/a garantiza la autenticidad que todos los datos que aporte y comunique o inserte en Auroraweb, personales o de terceras personas es exacta, veraz, verdadera, fiable, licita, así como su potestad para facilitarlos. Por lo que es el Usuario/a el único responsable de las inexactas manifestaciones o falsedad de los datos facilitados y de los perjuicios que pudiera ocasionar por ello a Auroraweb o terceras personas, con motivo de los servicios, por la información que faciliten o inserten. Es su exclusiva obligación y responsabilidad de mantener siempre y modificar en todo momento, sus datos personales o de terceras personas y tenerlos siempre al día de modo que estén permanentemente actualizados y responder con la máxima veracidad a la situación actual del Usuario/a o de terceras personas.



            <br><br><br><br><br>
            EL REGISTRO DEL USUARIO/A.

            <br>
            El registro de Usuario/a se efectúa de la forma expresamente indicada en el propio servicio.
            <br>
            El Usuario/a asume toda responsabilidad del uso de Auroraweb. Dicha responsabilidad se basa en la previa suscripción de registro de Usuario/a que fuese necesario para el acceso a determinados contenidos o servicios, en dichos registros el usuario será responsable de aportar toda la información veraz y licita. Como consecuencia de este registro, al Usuario/a se le designará un identificador ID o LOGIN y la contraseña de la que será responsable, comprometiéndose a conservarla y hacer un uso diligente y confidencial de la misma.
            <br>
            Los Usuarios/as de Auroraweb están obligados en todo momento a respetar las leyes aplicables y los derechos de terceros/as al utilizar los servicios y contenidos del Sitio.
            <br>
            El uso de la contraseña es personal e intransferible. No se permite el acceso ni siquiera temporal a terceras personas. El Usuario/a deberá adoptar todas las precauciones para la custodia de la contraseña por el seleccionada, evitando el uso de la misma por terceros. Por lo cual el Usuario/a es el único responsable de la utilización de su contraseña y de lo que con ella se realice, con total exclusión de responsabilidad para Auroraweb. Suponiendo que el Usuario/a sospeche o conozca del uso de su contraseña por terceras personas deberá a la mayor brevedad posible informar de tal circunstancia a Auroraweb.
            <br>
            Pertenece al Usuario/a de forma única y exclusivamente responsable de mantener la seguridad de su propia cuenta, y de su terminal electrónico o aparato debiendo notificar de forma urgente cualquier uso no autorizado, indebido o fallo de seguridad de su cuenta o de los servicios de la web.
            <br>
            Auroraweb no utilizamos spamming, por lo que informamos que únicamente tratamos los datos que los Usuarios/as nos mandan con los formularios, telefono o email.




            <br><br><br><br><br>
            El CONSENTIMIENTO DEL USUARIO/A.

            <br>
            Al pulsar clic para enviar los datos insertados en el formulario, el Usuario/a manifiesta haber leído y aceptado expresamente en toda su totalidad las Condiciones de Uso al igual que la Política de Privacidad de Auroraweb otorgando al mismo tiempo su consentimiento expreso e inequívoco al tratamiento de sus datos personales o de terceras personas insertadas por el Usuario/a conforme con todas las finalidades informadas y los servicios ofrecidos.
            <br>
            El Usuario/a consiente expresamente y acepta que en el momento que se produce el registro e introduce sus datos personales o de terceras personas y serán visibles públicamente en Internet.
            <br>
            Una vez se haya inhabilitado una cuenta por algún motivo tasado en estas condiciones, no será posible crear otro usuario sin un consentimiento previo por nuestra parte.



            <br><br><br><br><br>

            MEDIOS DE CONTRATACIÓN, SERVICIOS, PRECIOS E IMPUESTOS.
            <br><br>

            Los servicios ofrecidos por Auroraweb, junto con sus características y precios aparecerán en pantalla, si bien algunos servicios serán o pueden ser tratados de forma paralela ya que requieren de una gestión comercial. Dichos servicios estarán disponibles para su contratación, mientras aparezcan los mismos en este sitio web.
            <br>
            El acceso a Auroraweb por los Usuarios/as tiene carácter libre y gratuito. Algunos de los servicios o contenidos de Auroraweb pueden estar sujetos a contratación previa, ser un servicio de pago por su uso, en la forma que se determine en las Condiciones Generales de Contratación cuya disposición se notificará de forma clara al Usuario/a.
            <br>
            Los precios indicados serán en euros, impuestos no incluidos en el caso de la contratación.
            <br>
            Los servicios contratados se pagarán a través de tarjeta de crédito, débito, Master Card.
            <br>
            D. Demetrio Cardador Habas se reserva el derecho a decidir, en cada momento, que servicios se ofrecen a los Usuarios/as de forma gratuita o de pago notificando cualquier cambio a través del sitio web Auroraweb.
            <br>
            D. Demetrio Cardador Habas se reserva el derecho a decidir en cada momento, que servicios se ofrecen, al igual que puede modificar los ofertados en su momento. Asimismo, se reserva el derecho a introducir servicios nuevos. Todos los cambios serán informados a través de la página web Auroraservicios.es y están sometidos a lo establecido en las Condiciones Generales de esta página.




            <br><br><br><br><br>
            PROHIBICIONES GENERALES DE LOS USUARIOS/AS DE Auroraweb Y EXCLUSIÓN DE GARANTIAS Y RESPONSABILIDAD DE Auroraweb.

            <br><br>
            Los Usuarios/as se comprometen a hacer un uso adecuado de los contenidos y servicios de Auroraweb y usarlos solo con fines legales, autorizados y aceptables.
            <br>
            Los Usuarios/as tienen expresamente prohibido este listado que es en con carácter enunciativo pero no limitado:
            <br>
            - A utilizar contenidos injuriosos o calumniosos con la independencia de que esos contenidos afecten a otras personas Usuarios/as.
            <br>
            - A utilizar contenidos que atenten contra los derechos fundamentales de los familiares y de las personas fallecidas, que estén dentro de unas normas de cortesía, que sean molestos, que puedan generar falsas opiniones o negativas para terceras personas o para otros Usuarios/as y no reúnan los parámetros de calidad y buen uso establecidos por Auroraweb
            <br>
            - Está prohibido la inserción de contenidos que contravengan los principios de la buena fe, que se refieran a actividades ilícitas, que falten al orden público, ilegales por la normativa nacional Española, comunitaria e internacional. La utilización y distribución de propaganda con contenidos de carácter pornográfico o que vulneren las leyes de protección de menores, sean ilegales, obscenas, difamatorias, amenazantes, intimidantes, acosadoras, agresivas, ofensivas racialmente o étnicas, que promuevan o fomenten conductas que serían ilegales o inadecuadas, incluida la promoción de delitos violentos, material obsceno, o gestión de contenidos eróticos o que estén relacionados con el tarot, ciencias ocultas, videncia, la xenofobia, racismo, los principios de la legalidad, falsedades, declaraciones erróneas, afirmaciones engañosas, protección de la dignidad humana, la protección de la vida privada, la honradez, responsabilidad, el orden público, protección de los usuarios/as y consumidores, de apología del terrorismo, derechos de los animales.
            <br>
            - Intentar acceder a la cuenta de otro Usuario/a o utilizar las cuentas de los Usuarios/as haciendo modificaciones o manipulando los contenidos.
            <br>
            - Cobrar, alquilar, vender y revender nuestros servicios.
            <br>
            - Molestar a otros que impliquen el envío de comunicaciones ilegales o inadmisibles, como spam, mensajería masiva, mensajería automática, marcado automático y metodologías similares.
            <br>
            - Difundir o introducir virus informáticos en la red o sistema, cualquier otro sistema logístico o físico que sea susceptible de provocar cualquier tipo de daño a los sistemas logísticos, informáticos o físicos de Auroraweb.
            <br>
            - A utilizar contenidos protegidos legalmente por la legislación relativa y referente a la propiedad intelectual, a patentes y marcas sin tener derecho a ello, distribuir u ofrecer servicios de bienes protegidos legalmente, fomentar y realizar acciones contrarias a la libre competencia encaminadas a la captación de clientes tales como piramidales, en cadena o bola de nieve.
            <br>
            - Por distribuir los servicios de Auroraweb en Internet, lo que supondría el uso masivo de los servicios en varios dispositivos a la misma vez.
            <br>
            - Queda totalmente prohibido el acceso y la navegación en el este sitio web, y en concreto al registro en los formularios o en el área privada, por parte de los menores de catorce años (14) de edad.
            <br>
            En el supuesto que lo hicieran deberán obtener debidamente y con anterioridad, el consentimiento de sus padres, tutores o representantes legales, los cuales serán considerados como responsables de los actos que lleven a cabo los menores a su cargo.
            <br>
            - Reproducir o difundir públicamente cualquiera de los contenidos de los Usuarios/as o de Auroraweb sin autorización previa y por escrito.
            <br>
            - La utilización de los contenidos, informaciones o materiales con fines expresamente prohibidos e ilícitos en las Condiciones de Uso y Condiciones Particulares, que resulten contrarias a los intereses y derechos de Auroraweb debiendo responder frente a terceros y frente a sus miembros en el supuesto caso de incumplir dichas obligaciones o contravenirlas,
            <br>
            - La difusión o introducción de cualquier virus informático que supuestamente pudiera o impida la normal utilización, interrupción, sobrecargar, dañar, inutilizar los contenidos, archivos, documentos, sistemas de información y, sistemas informáticos, imágenes, material y toda clase de contenidos almacenados en cualquier equipo informático de cualquier Usuario/a y de Auroraweb.
            <br>
            - A utilizar sistemas informáticos/mecanismos, software o scripts en relación con la utilización de Auroraweb.
            <br>
            - Sobrescribir, copiar, modificar, bloquear,etc… los servicios de Auroraweb.
            <br>
            El Usuario/a que incumpla intencional o culpablemente cualquiera de las precedentes obligaciones responderá de todos los daños y perjuicios que cause.
            <br>
            El Usuario/a acepta voluntariamente y es consciente de que el uso de estos servicios tiene lugar bajo su única y exclusiva responsabilidad.
            <br>
            El Usuario/a responderá de todos y cada uno de los daños y perjuicios de toda naturaleza que Auroraweb puede o pudiera sufrir como consecuencia del mal uso e incumplimiento de cualesquiera de las obligaciones a las que queda sometido el Usuario/a por virtud de las presentes Condiciones de Uso, o de las leyes en relación con la forma y utilización de los servicios ofrecidos.
            <br>
            Auroraweb se reserva todos los derechos de retirar del sitio todos aquellos comentarios y aportaciones que vulneren el respeto y la dignidad de los Usuarios/as o terceras personas.
            <br>
            Al Usuario que se le retiren contenidos, comentarios, aportaciones, etc… que vulnere el respeto y la dignidad de las personas no tendrá derecho a reclamación alguna a Auroraweb.
            <br>
            Auroraweb no es responsable de las informaciones vertidas por los Usuarios/as en el sitio web.
            <br>
            Auroraweb no puede controlar todos y cada uno de los contenidos publicados por los Usuarios/as de manera que no puede asumir la responsabilidad total o parcial sobre los contenidos.
            <br>
            Periódicamente se revisarán los contenidos insertados para asegurar que se cumplan las normas de las Condiciones de uso y de calidad de Auroraweb.
            <br>
            Si algún usuario/a o tercera persona observa algún contenido inapropiado en Auroraweb por favor contacte con nosotros en la mayor brevedad posible.




            <br><br><br><br><br>
            ACTUALIZACIÓN DE NUESTRA POLÍTICA DE PRIVACIDAD Y LAS CONDICIONES GENERALES DE USO.

            <br><br>
            Auroraweb se reserva el derecho y la posibilidad de modificar, cambiar total o parcial sin previo aviso, los contenidos de la presente Política de Privacidad y las Condiciones Generales de Uso aquí descritas y adaptarlas a futuras regulaciones siendo debidamente publicadas en la web de Auroraweb. Al continuar el Usuario/a con nuestros servicios, confirma su aceptación a las nuevas condiciones de Uso y política de privacidad.
            <br>
            Si el Usuario/a no está de acuerdo con la nueva Política de Privacidad y Condiciones de Uso y con sus modificaciones, está en su total derecho de darse de baja para dejar de usar nuestros servicios. Al mismo tiempo que le recomendamos al Usuario/a que cada vez que acceda o utilice Auroraweb lea detenidamente dichas Condiciones y Política de privacidad.



            <br><br><br><br><br>

            MODIFICACIONES EN EL SITIO WEB y PRECIOS.

            <br><br>
            Auroraweb se reserva el derecho a efectuar sin previo aviso las modificaciones que considere oportunas en el sitio web, pudiendo suprimir, cambiar, cancelar o añadir tanto los contenidos y servicios que se presten a través de la web como de la forma en que estos aparecen localizados o presentados en el sitio Web.
            <br>
            Igualmente a modificar al alza o a la baja el precio de todos sus servicios sin previo aviso, sin que el Usuario/a tenga derecho a reclamación alguna.

            <br><br>
            Los Usuarios/as son los propietarios de sus datos o contenidos y lo importante que es preservar el acceso a los mismos, por lo que, en el supuesto que se interrumpa un servicio se procurará actuar con la máxima celeridad e informar a la mayor brevedad posible para que pueda recuperar, recoger, copiar, su información en dicho servicio.




            <br><br><br><br><br><br>
            LOS DERECHOS DE EXCLUSIÓN.
            <br>
            Auroraweb se reserva el derecho a retirar o denegar el acceso sin necesidad de previo aviso, a instancias de un tercero o propias, en los casos que se infrinjan las leyes o las condiciones general de uso y política de privacidad, sin derecho a reclamación alguna.


            <br><br><br><br><br><br>

            POLÍTICA DE PROTECCIÓN DE DATOS.
            <br>
            Partiendo del principio de la protección integral de la privacidad, constatar que la web Auroraweb cumple con todas las directrices establecidas por la Ley 15/1999, de 13 de diciembre LOPD y el Reglamento General de Protección de Datos.
            <br>
            D. Demetrio Cardador Habas cumple íntegramente con la legislación vigente en materia de protección de datos de carácter personal, y con los compromisos de confidencialidad propios de su actividad. Los datos que nos facilite el usuario serán incorporados a un fichero de datos personales. en este sentido Demetrio Cardador Habas ha adoptado los niveles de protección que legalmente se exigen, y ha instalado todas las medidas técnicas a su alcance para evitar la perdida, mal uso, alteración, acceso no autorizado por terceros. No obstante, el usuario debe ser consciente de que las medidas de seguridad en Internet no son inexpugnables. En caso en que considere oportuno que se cedan sus datos de carácter personal a otras entidades, el usuario será informado de los datos cedidos, de la finalidad del fichero y del nombre y dirección del cesionario, para que de su consentimiento inequívoco al respecto.

            <br><br>
            La presente Política de Privacidad y el resto de Condiciones Legales del sitio web se rigen en todos y cada uno de sus extremos por la ley española y Europea.
            <br>
            Informando también al Usuario/a de que damos cumplimiento a la ley 34/2002 del 11 de julio de los servicios de la sociedad de la información y el comercio electrónico y le solicita al Usuario/a su consentimiento que el Usuario/a acepta el tratamiento de su email o correo electrónico con fines únicamente informativos y comerciales por parte de Auroraweb o de colaboradores.
            <br>
            El Usuario/a puede ejercer sus derechos de acceso, oposición, cancelación o rectificación con la finalidad del tratamiento y las comunicaciones de los datos a terceros. Para ello debe de contactar con nosotros en info@auroraservicios.com
            <br>
            El motivo de los datos recopilados o recabados al registrarse el Usuario/a en los formularios de Auroraweb no son más que los descritos seguidamente.
            <br>
            Poder facilitar a los Usuarios/as y proveedores, colaboradores, etc…del Sitio un acceso a este, para poder controlar su espacio o sitio en Auroraweb hacer nuevas inserciones, modificaciones de imágenes, fotos, datos, logos, etc. para la confección, exposición en la web y él envió de esquelas a la App de Auroraweb por parte del Usuario/a o proveedores/colaboradores interesados, informando de las personas fallecidas y otros contenidos.
            <br>
            Al igual que para facilitarles la búsqueda de los datos o información relacionada con el fallecimiento.
            <br>
            Al registrarse como Usuario/a de Auroraweb como particulares o empresas se le solicitan los datos necesarios para dicha finalidad o de facilitar un sitio o espacio para las personas fallecidas donde se podrán publicar, insertar con sus datos, imágenes, información o contenidos de los difuntos como esquelas.
            <br>
            Al enviar a través de los formularios contenidos de terceras personas, datos personales, textos, imágenes etc. del fallecido, el usuario es consciente que todos sus datos personales facilitados puedan ser cedidos al familiar del fallecido que lo solicite o autoridades.
            <br>
            Hospedaje web
            <br>
            1&1 Internet España S.L.U.<br>
            Cif. B-85049435<br>
            <br>1&1 Internet España S.L.U., Sociedad Unipersonal, se encuentra inscrita en el Registro Mercantil de Madrid, Tomo: 24.232, Libro 0, Folio 73, Sección 8, Hoja M-435500, Inscripción 1.





            <br><br><br><br><br>
            ACCESO, RECTIFICACIÓN, BAJA SERVICIO.

            <br><br>
            Conforme a la normativa vigente hacemos todo lo necesario para evitar su alteración, pérdida, tratamiento o acceso no autorizado.
            <br>
            El Usuario/a tiene derecho al acceso, rectificación o la baja de los servicios de Auroraweb en este sitio web de forma sencilla, rápida y gratuita o en el supuesto de no poderlo hacer a través de la web se pondrá en contacto con nosotros con la referencia “Protección de Datos” Calle Gongora nº 9 local, Pozoblanco, Cordoba, España. o info@auroraservicios.com
            <br>
            Adjúntanos la fecha, nombre y apellidos, DNI fotocopia, o equivalente y firmado.
            <br>
            Los servicios de comunicaciones comerciales y el tratamiento de datos personales realizados a través de medios electrónicos, conformes a la Ley Organica 15-1999 13 diciembre de la protección de los datos de carácter personal, el Reglamento General de Protección de Datos y la ley 34-2002 11 julio de los servicios sociedad de la información – comercio electrónico.



            <br><br><br><br><br>

            PROPIEDAD INTELECTUAL E INDUSTRIAL.

            <br><br>
            Todos los derechos quedan reservados. No se pueden usar nuestros derechos de Autor.
            <br>
            D. Demetrio Cardador Habas es titular de todos y cada uno de los derechos de autor, de propiedad intelectual e industrial de este sitio de todos los demás derechos asociados a este sitio, también imágenes comerciales, fotos, textos, las estructuras y los diseños, logos, marcas o secretos comerciales, vídeos, textos, audio, software, sonidos, bases de datos, anagramas, colores y combinaciones, archivos, el acceso y uso, patentes, etc. estando protegidos por los tratados y leyes internacionales e industriales.
            <br>
            En virtud de los dispuesto en los artículos 8 y 32.1. Párrafo segundo de la ley de protección intelectual está estrictamente prohibida la explotación o parte del contenido de Auroraweb, cualquier reproducción, transmisión, adaptación, traducción, modificación, comunicación al público, o cualquier otra explotación de todo o parte del contenido de este sitio propiedad de D.Demetrio Cardador Habas, efectuada de cualquier forma o por cualquier medio, electrónico, mecánico u otros, están estrictamente prohibidos salvo autorización previa y por escrito de D. Demetrio Cardador Habas o terceros titulares. Cualquier infracción de estos derechos puede dar lugar a procedimientos extrajudiciales o judiciales civiles y/o penales que correspondan.
            <br>
            El Usuario/a esta obligado y se compromete a respetar cada uno de los derechos de la propiedad industrial e intelectual de Auroraweb. El Usuario/a esta obligado y se abstendrá de manipular cualquier dispositivo, programa, eludir, suprimir, alterar, el sistema de seguridad o protección que estuviera instalado en el sitio web Auroraweb.
            <br>
            Los derechos y la legitimidad del uso de los contenidos aportados por los Usuarios/as es de su única exclusividad y responsabilidad.
            <br>
            En un posible caso o supuesto si un Usuario/a o tercera persona de Auroraweb considera que se ha producido una intromisión y/o invasión de un contenido por la violación de sus derechos en el sitio web, informara a la mayor brevedad a Auroraweb por escrito a la dirección física en la parte superior indicada.
            <br>
            Se informará de los contenidos protegidos por los derechos de la propiedad intelectual y el sitio y/o ubicación en Auroraweb, al igual que los datos personales o de terceros interesados y de los derechos presuntamente infringidos y violados. Al presentar la reclamación una tercera persona deberá presentar la representación a nombre del Usuario/a al que representa. Al mismo tiempo se presentará la titularidad o acreditación, en vigencia de los derechos de la propiedad intelectual. Se deberá adjuntar una declaración en la que el interesado y/o Usuario/a facilita la información exacta, cierta y veraz de los datos enviados a los que se refiere.



            <br><br><br><br><br>

            USO DE DATOS, SOFTWARE, DESCARGAS.
            <br><br>

            El Usuario/a proporcionara su propia conexión de tarifa de datos para la conexión a Internet, su software, ya que dichos servicios no se proporcionan por Auroraweb.
            <br>
            También acepta la descarga y la instalación de actualizaciones de nuestros servicios incluso de forma automática, hasta dejar de usar nuestros servicios.
            <br><br><br><br><br>




            ENLACES, HIPERVÍNCULOS O LINKS.

            <br><br>
            Los Usuarios/as de Auroaweb que hayan insertado en este sitio web por su cuenta enlaces, hipervínculos o links de otros sitios web son los únicos responsables relevando a Auroraweb de toda responsabilidad.
            <br>
            Auroraweb puede incluir links, hipervínculos o enlaces de otros sitios no perteneciendo a esta web y gestionado por terceros con el fin de facilitar un acceso a una información posiblemente interesante para el Usuario/a de este sitio Web.
            <br>
            En ningún caso Auroraweb asume responsabilidad alguna de los enlaces pertenecientes a un sitio externo o ajeno, ni garantizamos su calidad, veracidad, validez, constitucionalidad, exactitud, fiabilidad, amplitud, calidad, o disponibilidad técnica. Igualmente Auroraweb tampoco se hace responsable del contenido de dichos enlaces o links.
            <br>
            Queda terminantemente prohibido insertar enlaces a páginas web de carácter discriminatorio, pornográfico, de enaltecimiento del terrorismo, mafias, sexistas, racistas, xenófobos, pornografía infantil, que atenten contra la seguridad pública, las malas prácticas, y la legalidad nacional e internacional.
            <br><br>

            DURACIÓN Y FINALISACIÓN DEL SERVICIO.
            <br>
            La prestación del servicio tiene una duración indefinida.
            <br>
            Así mismo, sin ningún perjuicio de lo anterior, esta facultada para dar por suspendida, terminada, interrumpir unilateral mente, en cualquier instante o momento sin ningún tipo de necesidad de previo aviso o notificación la prestación de los servicios que en Auroraweb se ofrecen a los usuarios y empresas.
            <br>
            En el supuesto que la empresa tenga esquelas pendientes de utilización o creación se le devolverá el dinero de estas descontando los gastos si los hubiera, sin ningún tipo de reclamación por parte de la empresa que utilice Auroraweb.




            LA LEGISLACIÓN APLICABLE Y LA JURISDICCIÓN.
            <br><br>

            Las presentes Condiciones Legales y el resto de condiciones del sitio web se regirán por la normativa vigente de la legislación española.
            <br>
            Auroraweb se reserva el derecho a ejercer cuantas acciones estén disponibles en derecho para exigir las responsabilidades que se deriven de cualquier incumplimiento así como cualquier utilización indebida de Auroraweb o de las disposiciones de estas Condiciones Generales de Uso del sitio web por parte de un Usuario/a ejerciendo todas las acciones civiles y penales que puedan corresponder en nuestro derecho.
            <br>
            En el supuesto que se produzca cualquier conflicto o discrepancia entre las partes en relación al contenido, interpretación, etc…, que no haya sido resuelto de mutuo acuerdo las partes se someterán a los tribunales de Pozoblanco, Cordoba, España, renunciando de forma expresa a cualquier otro fuero que pudiera corresponderles.
            <br>
            Las partes de mutuo acuerdo pueden decidir acudir a un arbitraje (en Pozoblanco, Cordoba) para intentar solucionar cualquier conflicto y, se obligan al cumplimiento de la decisión arbitral si se decantan por esta opción.
            <br>
            <br>

        </div>
    </section>
    <section class="row m-5-arriba">
        <div class="col-md-12 Borde_difuminado">
            <p><h5>POLITICA DE COOKIES</h5>

                Cookie es un fichero que se descarga en su ordenador al acceder a determinadas páginas web. Las cookies permiten a una página web, entre otras cosas, almacenar y recuperar información sobre los hábitos de navegación de un usuario o de su equipo y, dependiendo de la información que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer al usuario.. El navegador del usuario memoriza cookies en el disco duro solamente durante la sesión actual ocupando un espacio de memoria mínimo y no perjudicando al ordenador. Las cookies no contienen ninguna clase de información personal específica, y la mayoría de las mismas se borran del disco duro al finalizar la sesión de navegador (las denominadas cookies de sesión).
            <br><br>
                La mayoría de los navegadores aceptan como estándar a las cookies y, con independencia de las mismas, permiten o impiden en los ajustes de seguridad las cookies temporales o memorizadas.
            <br><br>
                Sin su expreso consentimiento –mediante la activación de las cookies en su navegador–auroraservicios no enlazará en las cookies los datos memorizados con sus datos personales proporcionados en el momento del registro o la compra..
            <br><br>
                ¿Qué tipos de cookies utiliza esta página web?
            <br><br>
                - Cookies técnicas: Son aquéllas que permiten al usuario la navegación a través de una página web, plataforma o aplicación y la utilización de las diferentes opciones o servicios que en ella existan como, por ejemplo, controlar el tráfico y la comunicación de datos, identificar la sesión, acceder a partes de acceso restringido, recordar los elementos que integran un pedido, realizar el proceso de compra de un pedido, realizar la solicitud de inscripción o participación en un evento, utilizar elementos de seguridad durante la navegación, almacenar contenidos para la difusión de videos o sonido o compartir contenidos a través de redes sociales.
            <br><br>
                - Cookies de personalización: Son aquéllas que permiten al usuario acceder al servicio con algunas características de carácter general predefinidas en función de una serie de criterios en el terminal del usuario como por ejemplo serian el idioma, el tipo de navegador a través del cual accede al servicio, la configuración regional desde donde accede al servicio, etc.
            <br><br>
                - Cookies de análisis: Son aquéllas que bien tratadas por nosotros o por terceros, nos permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del servicio ofertado. Para ello se analiza su navegación en nuestra página web con el fin de mejorar la oferta de productos o servicios que le ofrecemos.
            <br><br>
                - Cookies publicitarias: Son aquéllas que, bien tratadas por nosotros o por terceros, nos permiten gestionar de la forma más eficaz posible la oferta de los espacios publicitarios que hay en la página web, adecuando el contenido del anuncio al contenido del servicio solicitado o al uso que realice de nuestra página web. Para ello podemos analizar sus hábitos de navegación en Internet y podemos mostrarle publicidad relacionada con su perfil de navegación.
            <br><br>
                - Cookies de publicidad comportamental: Son aquéllas que permiten la gestión, de la forma más eficaz posible, de los espacios publicitarios que, en su caso, el editor haya incluido en una página web, aplicación o plataforma desde la que presta el servicio solicitado. Estas cookies almacenan información del comportamiento de los usuarios obtenida a través de la observación continuada de sus hábitos de navegación, lo que permite desarrollar un perfil específico para mostrar publicidad en función del mismo.
            <br><br>
                Cookies de terceros: La Web de auroraservicios puede utilizar servicios de terceros que, por cuenta de auroraservicios, recopilaran información con fines estadísticos, de uso del Site por parte del usuario y para la prestacion de otros servicios relacionados con la actividad del Website y otros servicios de Internet.
            <br><br>
                En particular, este sitio Web utiliza Google Analytics, un servicio analítico de web prestado por Google, Inc. con domicilio en los Estados Unidos con sede central en 1600 Amphitheatre Parkway, Mountain View, California 94043.  Para la prestación de estos servicios, estos utilizan cookies que recopilan la información, incluida la dirección IP del usuario, que será transmitida, tratada y almacenada por Google en los términos fijados en la Web Google.com. Incluyendo la posible transmisión de dicha información a terceros por razones de exigencia legal o cuando dichos terceros procesen la información por cuenta de Google.
            <br><br>
                El Usuario acepta expresamente, por la utilización de este Site, el tratamiento de la información recabada en la forma y con los fines anteriormente mencionados. Y asimismo reconoce conocer la posibilidad de rechazar el tratamiento de tales datos o información rechazando el uso de Cookies mediante la selección de la configuración apropiada a tal fin en su navegador. Si bien esta opción de bloqueo de Cookies en su navegador puede no permitirle el uso pleno de todas las funcionalidades del Website.
            <br><br>
                Puede usted permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones del navegador instalado en su ordenador:
            <br><br>
            <ul style="padding-left: 30px;">
                <li><em><a href="http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647" target="_blank">Chrome</a></em></li>
                <li><em><a href="http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9" target="_blank">Explorer</a></em></li>
                <li><em><a href="http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we" target="_blank">Firefox</a></em></li>
                <li><em><a href="http://support.apple.com/kb/ph5042" target="_blank">Safari</a></em></li>
            </ul>
                Si tiene dudas sobre esta política de cookies, puede contactar con auroraservicios en info@auroraservicios.es</p>
        </div>
    </section>
</div>

</body>

</html>

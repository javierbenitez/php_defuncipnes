<?php
session_start();
if (!isset($_SESSION['usuario']) && !isset($_SESSION['rango'])) {
    header("Location: index.php");
}

$usuario=$_SESSION['usuario'];
$userId = $_SESSION['usuario_id'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

include 'librerias/Encriptador.php';
$pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

include_once "librerias/DataInvoice.php";
include_once "modelo/User.php";

$repoInvoice = new DataInvoice();
$user = new User();
$user->setRango($rango);
$user->setId($userId);

if (isset($_GET['sudates']) && isset($_GET['since_invoice'])
    && strlen(trim($_GET['since_invoice'])) > 9&& strlen(trim($_GET['until_invoice'])) > 9){
    $dateStart = DateTime::createFromFormat('d-m-Y',  trim($_GET['since_invoice']));
    $dateEnd = DateTime::createFromFormat('d-m-Y',  trim($_GET['until_invoice']));

    $Lista = $repoInvoice->getAllPublications("death", $user, $dateStart->format('Y-m-d'), $dateEnd->format('Y-m-d'));
}else{
    $Lista = $repoInvoice->getAllPublications("death", $user);
}

?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>
<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container m-5-arriba">
    <section class="row">
        <header class="sheader">
            <h2>Tus publicaciones de defunciones</h2>
        </header>
        <div class="panel panel-default sheader">
            <div class="panel-body">
                <form autocomplete="off" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" class="form-inline form-dates">
                    <div class="form-group">
                        <label for="email">Desde: </label>
                        <input autocomplete="off" type="text" name="since_invoice" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Hasta: </label>
                        <input autocomplete="off" type="text" name="until_invoice" class="form-control">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="sudates" class="btn btn-info"> Buscar</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-12">
            <table id="Tabla_contenido" class="table">
                <thead>
                    <tr class="header thead-dark text-center">
                        <th>Usuario</th>
                        <th>Apellidos, Nombre</th>
                        <th>Teléfono</th>
                        <th>Fecha</th>
                        <th>CP 1</th>
                        <th>Precio</th>
                        <th>Ver publicacion</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach ($Lista as $fila){
                    echo "<tr>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila["username"];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila["last_name"].", ".$fila["name"];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila[4];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila["created"];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo $fila["cp_1"];
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            if($fila["price"] == "" || $fila["price"] == null) echo "0"; else echo $fila["price"];
                            echo " €";
                        echo "</td>";
                        echo "<td class='align-middle text-center'>";
                            echo "<a href=\"ver_publicacion.php?de=1&code=$fila[code]&user=$fila[username]\" target='_blank' class=\"btn btn-secondary\" role=\"button\" aria-pressed=\"true\">Ver publicacion</a>";
                        echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
</div>
<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script>
    $.datepicker.setDefaults($.datepicker.regional['es']);
    (function () {
        var dateStart, dateEnd;

        function initVars(){
            dateStart = $("input[name=since_invoice]");
            dateEnd = $("input[name=until_invoice]");
        }

        initVars();

        dateStart.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            endDate: "today",
            maxDate: "today",
            language: 'es'
        });

        dateEnd.datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            endDate: "today",
            maxDate: "today",
            language: 'es'
        });

        <?php
        if(isset($_GET['since_invoice'])){ ?>
        dateStart.val('<?php echo trim($_GET['since_invoice']); ?>');
        <?php }
        if(isset($_GET['until_invoice'])){ ?>
        dateEnd.val('<?php echo trim($_GET['until_invoice']); ?>');
        <?php } ?>
    })();
</script>
</body>
</html>
<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin"){
    header("Location:index.php");
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

?>

<html xmlns="http://www.w3.org/1999/html">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <?php include "css/basic_style.php"; ?>
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
</head>

<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container">
        <section class="row justify-content-center">
            <article>
                <form  class="form-horizontal m-5-arriba" id="formulario" name="formulario" action="insertar_publicidad.php" enctype="multipart/form-data" method="POST">
                <?php
                if(isset($_GET['error'])){?>
                    <div class="alert alert-danger">
                        <p><?php echo $_GET['error']; ?></p>
                    </div>
                    <?php
                }
                ?>
                <div class="row">
                    <div class="col-md-4" >
                        <input required name ="codigo_postal"
                            <?php if(isset($_SESSION['cp1'])) echo 'value="'.$_SESSION['cp1'].'" '; ?>
                               type="number" class="form-control" id="codigo_postal" aria-describedby="" placeholder="Codigo Postal">
                    </div>
                    <div class="col-md-4" >
                        <input required name ="nombre_publi"
                            <?php if(isset($_SESSION['nombre_publi'])) echo 'value="'.$_SESSION['nombre_publi'].'" '; ?>
                               type="text" class="form-control" id="nombre_publi" aria-describedby="" placeholder="Nombre">
                    </div>
                    <div class="col-md-4" >
                        <label for="imagen" class="m-0">
                            Imagen de publicidad  <input required type="file" name="imagen" id="imagen">
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3" >
                        <input name ="cp2"
                            <?php if(isset($_SESSION['cp2'])) echo 'value="'.$_SESSION['cp2'].'" '; ?>
                               type="number" class="form-control" id="cp2" aria-describedby="" placeholder="Otro codigo postal (opcional)">
                    </div>
                    <div class="col-md-3" >
                        <input name ="cp3"
                            <?php if(isset($_SESSION['cp3'])) echo 'value="'.$_SESSION['cp3'].'" '; ?>
                               type="text" class="form-control" id="cp3" aria-describedby="" placeholder="Otro codigo postal (opcional)">
                    </div>
                    <div class="col-md-3" >
                        <input name ="url" type="text" class="form-control" id="url" aria-describedby="" placeholder="https://www....es/">
                    </div>
                    <div class="col-md-3 checkbox" >
                        <label for="premium">
                            <input type="checkbox"
                                <?php if(isset($_SESSION['cp1']) && $_SESSION['premium'] == 1) echo 'checked="checked" '; ?>
                                   name="premium" id="premium">
                            Premium
                        </label>
                    </div>
                </div>
                <button name="boton" type="submit" class="btn btn-primary align-self-center">Enviar</button>
            </form>
            </article>
        </section>
    <div class="row box-image">
        <div class="col-sm-5">
            <img src="imagenes/titulo_2.jpg" alt="Logo Aurora Servicios">
        </div>
        <div class="col-sm-7">
            <p class="alert alert-warning">Para que se vea bien la publicidad en los dispositivos móviles es recomendable subir imágenes con
             la relación de aspecto 16:3 (similar a la imagen de ejemplo).</p>
        </div>
    </div>
</div>

<?php include "includes/footer.php"; ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
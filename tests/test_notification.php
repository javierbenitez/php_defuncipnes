<?php

require("../controller/Conexion.php");
$logeo = new Logeo();
$devices = [];

$result['estado'] = 1; // No request method POST

if($_SERVER['REQUEST_METHOD'] == 'POST') {

    $logeo = new Logeo();
    $conexion = $logeo->conectarBD();

    $title = "Prueba dev notificacion";
    $body = "Body notificacion";

    $body = json_decode(file_get_contents("php://input"), true);

    if ($body != null) {// Android
        if ($body['typeOfDevice'] == 2) {
            $SO = "Android";
        } else {
            $SO = $body['typeOfDevice'];
        }
        $tokenDevice = $body['tokenDevice']; //device Token para las notificaciones
    } else {//iOS
        if ($_POST['typeOfDevice'] == 1) {
            $SO = "iOS";
        } else {
            $SO = $_POST['typeOfDevice'];
        }
        $tokenDevice = $_POST['tokenDevice']; //device Token para las notificaciones
    }

    $logeo->sendNotificationPushTest($tokenDevice, $title, $body, $SO);
}

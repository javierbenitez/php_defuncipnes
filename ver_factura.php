<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

$ref = trim($_GET["ref"]);
//$user = trim($_GET["user"]);

if(isset($_GET['de'])){
    $item = "death";
}else{
    $item = "ceremony";
}

include_once "librerias/DataInvoice.php";
include_once "controller/InvoiceController.php";
$repoInvoice = new DataInvoice();
$controller = new InvoiceController();

$result = $repoInvoice->getInvoiceByReference($item, $ref);

$dataCompany = $result[0];

$url = $_SERVER['PHP_SELF']."?ref=$dataCompany[referencia]";
if($item == "death"){
    $url .= "&de=1";
}

if(isset($_POST['ref']) && isset($_POST['estado'])){
    $invo = $_POST["ref"];
    $status = $_POST["estado"];
    $controller->updateStatusInvoice($invo, $status, $item, $user);
}

if(isset($_GET['deshacer']) && $_GET['deshacer'] == 1){
    $repoInvoice->delInvoice($dataCompany["referencia"]);
    if($item === 'death'){
        header("Location: listar_facturas.php");
    }else{
        header("Location: listar_facturas_misas.php");
    }
}

if(isset($_GET['generate']) && $_GET['generate'] == 1){
    $options = [];
    $b = true;
    $options['aurora'] = $repoInvoice->getDataAurora();
    $options['company'] = $dataCompany;
    $publications = explode(",", $_POST['invoices_mark']);
    $aux = explode("__", $publications[0])[1];

    if(strlen($_POST['invoices_mark']) > 1) {
        for ($i = 1; $i < count($publications); $i++) {
            $arrPublication = explode("__", $publications[$i]);
            if ($aux != $arrPublication[1]) {
                $message = "No puedes facturar publicaciones de distintos usuarios";
                header("Location: $_SERVER[PHP_SERVER]?m=$message");
                break;
            }
        }
        $options['productos'] = [];
        for ($i = 0; $i < count($publications); $i++){
            $arrPublication = explode("__", $publications[$i]);
            $result = $repoInvoice->getPublicationByCode($item, $arrPublication[0]);
            $options['number_invoice'] = $dataCompany["referencia"];
            $options['productos'][$i]['producto'] = $item === 'death' ? 'Publicación de defunción' : 'Publicación de misa';
            $options['productos'][$i]['cantidad'] = 1;
            $options['productos'][$i]['precio'] = $result[0]['price'];
        }
        $controller->generarFactura($options);
    }else{
        $message = "Debes seleccionar alguna publicación";
        header("Location: $_SERVER[PHP_SERVER]?m=$message");
    }
}

include 'librerias/Encriptador.php';

$hash = "qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

if($result[0]['user_id'] != null){
    include_once "librerias/DataCompany.php";
    $repoCompany = new DataCompany();
    $company = $repoCompany->getCompany($result[0]['user_id']);

    $result[0]['name_company'] = $company['name_company'];
    $result[0]['address'] = $company['address'];
    $result[0]['phone1'] = $company['phone1'];
    $result[0]['nif_company'] = $company['nif_company'];
}

$allStatus = $repoInvoice->getAllStatus();
?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>

<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container">
    <div class="panel" id="panel-status">
        <?php
        if($rango == "admin"){
            ?>
            <div>
                <form action="<?php echo $url; ?>" id="form-change-status" method="post">
                    <input type="hidden" value="<?php echo $dataCompany["referencia"]."__".$result[0]['invoice_id']; ?>" name="ref">
                    <select name="estado" id="estado" class="form-control">
                        <?php
                        if($result['status_invoice'] == null){
                            echo '<option value="-" >Sin acción</option>';
                        }
                        foreach ($allStatus as $status){
                            echo '<option value="'.$status['status'].'"  title="'.$status['description'].'" ';
                            if($status['status'] == $dataCompany['status']) echo ' selected ';
                            echo '>'.$status['status'].'</option>';
                        }
                        ?>
                    </select>
                </form>
            </div>
        <?php } ?>
    </div>
    <form action="<?php echo $url; ?>" method="post">
        <section class="row">
            <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba text-center"  >
                <div class="row ustify-content-center">
                    <div class="col-md-12 text-center" >
                        <h2>Factura Nº: <?php echo $dataCompany["referencia"]; ?></h2>
                        <input type="hidden" value="<?php echo $dataCompany["referencia"]; ?>" name="ref">
                    </div>
                </div>
                <div class="row m-5-arriba">
                    <?php
                    if($rango != null){ ?>
                        <div class="col-md-4 ">
                            <p><b for="estado">Estado: </b><span class="text-danger"><?php echo $dataCompany['status']  ?></span></p>
                        </div>
                    <?php } ?>
                </div>
                <div class="row m-5-arriba">
                    <div class="col-md-3 col-xs-12" >
                        <h5>Empresa: </h5><span><?php echo $dataCompany['company'];?></span>
                        <input type="hidden" value="<?php echo $dataCompany["company"]; ?>" name="company">
                    </div>
                    <div class="col-md-3 col-xs-12" >
                        <h5>Dirección: </h5><span><?php echo $dataCompany['address'];?></span>
                        <input type="hidden" value="<?php echo $dataCompany["address"]; ?>" name="address">
                    </div>
                    <div class="col-md-3 col-xs-12" >
                        <h5>Teléfono empresa: </h5><span><?php echo $dataCompany['phone'];?></span>
                        <input type="hidden" value="<?php echo $dataCompany["phone"]; ?>" name="phone">
                    </div>
                    <div class="col-md-3 col-xs-12" >
                        <h5>NIF: </h5><span><?php echo $dataCompany['nif'] ;?></span>
                        <input type="hidden" value="<?php echo $dataCompany['nif']; ?>" name="nif">
                    </div>
                </div>
                <div class="row m-5-arriba">
                    <div class="col-md-3" >
                        <h5>Gerente: </h5><span><?php echo $dataCompany["company_name_contact"] ?></span>
                        <input type="hidden" value="<?php echo $dataCompany["company_name_contact"]; ?>" name="company_name_contact">
                    </div>
                    <div class="col-md-3" >
                        <h5>Cuenta: </h5><span><?php echo Encriptador::desencriptar($dataCompany["account"], $hash) ;?></span>
                        <input type="hidden" value="<?php echo Encriptador::desencriptar($dataCompany["account"], $hash); ?>" name="account">
                    </div>
                    <div class="col-md-3" >
                        <h5>Email: </h5><span><?php echo $dataCompany["company_email"] ;?></span>
                        <input type="hidden" value="<?php echo $dataCompany["company_email"]; ?>" name="company_email">
                    </div>
                    <div class="col-md-3" >
                        <h5>Fecha: </h5><span><?php echo $dataCompany["company_created"]; ?></span>
                        <input type="hidden" value="<?php echo $dataCompany["company_created"]; ?>" name="fecha">
                    </div>
                </div>
                <input type="hidden" value="<?php if($item === "death") echo "Publicación de defunción"; else echo "Misa"; ?>" name="producto">
            </div>
            <div class="row div-buttons">
                <div class="col-md-4">
                    <a href="" class="btn btn-danger" >Atrás</a>
                </div>
                <?php if($rango == "admin"){  ?>
                <div class="col-md-4">
                    <input type="button" class="btn btn-success" id="sugenerate" name="sugenerate" value="Generar">
                </div>
                <div class="col-md-4">
                    <input type="hidden" name="des_invoice" id="des_invoice" value="0" >
                    <input type="submit" class="btn btn-success" id="deshacer_invoice" name="deshacer_invoice" value="Deshacer factura">
                </div>
                <?php } ?>
            </div>
            <input type="hidden" name="invoices_mark" id="invoices_mark" value="0">
        </section>
    <table id="Tabla_contenido" class="table">
        <thead class="header thead-dark text-center">
            <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Teléfono</th>
                <th>CP 1</th>
                <th>Fecha</th>
                <th>Publicación</th>
            </tr>
        </thead>
        <tbody>
        <?php
        foreach ($result as $item){
          echo "<tr>";
                echo "<td class='align-middle text-center'>";
                    echo $item["code"];
                    echo "<input type='checkbox' class='hidden' name='invoice_mark[]' checked=true class='mark' value='".$item["code"]."__".$item["company_id"]."' data_company='".$item["company_id"]."' />";
                echo "</td>";
                echo "<td class='align-middle text-center'>";
                    echo $item["name"];
                echo "</td>";
                echo "<td class='align-middle text-center'>";
                    echo $item["last_name"];
                echo "</td>";
                echo "<td class='align-middle text-center'>";
                    echo $item["phone"];
                echo "</td>";
                echo "<td class='align-middle text-center'>";
                    echo $item["cp_1"];
                echo "</td>";
                echo "<td class='align-middle text-center'>";
                    echo $item["created"];
                echo "</td>";
                echo "<td class='align-middle text-center'>";
                    echo "<a href=\"ver_publicacion.php?de=1&code=$item[code]&user=$item[username]\" target='_blank' class=\"btn btn-secondary\" role=\"button\" aria-pressed=\"true\">Ver publicacion</a>";
                echo "</td>";
            echo "</tr>";
            }
        ?>
        </tbody>
    </table>
    </form>
</div>
<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script>
    (function () {
        var selectStatus, form, deshacerFactura, btnGenerateInvoice;

        function initVars(){
            selectStatus = $('select[name=estado]');
            deshacerFactura = $('#deshacer_invoice');
            form = $('#form-change-status');
            btnGenerateInvoice = $("#sugenerate");
        }

        initVars();

        selectStatus.on('change', function (e) {

            Swal.fire({
                title: '¿Estás seguro que quieres cambiar el estado de la factura?',
                text: "No debes cambiarlo a menos que estés seguro de lo que estás haciendo.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, cambiarlo'
            }).then((result) => {
                if (result.value) {
                    form.submit();
                }
            })
        });

        btnGenerateInvoice.on('click', function (e) {
            e.preventDefault();

            var arr = $('[name="invoice_mark[]"]:checked').map(function () {
                return this.value;
            }).get();

            var form = $(this).parent('div').parent('div').parent("section").parent('form');
            var action = form.attr("action");
            action = action+"&generate=1";
            form.attr("action", action);
            $(this).value = "Generar";
            $(this).val("Generar");
            var inputCheck = $('#invoices_mark');
            inputCheck.val(arr);
            form.submit();
        });

        deshacerFactura.on('click', function (e) {
            e.preventDefault();
            var form = $(this).parent('div').parent('div').parent("section").parent('form');
            var action = form.attr("action");
            action = action+"&deshacer=1";
            form.attr("action", action);
            Swal.fire({
                title: '¿Estás seguro que quieres deshacer la factura?',
                text: "No debes cambiarlo a menos que estés seguro de lo que estás haciendo.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, deshacer'
            }).then((result) => {
                if (result.value) {
                    form.submit();
                }
            });
        })
    })();
</script>
</body>
</html>
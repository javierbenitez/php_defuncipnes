<?php
session_start();
if (!isset($_SESSION['usuario']) && !isset($_SESSION['rango'])) {
    header("Location: index.php");
}

if (!isset($_GET["code"]) || !isset($_GET['user'])){
    header("Location:index.php");
}

$code = trim($_GET["code"]);
$user = trim($_GET["user"]);

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];

include 'librerias/Encriptador.php';
$pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

include_once "librerias/DataInvoice.php";

if(isset($_GET['de']) && $_GET['de'] == 1){
    $item = "death";
}else{
    $item = "ceremony";
}
$repoInvoice = new DataInvoice();

if(isset($_POST['inovice_submit'])){

    $code = $_POST["code"];
    $status = "Emitida";
    $controller->updateStatusInvoice($code, $status, $item, null, true);
    $options = [];
    $options['aurora'] = $repoInvoice->getDataAurora();

    $price = 0;
    $options['invoice'] = $_POST['ref'];
    $options['nombre'] = $_POST['apellidos'];
    $options['apellidos'] = $_POST['nombre'];
    $date = new DateTime($_POST['fecha']);
    $options['fecha'] = $date->format("d-m-Y");
    $options['telefono'] = $_POST['telefono'];
    $options['nif'] = $_POST['nif'];
    $options['usuario'] = $_POST['usuario'];
    $options['codigo'] = $code;
    $options['empresa'] = $_POST['empresa'];
    $options['productos'] = [];
    $producto = [];
    if($_POST['precio'] != null && $_POST['precio'] != ""){
        $price = trim($_POST['precio']);
    }
    $producto['precio'] = $price;
    $producto['producto'] = $_POST['producto'];
    $producto['cantidad'] = $_POST['cantidad'];
    $options['productos'][0] = $producto;

    $controller->generarFactura($options);
}

$result = $repoInvoice->getPublication($item, $user, $code);

if(!isset($_SESSION['publicacion'])){
    $_SESSION['publicacion'] = null;
}

$_SESSION['publicacion'] = $result;

if($result == null)
{
    header("Location: error404.php");
}

?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title>
    <link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>

<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container">

    <section class="row">
        <div class="col-sm-12 p-5 Borde_difuminado m-5-arriba text-center"  >
            <div class="row ustify-content-center">
                <div class="col-md-12 text-center" >
                    <h2>Publicación <?php
                        if(isset($result['foto'])){
                            ?>(defunción) <?php }else{?>(misa)<?php } ?></h2>
                </div>
            </div>
            <div class="alert alert-info">
                <header>
                    <h3>Datos contacto publicación</h3>
                </header>
                <div class="row">
                    <div class="col-sm-4"><?php echo $result['name_contact'] ?></div>
                    <div class="col-sm-4"><?php echo $result['last_name_contact'] ?></div>
                    <div class="col-sm-4"><?php echo $result['phone_contact'] ?></div>
                </div>
            </div>
            <?php
            if(isset($result['foto'])){
            ?>
            <section class="row justify-content-center m-5-arriba">
                <div class="col-md-4 text-center">
                    <h3>Imagen del tanatorio</h3>
                    <img style="width: 100%" src="<?php echo $result['foto']; ?>">
                </div>
                <div class="col-md-4 text-center">
                    <h3>Imagen del fallecido</h3>
                    <img style="width: 100%" src="<?php echo $result['foto2'];?>">
                </div>
            </section>
            <?php } ?>
            <div class="row m-5-arriba">
                <div class="col-md-4" >
                    <h5>Nombre: </h5><span><?php echo $result['nombre'];?></span>
                </div>
                <div class="col-md-4" >
                    <h5>Apellidos: </h5><span><?php echo $result['apellidos'] ;?></span>
                </div>
                <div class="col-md-4" >
                    <h5>Apodo: </h5><span><?php echo $result['apodo'];?></span>
                </div>
            </div>
            <?php
            if(isset($result['direccion'])){
            ?>
            <div class="row m-5-arriba">
                <div class="col-md-6" >
                    <h5>Dirección: </h5><span><?php echo $result['direccion'];?></span>
                </div>
                <div class="col-md-6" >
                    <h5>Localidad: </h5><span><?php echo $result['localidad'];?></span>
                </div>
            </div>
            <?php } ?>
            <div class="row m-5-arriba">
                <div class="col-md-4" >
                    <h5>Fecha fallecimiento: </h5><span><?php echo $result['fecha_fallecimiento'];?></span>
                </div>
                <div class="col-md-4" >
                    <h5>Edad: </h5><span><?php echo $result['edad'];?></span>
                </div>
                <div class="col-md-4" >
                    <h5>Servicio: </h5><span><?php if ($result['premium'] != 0){echo "Premium";}else{echo "Normal";}?></span>
                </div>
            </div>
            <div class="row m-5-arriba">
                <div class="col-md-6" >
                    <h5>Cónyuge: </h5><span><?php echo $result['conyuge'];?></span>
                </div>
                <div class="col-md-6" >
                    <h5>Hijos: </h5><span><?php echo $result['hijos'];?></span>
                </div>
            </div>
            <div class="row m-5-arriba">
                <div class="col-md-6" >
                    <h5>Padres: </h5><span><?php echo $result['padres'];?></span>
                </div>
                <div class="col-md-6" >
                    <h5>Hijos politicos: </h5><span><?php echo $result['hijos_politicos'];?></span>
                </div>
            </div>
            <div class="row m-5-arriba">
                <div class="col-md-6" >
                    <h5>Hermanos: </h5><span><?php echo $result['hermanos'];?></span>
                </div>
                <div class="col-md-6" >
                    <h5>Hermanos politicos: </h5><span><?php echo $result['hermanos_politicos'];?></span>
                </div>
            </div>
            <div class="row m-5-arriba">
                <div class="col-md-6" >
                    <h5>Familiares: </h5><span><?php echo $result['familiares'];?></span>
                </div>
                <div class="col-md-6" >
                    <h5>Pareja: </h5><span><?php echo $result['pareja'];?></span>
                </div>
            </div>
            <div class="row m-5-arriba">
                <div class="col-md-6" >
                    <h5>Lugar Tanatorio: </h5><span><?php echo $result['localidad_ceremonia'];?></span>
                </div>
                <div class="col-md-6" >
                    <h5>Lugar Iglesia: </h5><span><?php if(isset($result['iglesia_ceremonia'])) echo $result['iglesia_ceremonia'];?></span>
                </div>
            </div>
            <div class="row m-5-arriba">
                <div class="col-md-6" >
                    <h5>Fecha entierro: </h5><span><?php echo $result['fecha_ceremonia'];?></span>
                </div>
                <div class="col-md-6" >
                    <h5>Hora entierro: </h5><span><?php echo $result['hora_ceremonia'];?></span>
                </div>
            </div>
            <?php
            if(isset($result['direccion'])){
            ?>
            <div class="row m-5-arriba">
                <div class="col-md-12" >
                    <h5>Texto: </h5><span><?php echo $result['texto'];?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6"><h5>Teléfono contacto: </h5><?php echo $result['contact_phone1'];?></div>
                <div class="col-xs-12 col-sm-6"><h5>Otro teléfono contacto: </h5><?php echo $result['contact_phone2'];?></div>
            </div>
            <?php }
            if(strtoupper($result['username']) === strtoupper($_SESSION['publicacion']['username']) || ($_SESSION['rango'] === 'admin')) {
                ?>
                <div class="row m-5-arriba">
                    <div class="col-md-12">
                        <?php
                        if($item === 'death') {
                            ?>
                            <a class="btn btn-outline-danger"
                               href="./editar_publicacion.php?edit=true&code=<?php echo $result['code']; ?>">Modificar</a>
                            <?php
                        }else{
                            ?>
                            <a class="btn btn-outline-danger"
                               href="./editar_misa.php?edit=true&code=<?php echo $result['code']; ?>">Modificar</a>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </section>
</div>
<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
</body>

</html>

<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}elseif ($_SESSION['rango']!="admin" && $_SESSION['rango']!="especial"){
    header("Location:index.php");
}

$usuario=$_SESSION['usuario'];
$correo=$_SESSION['correo'];
$rango=$_SESSION['rango'];


include 'librerias/Encriptador.php';
$pass="qCQb+@E#LGaLe2E;+c,8xphSK4J_!,";

$user = $_SESSION['user_view'][0];
?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aurora Servicios</title><link rel="icon" href="imagenes/logo.ico">
    <?php include "css/basic_style.php"; ?>
</head>
<body style="background-color: #4dbaff">
<?php include "includes/nav.php"; ?>
<div class="container" >

    <section class="row">
        <div class="col-md-12 text-center">
            <h2>Usuario</h2>
            <?php
            if(isset($_SESSION['user_father'])){
                ?>
                <h3>Pertenece al usuario <?php echo '<form action="controller/UsuarioController.php" method="post" >
                                    <input type="hidden" value="'.$_SESSION['user_father']['username'].'" name="username" />
                                    <input type="hidden" value="'.$_SESSION['user_father']['cp'].'" name="cp" />
                                    <input type="hidden" value="'.$_SESSION['user_father']['id'].'" name="user_id" />
                                    <button type="submit" class="no-style" >'.$_SESSION['user_father']['username'].'</button></form>';?></h3>
            <?php
            }
            ?>
        </div>
    </section>

    <section class="row justify-content-center">
        <div class="col-sm-12 p-5 Borde_difuminado">
            <div class="row">
                <div class="col-md-12" >
                    <h4>Usuario</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" >
                    <p><b>Usuario: </b><?php if(isset($user)) echo $user['username'];?></p>
                </div>
                <div class="col-md-6" >
                    <p><b>Email: </b><?php if(isset($user))echo $user['email'];?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label for="rango">
                        <p><b>Tipo de usuario</b></p>
                    </label>
                </div>
                <div class="col-md-3">
                    <p><?php if(isset($user))echo $user['rango'];?></p>
                </div>
            </div>
        </div>
    </section>
    <section class="row justify-content-center m-5-arriba">
        <div class="col-sm-12 p-5 Borde_difuminado" >
            <div class="row">
                <div class="col-md-12" >
                    <h4>Datos Empresa</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4" >
                    <p><b>Nombre empresa: </b><?php if(isset($user))echo $user['name'];?></p>
                </div>
                <div class="col-md-4" >
                    <p><b>Teléfono: </b><?php if(isset($user))echo $user['phone1']; ?></p>
                </div>
                <div class="col-md-4" >
                    <p><b>Nif: </b><?php if(isset($user))echo $user['nif'];?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <p><b>Dirección: </b><?php if(isset($user))echo $user['address'];?></p>
                </div>
                <div class="col-md-4">
                    <p><b>Localidad: </b><?php if(isset($user))echo $user['localidad'];?></p>
                </div>
                <div class="col-md-4">
                    <p><b>Código postal: </b><?php if(isset($user))echo $user['cp'];?></p>
                </div>
            </div>
            <div class="row m-2-arriba">
                <div class="col-md-6" >
                    <p><b>Cuenta: </b><?php if(isset($user))echo Encriptador::desencriptar($user['account'], $pass);?></p>
                </div>
                <div class="col-md-6" >
                    <p><b>Gerente: </b><?php if(isset($user))echo $user['name_contact'];?></p>
                </div>
            </div>
        </div>
    </section>
    <?php if($user['rango'] === "grupo"){ ?>
    <form class="form-horizontal m-5-arriba" id="formulario" name="formulario" action="controller/UsuarioController.php"
          autocomplete="off" method="POST">
        <section class="row justify-content-center">
            <div class="col-sm-12 p-5 Borde_difuminado">
                <div class="row">
                    <div class="col-md-12" >
                        <h4>Añadir usuario asociado</h4>
                    </div>
                </div>
                <input type="hidden" name="created_user_group" value="<?php echo $user['id']; ?>">
                <div class="row">
                    <div class="col-md-4" >
                        <input required name ="usuario"  autocomplete="off" type="text" class="form-control" id="usuario" value="<?php if(isset($_SESSION['created_user']))echo $_SESSION['created_user'];?>" placeholder="Usuario">
                    </div>
                    <div class="col-md-4">
                        <input required name ="localidad" type="text" class="form-control" id="localidad"  value="<?php if(isset($_SESSION['created_localidad']))echo $_SESSION['created_localidad'];?>" placeholder="Localidad">
                    </div>
                    <div class="col-md-4">
                        <input required name ="cp" type="number" class="form-control" id="cp"  value="<?php if(isset($_SESSION['created_cp']))echo $_SESSION['created_cp'];?>" placeholder="Codigo Postal">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" >
                        <input name ="precio" type="number" step="any" class="form-control" id="precio" value="<?php if(isset($_SESSION['created_precio']))echo $_SESSION['created_precio'];?>" placeholder="0.0">
                    </div>
                    <div class="col-md-4" >
                        <input required name ="contrasena"  autocomplete="off" type="password" class="form-control" id="contrasena" aria-describedby="" placeholder="Contraseña">
                    </div>
                    <div class="col-md-4" >
                        <input required name ="rep_contrasena" type="password" class="form-control" id="rep_contrasena" aria-describedby="" placeholder="Repite Contraseña">
                    </div>
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <input name="boton" type="submit" id="btn-end" class="btn btn-danger align-self-center" value="Terminar">
                <input name="boton" type="submit" class="btn btn-primary align-self-right" value="Enviar">
            </div>
        </div>
    </form>

    <?php } if(isset($_SESSION['users_group']) && $_SESSION['users_group'] != null){ ?>
        <section>
            <article>
                <header>
                    <h3>Usuarios asociados a  <?php echo $user['username']; ?></h3>
                </header>
                <div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>C. Postal</th>
                            <th>Localidad</th>
                            <th>Precio</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($_SESSION['users_group'] as $user){
                            if($user['userId'] == null){
                                $background = "style='background: #a4bdd6;'";
                            } else{
                                $background = "";
                            }
                            echo '<tr '.$background.'>';
                            ?>
                                <td><?php echo $user['username']; ?></td>
                                <td><?php echo $user['codigo_postal']; ?></td>
                                <td><?php echo $user['localidad']; ?></td>
                                <td><?php echo $user['precio']; ?> €</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </article>
        </section>
    <?php } ?>
</div>
<?php include "includes/footer.php"; ?>
<?php include "js/basic_js.php"; ?>
<script>

</script>
</body>
</html>